/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: oct_ws_priv.c

Copyright (c) 2015 Octasic Inc. All rights reserved.

Description:	Wireshark private data

This source code is Octasic Confidential. Use of and access to this code
is covered by the Octasic Device Enabling Software License Agreement.
Acknowledgement of the Octasic Device Enabling Software License was
required for access to this code. A copy was also provided with the release.

Release: Octasic Application Development Framework OCTADF-04.01.00-B1993 (2015/09/16)

$Octasic_Revision: 4 $


\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
#ifndef __OCT_WS_PRIV_H__
#define __OCT_WS_PRIV_H__

/*****************************  INCLUDE FILES  *******************************/

/*****************************  DEFINES **************************************/
typedef struct
{
	guint16	total_packet_size;
	guint16	pktHdt_size;
	guint8	api_type;
	guint8	format;			/* PktFormat */
	guint8	trace_flag;
	guint16	padding;

}tOctWsPrivateData;

#endif /* __OCT_WS_PRIV_H__ */
