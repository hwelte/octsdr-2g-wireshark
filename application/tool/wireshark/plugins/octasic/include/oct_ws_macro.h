/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: oct_ws_macro.c

Copyright (c) 2015 Octasic Inc. All rights reserved.

Description:	Wireshark macro

This source code is Octasic Confidential. Use of and access to this code
is covered by the Octasic Device Enabling Software License Agreement.
Acknowledgement of the Octasic Device Enabling Software License was
required for access to this code. A copy was also provided with the release.

Release: Octasic Application Development Framework OCTADF-04.01.00-B1993 (2015/09/16)

$Octasic_Revision: 4 $


\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
#ifndef __OCT_WS_MACRO_H__
#define __OCT_WS_MACRO_H__

/*****************************  INCLUDE FILES  *******************************/
#include <color.h>
#include <color_filters.h>

/*****************************  DEFINES **************************************/
#ifndef UNUSED
#ifdef __GNUC__
#  define UNUSED(x) UNUSED_ ## x __attribute__((__unused__))
#else
#  define UNUSED(x) UNUSED_ ## x
#endif
#endif

#ifndef UNUSED_FUNCTION
#ifdef __GNUC__
#  define UNUSED_FUNCTION(x) __attribute__((__unused__)) UNUSED_ ## x
#else
#  define UNUSED_FUNCTION(x) UNUSED_ ## x
#endif
#endif

#define mWS_RED_COMPONENT(x)   (guint16) (((((x) >> 16) & 0xff) * 65535 / 255))
#define mWS_GREEN_COMPONENT(x) (guint16) (((((x) >>  8) & 0xff) * 65535 / 255))
#define mWS_BLUE_COMPONENT(x)  (guint16) ( (((x)        & 0xff) * 65535 / 255))

#define mWS_FIELDSIZE( type, field)		(sizeof(((type *)0)->field))
#define mWS_FIELDOFFSET(type, field)    ((guint32)&(((type *)0)->field))
#define mWS_COUNTOF(array) (sizeof(array)/sizeof(array[0]))

#if defined(VERSION_MAJOR) && (VERSION_MAJOR > 1 || (VERSION_MAJOR == 1 && VERSION_MINOR > 8))
#define OCT_MATCH_STRVAL_IDX(a,b,c) try_val_to_str_idx(a,b,c)
#else
#define OCT_MATCH_STRVAL_IDX(a,b,c) match_strval_idx(a,b,c)
#define dissector_add_uint(a,b,c)	dissector_add(a,b,c)
#endif

#endif /* __OCT_WS_MACRO_H__ */
