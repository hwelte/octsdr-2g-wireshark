/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: oct_ws_version.c

Copyright (c) 2015 Octasic Inc. All rights reserved.

Description:	Octasic plugin version

This source code is Octasic Confidential. Use of and access to this code
is covered by the Octasic Device Enabling Software License Agreement.
Acknowledgement of the Octasic Device Enabling Software License was
required for access to this code. A copy was also provided with the release.

Release: Octasic Application Development Framework OCTADF-04.01.00-B1993 (2015/09/16)

$Octasic_Revision: 4 $


\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
#ifndef __OCT_WS_VERSION_H__
#define __OCT_WS_VERSION_H__

/*****************************  INCLUDE FILES  *******************************/

/*****************************  DEFINES **************************************/
#define NO_TGT_VERSION 


/*****************************  DEFINES **************************************/


#endif /* __OCT_WS_VERSION_H__ */
