/* Included *after* config.h, in order to re-define these macros */
#include "../../include/oct_ws_version.h"

#ifdef PACKAGE
#undef PACKAGE
#endif

/* Name of package */
#define PACKAGE "octpkt"


#ifdef VERSION
#undef VERSION
#endif

#ifndef MODULE_VERSION
#define MODULE_VERSION 0_0_1_DEV
#endif

#define _QUOTEME(x) #x
#define QUOTEME(x) _QUOTEME(x)

/* Version number of package */
/* WAS : #if !defined( TGT_VERSION ) || (TGT_VERSION == 0) but TGT_VERSION may be a string ... can't compare */
#if defined( NO_TGT_VERSION )
#define VERSION QUOTEME(MODULE_VERSION)
#else
#define VERSION QUOTEME(MODULE_VERSION-TGT_VERSION)
#endif

