/* Do not modify this file.  */
/* It is created automatically by the Makefile.  */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif


#include <gmodule.h>

#include "../include/moduleinfo.h"

#ifndef ENABLE_STATIC

#if defined(VERSION_MAJOR) && (VERSION_MAJOR > 1 || (VERSION_MAJOR == 1 && VERSION_MINOR > 8))
#define WS_BUILD_DLL
#include "ws_symbol_export.h"
#define WS_EXPORT WS_DLL_PUBLIC_NOEXTERN
#else
#define WS_EXPORT G_MODULE_EXPORT	
#endif

WS_EXPORT const gchar version[] = VERSION;

/* Start the functions we need for the plugin stuff */
WS_EXPORT void
plugin_register (void)
{
  {extern void proto_register_octpkt (void); proto_register_octpkt ();}
}

WS_EXPORT void
plugin_reg_handoff(void)
{
  {extern void proto_reg_handoff_octpkt (void); proto_reg_handoff_octpkt ();}
}
#endif

