/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: plugin_info.h

Copyright (c) 2015 Octasic Inc. All rights reserved.

Description:	Wireshark plugin common

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: Octasic Application Development Framework OCTADF-04.01.00-B1993 (2015/09/16)

$Octasic_Revision: 4 $


\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
#ifndef __PLUGIN_INFO_H__
#define __PLUGIN_INFO_H__


/*****************************  INCLUDES *************************************/
#include <epan/packet.h>
#include "../../../include/oct_ws_version.h"
#include "../../../include/oct_ws_macro.h"

/*****************************  DEFINES **************************************/


#ifdef PACKAGE
#undef PACKAGE
#endif

/* Name of package */
#define PACKAGE "OCTVC1"


#ifdef VERSION
#undef VERSION
#endif

#ifndef MODULE_VERSION
#define MODULE_VERSION 0_0_1_DEV
#endif

#define _QUOTEME(x) #x
#define QUOTEME(x) _QUOTEME(x)

/* Version number of package */
/* WAS : #if !defined( TGT_VERSION ) || (TGT_VERSION == 0) but TGT_VERSION may be a string ... can't compare */
#if defined( NO_TGT_VERSION )
#define VERSION QUOTEME(MODULE_VERSION)
#else
#define VERSION QUOTEME(MODULE_VERSION-TGT_VERSION)
#endif


/*****************************  VARIABLE ***********************************/
extern dissector_handle_t data_handle;


/*****************************  PROTO **************************************/
void proto_register_octvc1_ctrl(void);
void proto_register_octvc1_event(void);


#endif /* __PLUGIN_INFO_H__ */
