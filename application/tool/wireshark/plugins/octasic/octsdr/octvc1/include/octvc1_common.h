/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: octvc1_common.h

      
Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark module dissector related functions

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/
#ifndef _OCTVC1_COMMON_H__
#define _OCTVC1_COMMON_H__

#include <epan/packet.h>
#include "octdev_common.h" 
#include "octpkt_common.h" 
#include "octvocnet_common.h" 

/****************************************************************************
	COMMON API ENUMERATION STRING VALUES
 ****************************************************************************/
extern const value_string  vals_tOCTVC1_OBJECT_CURSOR_ENUM[]; 
extern const value_string  vals_tOCTVC1_BUFFER_FORMAT_ENUM[]; 
extern const value_string  vals_tOCTVC1_BUFFER_FORMAT_MAIN_ENUM[]; 
extern const value_string  vals_tOCTVC1_ETH_PORT_ID_ENUM[]; 
extern const value_string  vals_tOCTVC1_IP_VERSION_ENUM[]; 
extern const value_string  vals_tOCTVC1_VLAN_PROTOCOL_ID_ENUM[]; 
extern const value_string  vals_tOCTVC1_MSG_FLAGS_MASK[4];
extern const value_string  vals_tOCTVC1_LOG_TYPE_ENUM[]; 
extern const value_string  vals_tOCTVC1_LOG_LEVEL_ENUM[]; 
extern const value_string  vals_tOCTVC1_LOG_PAYLOAD_TYPE_ENUM[]; 
extern const value_string  vals_tOCTVC1_LOG_TRACE_MASK[11];
extern const value_string  vals_tOCTVC1_FILE_TYPE_ENUM[]; 
extern const value_string  vals_tOCTVC1_FILE_FORMAT_ENUM[]; 
extern const value_string  vals_tOCTVC1_MODULE_ID_ENUM[]; 
extern const value_string  vals_tOCTVC1_TAP_DIRECTION_ENUM[]; 
extern const value_string  vals_tOCTVC1_TAP_ID[10];
extern const value_string  vals_tOCTVC1_RADIO_STANDARD_ENUM[]; 
extern const value_string  vals_tOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM[]; 
extern const value_string  vals_tOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM[]; 
extern const value_string  vals_tOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM[]; 
extern const value_string  vals_tOCTVC1_RADIO_CONFIG_SCRIPT_TYPE_ENUM[]; 
extern const value_string  vals_tOCTVC1_RADIO_ID_DIRECTION_ENUM[]; 
extern const value_string  vals_tOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM[]; 
extern const value_string  vals_tOCTVC1_RADIO_RX_GAIN_CTRL_MODE_ENUM[]; 
extern const value_string  vals_tOCTVC1_PROCESS_TYPE_ENUM[]; 
extern const value_string  vals_tOCTVC1_PROCESS_STATE_ENUM[]; 
extern const value_string  vals_tOCTVC1_PROCESS_TASK_STATE_ENUM[]; 

/****************************************************************************
	COMMON API DISSECTOR
 ****************************************************************************/
extern unsigned int  dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_CURSOR_INDEX_GET(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_OBJECT32_NAME(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_VLAN_TAG(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_VLAN_HEADER_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_LIST_HANDLE_OBJECT_GET(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_LIST_NAME_OBJECT32_GET(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_LIST_INDEX_GET(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_LIST_INDEX_GET_SUB_INDEX(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_MSG_HEADER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_EVENT_HEADER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_LOG_HEADER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_FILE_HEADER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_MODULE_DATA(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_API_SESSION_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_API_SESSION_EVT_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_PROCESS_ERROR(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_PROCESS_TASK_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );
extern unsigned int  dissect_tOCTVC1_PROCESS_CPU_USAGE_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue );

/****************************************************************************
	COMMON USER REGISTATION
 ****************************************************************************/
void ws_register_OCTVC1_common(void);
#define cOCTVC1_PRIVATE_ID_STRING "unknowned_private"
#define cOCTVC1_UNKNOWN_STRING "Unknown"

#ifndef OCT_OPTION_REMOVE_PRIVATE
#define cOCTVC1_THE_USER  "oct-super-user"
#define cOCTVC1_THE_USER_ID  "octsu98asic"
#endif /* OCT_OPTION_REMOVE_PRIVATE */

#define cOCTVC1_PRIVATE_VISIBILITY_USER  "oct-dev"
#ifdef OCT_DECLARE_COMMON
gchar* aOCTVC1_user_list[][2] = {
	{ "oct-dev","octdev98asic"},
#ifndef OCT_OPTION_REMOVE_PRIVATE
	{ "feature-spec","feature-psw"},
	{ "company","company-psw"},
	{ "company-2","company-2-psw"},
#endif /* OCT_OPTION_REMOVE_PRIVATE */
	{NULL,NULL}
 };
#else
gchar* aOCTVC1_user_list[][2];
#endif /* OCT_DECLARE_COMMON */

extern const gchar* octvc1_chck_private( guint32 f_event_id_code, const value_string *f_PrivateApi, gint32 * f_piRegistered );

#endif /* _OCTVC1_COMMON_H__ */

