
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: oct_ws_util.h

Copyright (c) 2015 Octasic Inc. All rights reserved.

Description:	Wireshark utility

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: Octasic Application Development Framework OCTADF-04.01.00-B1993 (2015/09/16)

$Octasic_Revision: 4 $


\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
#ifndef __OCT_WS_UTIL_H__
#define __OCT_WS_UTIL_H__

/*****************************  INCLUDE FILES  *******************************/
#include <epan/packet.h>

/*****************************  DEFINES **************************************/

typedef int ( * tmodule_dissector_fnc )( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree );
typedef int ( * tCID_dissector_fnc )( tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset );
typedef void ( * tregister_dissector_fnc )( void );

typedef struct
{
	gint ett;
	int proto;

}tWS_ENTITY_HANDLE;

typedef struct
{
	char *pszValue;
	int lValue;

}tWS_EXTRA_VALUE;

extern int hf_octvc1_msg_cmd;
extern int hf_octvc1_msg_rsp;
extern int hf_octvc1_msg_evt;

extern gint ett_octvc1_msg_type;



/*****************************  PROTO **************************************/
extern void ws_dissector_not_registered(void);
extern int ws_dissector_module_notimplemented( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree );
extern int ws_dissector_notimplemented( tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset );


extern unsigned int  dissect_tOCTVC1_MAIN_FILE_HANDLE(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtraValue );
extern const char *  dissect_OCTVC1_Return_Code( guint32 ulRc );


#if defined(VERSION_MAJOR) && (VERSION_MAJOR > 1 || (VERSION_MAJOR == 1 && VERSION_MINOR > 8))
WS_DLL_PUBLIC const char *decode_enumerated_bitfield(const guint32 val, const guint32 mask,const int width, const value_string *tab, const char *fmt);
#endif


#endif /* __OCT_WS_UTIL_H__ */
