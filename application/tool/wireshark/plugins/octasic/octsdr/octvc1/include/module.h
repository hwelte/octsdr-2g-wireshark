/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: module.h

Copyright (c) 2015 Octasic Inc. All rights reserved.

Description:	Wireshark module

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: Octasic Application Development Framework OCTADF-04.01.00-B1993 (2015/09/16)

$Octasic_Revision: 4 $


\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
#ifndef __MODULE_H__
#define __MODULE_H__

/*****************************  INCLUDE FILES  *******************************/
#include <epan/packet.h>
#include "ws_util.h"


/*****************************  INCLUDES *************************************/
/*****************************  DEFINES **************************************/

typedef struct
{
	const char *pszModuleName;
	const value_string *apCodeIdValueString;
	const value_string *apEvtIdValueString;
	const value_string *apSupIdValueString;
	tregister_dissector_fnc	Register_fnc;
	tmodule_dissector_fnc	Dissector_fnc;
	const value_string *apPrivateCodeIdValueString;
	const value_string *apPrivateEvtIdValueString;
}tModuleColInfo;

extern const value_string vals_OCTVC1_module_UID[];
extern tModuleColInfo aOCTVC1ModuleColInfo[];
extern int proto_octvc1_ctrl;

/*****************************  PROTO **************************************/
void ws_register_dissector_module(void);


#endif /* __MODULE_H__ */
