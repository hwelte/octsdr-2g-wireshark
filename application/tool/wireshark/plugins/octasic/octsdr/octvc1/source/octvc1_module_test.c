/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: octvc1_module_test.c

      
Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark module dissector related functions

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/

#include <config.h>
#include <epan/packet.h>
#include "../../../include/oct_ws_macro.h"
#include "../include/module.h"
#include "../include/octvc1_common.h"

#include <test/octvc1_test_api_priv.h>


/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_TEST_EXEC_PARMS[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_EXEC_PARMS;

void  register_tOCTVC1_TEST_EXEC_PARMS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_EXEC_PARMS[0],
			{ "ahObj", "octvc1.test.exec_parms.ahobj",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ahObj",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_EXEC_PARMS[1],
			{ "szParms", "octvc1.test.exec_parms.szparms",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szParms",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_EXEC_PARMS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	CMD/RSP Registered 
 ****************************************************************************/

int ahf_tOCTVC1_TEST_MSG_DESCR_LIST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_DESCR_LIST_CMD;

void  register_tOCTVC1_TEST_MSG_DESCR_LIST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_LIST_CMD[0],
			{ "ObjectCursor", "octvc1.test.descr.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_DESCR_LIST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_DESCR_LIST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_DESCR_LIST_RSP;

void  register_tOCTVC1_TEST_MSG_DESCR_LIST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_LIST_RSP[0],
			{ "ObjectCursor", "octvc1.test.descr.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_LIST_RSP[1],
			{ "ObjectNameList", "octvc1.test.descr.list.objectnamelist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectNameList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_DESCR_LIST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_DESCR_INFO_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_DESCR_INFO_CMD;

void  register_tOCTVC1_TEST_MSG_DESCR_INFO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_INFO_CMD[0],
			{ "hDescr", "octvc1.test.descr.info.hdescr",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hDescr",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_DESCR_INFO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_DESCR_INFO_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_DESCR_INFO_RSP;

void  register_tOCTVC1_TEST_MSG_DESCR_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_INFO_RSP[0],
			{ "hDescr", "octvc1.test.descr.info.hdescr",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hDescr",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_INFO_RSP[1],
			{ "szName", "octvc1.test.descr.info.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_INFO_RSP[2],
			{ "szDescription", "octvc1.test.descr.info.szdescription",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szDescription",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_INFO_RSP[3],
			{ "szUsage", "octvc1.test.descr.info.szusage",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szUsage",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_DESCR_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD;

void  register_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD[0],
			{ "szName", "octvc1.test.descr.info_by_name.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP;

void  register_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP[0],
			{ "hDescr", "octvc1.test.descr.info_by_name.hdescr",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hDescr",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP[1],
			{ "szName", "octvc1.test.descr.info_by_name.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP[2],
			{ "szDescription", "octvc1.test.descr.info_by_name.szdescription",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szDescription",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP[3],
			{ "szUsage", "octvc1.test.descr.info_by_name.szusage",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szUsage",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_DESCR_STATS_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_DESCR_STATS_CMD;

void  register_tOCTVC1_TEST_MSG_DESCR_STATS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_STATS_CMD[0],
			{ "hDescr", "octvc1.test.descr.stats.hdescr",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hDescr",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_DESCR_STATS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_DESCR_STATS_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_DESCR_STATS_RSP;

void  register_tOCTVC1_TEST_MSG_DESCR_STATS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_DESCR_STATS_RSP[0],
			{ "hDescr", "octvc1.test.descr.stats.hdescr",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hDescr",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_DESCR_STATS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD[0],
			{ "hDescr", "octvc1.test.runner_server.test.add.hdescr",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hDescr",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD[1],
			{ "Parms", "octvc1.test.runner_server.test.add.parms",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Parms",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP[0],
			{ "hTest", "octvc1.test.runner_server.test.add.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP[1],
			{ "szDisplayName", "octvc1.test.runner_server.test.add.szdisplayname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szDisplayName",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD[0],
			{ "hTest", "octvc1.test.runner_server.test.remove.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP[0],
			{ "hTest", "octvc1.test.runner_server.test.remove.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD[0],
			{ "ObjectCursor", "octvc1.test.runner_server.test.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP[0],
			{ "ObjectCursor", "octvc1.test.runner_server.test.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP[1],
			{ "ObjectNameList", "octvc1.test.runner_server.test.list.objectnamelist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectNameList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD[0],
			{ "hTest", "octvc1.test.runner_server.test.info.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP[0],
			{ "hTest", "octvc1.test.runner_server.test.info.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP[1],
			{ "hDescr", "octvc1.test.runner_server.test.info.hdescr",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hDescr",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP[2],
			{ "Parms", "octvc1.test.runner_server.test.info.parms",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Parms",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD[0],
			{ "hTest", "octvc1.test.runner_server.test.stats.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP[0],
			{ "hTest", "octvc1.test.runner_server.test.stats.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP[1],
			{ "szStats", "octvc1.test.runner_server.test.stats.szstats",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szStats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD[0],
			{ "hTest", "octvc1.test.runner_server.test.start.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP[0],
			{ "hTest", "octvc1.test.runner_server.test.start.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD[0],
			{ "hTest", "octvc1.test.runner_server.test.stop.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP;

void  register_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP[0],
			{ "hTest", "octvc1.test.runner_server.test.stop.htest",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTest",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_TEST_EXEC_PARMS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_EXEC_PARMS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_EXEC_PARMS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_EXEC_PARMS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_TEST_EXEC_PARMS);
		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_EXEC_PARMS, ahObj), "ahObj");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<8; i++ )
			{
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_TEST_EXEC_PARMS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_EXEC_PARMS, ahObj), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_TEST_EXEC_PARMS[0], tvb, offset,
			4, temp_data, "[%d]: 0x%08x", i, temp_data );
		}
				offset+=4;
			}
		}
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_TEST_EXEC_PARMS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_EXEC_PARMS, szParms), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_TEST_EXEC_PARMS, szParms);

	}


	return offset;

};

/****************************************************************************
	CMD/RSP dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_TEST_MSG_DESCR_LIST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_DESCR_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_LIST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_DESCR_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_LIST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_LIST_CMD, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_DESCR_LIST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_DESCR_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_LIST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_DESCR_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_LIST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_LIST_RSP, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_LIST_RSP, ObjectNameList), "ObjectNameList:tOCTVC1_LIST_NAME_OBJECT32_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_NAME_OBJECT32_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_DESCR_INFO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_DESCR_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_INFO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_DESCR_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_INFO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_INFO_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_CMD, hDescr);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_DESCR_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_DESCR_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_DESCR_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_RSP, hDescr);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_INFO_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_RSP, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_RSP, szName);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_INFO_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_RSP, szDescription), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_RSP, szDescription);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_INFO_RSP[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_RSP, szUsage), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_RSP, szUsage);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD, szName);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP, hDescr);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP, szName);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP, szDescription), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP, szDescription);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP, szUsage), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP, szUsage);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_DESCR_STATS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_DESCR_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_STATS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_DESCR_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_STATS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_STATS_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_STATS_CMD, hDescr);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_DESCR_STATS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_DESCR_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_STATS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_DESCR_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_DESCR_STATS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_DESCR_STATS_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_DESCR_STATS_RSP, hDescr);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD, hDescr),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD, hDescr);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD, Parms), "Parms:tOCTVC1_TEST_EXEC_PARMS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_TEST_EXEC_PARMS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP, hTest);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP, szDisplayName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP, szDisplayName);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD, hTest),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD, hTest);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP, hTest);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP, ObjectNameList), "ObjectNameList:tOCTVC1_LIST_NAME_OBJECT32_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_NAME_OBJECT32_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD, hTest),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD, hTest);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP, hTest);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP, hDescr),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP[1], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP, hDescr);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP, Parms), "Parms:tOCTVC1_TEST_EXEC_PARMS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_TEST_EXEC_PARMS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD, hTest),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD, hTest);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP, hTest);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP, szStats), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP, szStats);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD, hTest),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD, hTest);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP, hTest);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD, hTest),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD, hTest);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP, hTest);

	}


	return 0;

};
/****************************************************************************
	MODULE REGISTERED EXPORTED FUNCTION
 ****************************************************************************/

void ws_register_OCTVC1_TEST(void)
{
	/****************************************************************************
		Register Common struct
	****************************************************************************/
	register_tOCTVC1_TEST_EXEC_PARMS(); 

	/****************************************************************************
		CMD/RSP Registered 
	****************************************************************************/
	register_tOCTVC1_TEST_MSG_DESCR_LIST_CMD(); 
	register_tOCTVC1_TEST_MSG_DESCR_LIST_RSP(); 
	register_tOCTVC1_TEST_MSG_DESCR_INFO_CMD(); 
	register_tOCTVC1_TEST_MSG_DESCR_INFO_RSP(); 
	register_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD(); 
	register_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP(); 
	register_tOCTVC1_TEST_MSG_DESCR_STATS_CMD(); 
	register_tOCTVC1_TEST_MSG_DESCR_STATS_RSP(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD(); 
	register_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP(); 

}

/****************************************************************************
	MODULE DISSECTOR FUNCTIONS
 ****************************************************************************/
int ws_dissect_OCTVC1_TEST_CMD( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_TEST_MSG_DESCR_LIST_CID: return dissect_tOCTVC1_TEST_MSG_DESCR_LIST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_DESCR_INFO_CID: return dissect_tOCTVC1_TEST_MSG_DESCR_INFO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CID: return dissect_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_DESCR_STATS_CID: return dissect_tOCTVC1_TEST_MSG_DESCR_STATS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_CID: return 0; break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_CID: return 0; break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_START_CID: return 0; break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_CID: return 0; break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_TEST_RSP( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_TEST_MSG_DESCR_LIST_CID: return dissect_tOCTVC1_TEST_MSG_DESCR_LIST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_DESCR_INFO_CID: return dissect_tOCTVC1_TEST_MSG_DESCR_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CID: return dissect_tOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_DESCR_STATS_CID: return dissect_tOCTVC1_TEST_MSG_DESCR_STATS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_CID: return 0; break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_CID: return 0; break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_START_CID: return 0; break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_CID: return 0; break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CID: return dissect_tOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_RSP( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_TEST_EVT( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			default: return 1;
		}
	}
	return 0;

}

/****************************************************************************
	MODULE DISSECTOR EXPORTED FUNCTION
 ****************************************************************************/

int ws_dissect_OCTVC1_TEST( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if (message_type == cOCTVC1_MSG_TYPE_RESPONSE)
		return ws_dissect_OCTVC1_TEST_RSP( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_COMMAND)
		return ws_dissect_OCTVC1_TEST_CMD( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_NOTIFICATION )
		return ws_dissect_OCTVC1_TEST_EVT( CID, tvb, pinfo, tree);
	else
		return 1;

}

