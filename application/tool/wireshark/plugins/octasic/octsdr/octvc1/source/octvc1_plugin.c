/* octvc1_plugin.c
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <string.h>

#include <config.h>
#include <epan/packet.h>
#include <epan/prefs.h>
#include <epan/expert.h>

#include "../include/plugin_info.h"
#include "../include/module.h"
#define OCT_DECLARE_COMMON
#include "../include/octvc1_common.h"


static int proto_octvc1 = -1;
dissector_handle_t data_handle;

static int fUserRegistered = 0;

char * pszOctvc1_user_name=NULL;
char * pszOctvc1_user_id=NULL;
int g_fIsRegistered = 0;
static char l_szRegisteredUser[128]={0};
static int l_iRegisteredUserLen = 0;
static int l_id_code_Registered = 0;
static gchar l_szAPIStr[256];
static int l_fIsBigBoss = 0;

const gchar* octvc1_chck_private( guint32 f_id_code, const value_string *f_PrivateApi, gint32 *f_piRegistered )
{
	const gchar* pszValueString;

	//CHck if same as last request
	if( l_iRegisteredUserLen && ( l_id_code_Registered == f_id_code ) )
	{
		*f_piRegistered = 1;
		return l_szAPIStr;
	}

	*f_piRegistered = 0;
	pszValueString = val_to_str( f_id_code, f_PrivateApi, cOCTVC1_UNKNOWN_STRING );
	if( strcmp( pszValueString, cOCTVC1_UNKNOWN_STRING ) )
	{
		// Expect API ID STRING:user,user2,user3
		gchar *pszColumn = strchr( pszValueString, ':' );
		if( pszColumn )
		{
			gchar *pszStr;

			// Keep API string
			strncpy( l_szAPIStr, pszValueString, (pszColumn-pszValueString) );
			l_szAPIStr[(pszColumn-pszValueString)]=0;

			// Chck if theBoss is register
			if( !l_fIsBigBoss )
				pszStr = strstr( pszColumn+1, l_szRegisteredUser );

			// Find if register for this
			if( l_fIsBigBoss ||
				( pszStr && ((pszStr==(pszColumn+1)) || (*(pszStr-1)==',') ) && 
				  ( (*(pszStr+l_iRegisteredUserLen)==',') || (*(pszStr+l_iRegisteredUserLen)==0 ) ) ) )
			{
				l_id_code_Registered = f_id_code;
				*f_piRegistered = 1;
				return l_szAPIStr;
			}
		}
	}
	return cOCTVC1_UNKNOWN_STRING;
}

void pref_proto_octvc1_apply_callback( void )
{
	char *pszPath;

	// by default this callback is called at startup ... skip the first call
	if( fUserRegistered )
	{
		int fReguser=0;

		{
			int i;

			for( i=0; ; i++ )
			{
				if( aOCTVC1_user_list[i][0] == NULL )
					break;

				if( ( strcmp( aOCTVC1_user_list[i][0], pszOctvc1_user_name ) == 0 ) &&
					( strcmp( aOCTVC1_user_list[i][1], pszOctvc1_user_id ) == 0 ) )
				{
					fReguser = 1;
					l_fIsBigBoss = 0;
					break;
				}
			}
		}

		if( fReguser )
		{
			g_fIsRegistered = 1;
			strcpy( l_szRegisteredUser, pszOctvc1_user_name  );
			l_iRegisteredUserLen = strlen( l_szRegisteredUser );
		}
		else
		{
			g_fIsRegistered = 0;
			l_fIsBigBoss = 0;
		}
	}
	fUserRegistered = 1;

	{
		char szPref[240];
		strcpy( szPref, "octvc1.user_name:user" );
		prefs_set_pref( szPref );
		strcpy( szPref, "octvc1.user_id:****" );
		prefs_set_pref( szPref );
		write_prefs( &pszPath );
	}

	// Reset last check
	l_id_code_Registered = 0;
	l_szAPIStr[0] = 0;



}

/*************************************************************************
*
*  Code to register the protocol with Wireshark
*
**************************************************************************/
void proto_register_octvc1(void)
{                 
	module_t *octvc1_module;

	proto_octvc1 = proto_register_protocol(	"OCTVC1 Packets", "Octasic OCTVC1", "octvc1");

	octvc1_module = prefs_register_protocol(proto_octvc1, pref_proto_octvc1_apply_callback);
	prefs_register_string_preference(octvc1_module, "user_name", "User name", "OCTVC1 user name", &pszOctvc1_user_name);
	prefs_register_string_preference(octvc1_module, "user_id", "User id", "OCTVC1 user id", &pszOctvc1_user_id);


	/* register ctrl dissector */
	proto_register_octvc1_ctrl();

	/* register event dissector */
	proto_register_octvc1_event();

	/* Call module register fnc */
	ws_register_dissector_module();

}


/*************************************************************************
*
*  If this dissector uses sub-dissector registration add a registration routine.
*  This format is required because a script is used to find these routines and
*  create the code that calls these routines.
*
**************************************************************************/
void proto_reg_handoff_octvc1(void)
{
	extern int proto_octvc1_event;
	extern int proto_octvc1_ctrl;

	extern int dissect_octvc1_ctrl(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree);
	extern int dissect_octvc1_event(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree);

	dissector_handle_t octvc1_ctrl_handle;
	dissector_handle_t octvc1_event_handle;

	register_dissector_table( "vocallonet.api_type", "Vocallonet API", FT_UINT32, BASE_HEX);

	octvc1_ctrl_handle = new_create_dissector_handle(dissect_octvc1_ctrl, proto_octvc1_ctrl);
	dissector_add_uint("vocallonet.api_type", 0, octvc1_ctrl_handle);
	
	octvc1_event_handle = new_create_dissector_handle(dissect_octvc1_event, proto_octvc1_event);
	dissector_add_uint("vocallonet.api_type", 0, octvc1_event_handle);

	data_handle = find_dissector("data");

}

