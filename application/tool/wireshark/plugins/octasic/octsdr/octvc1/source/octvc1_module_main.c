/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: octvc1_module_main.c

      
Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark module dissector related functions

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/

#include <config.h>
#include <epan/packet.h>
#include "../../../include/oct_ws_macro.h"
#include "../include/module.h"
#include "../include/octvc1_common.h"

#include <main/octvc1_main_evt_priv.h>


/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_MAIN_APPLICATION_STATE_ENUM[] = 
 {
	{ cOCTVC1_MAIN_APPLICATION_STATE_ENUM_STOPPED, "cOCTVC1_MAIN_APPLICATION_STATE_ENUM_STOPPED" },
	{ cOCTVC1_MAIN_APPLICATION_STATE_ENUM_STARTED, "cOCTVC1_MAIN_APPLICATION_STATE_ENUM_STARTED" },
	{ cOCTVC1_MAIN_APPLICATION_STATE_ENUM_BOOTED, "cOCTVC1_MAIN_APPLICATION_STATE_ENUM_BOOTED" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_MAIN_APPLICATION_MODULE_STATE_ENUM[] = 
 {
	{ cOCTVC1_MAIN_APPLICATION_MODULE_STATE_ENUM_INVALID, "cOCTVC1_MAIN_APPLICATION_MODULE_STATE_ENUM_INVALID" },
	{ cOCTVC1_MAIN_APPLICATION_MODULE_STATE_ENUM_STOPPED, "cOCTVC1_MAIN_APPLICATION_MODULE_STATE_ENUM_STOPPED" },
	{ cOCTVC1_MAIN_APPLICATION_MODULE_STATE_ENUM_STARTED, "cOCTVC1_MAIN_APPLICATION_MODULE_STATE_ENUM_STARTED" },
	{ cOCTVC1_MAIN_APPLICATION_MODULE_STATE_ENUM_ERROR, "cOCTVC1_MAIN_APPLICATION_MODULE_STATE_ENUM_ERROR" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_MAIN_APPLICATION_MODULE_CONFIG_STATE_ENUM[] = 
 {
	{ cOCTVC1_MAIN_APPLICATION_MODULE_CONFIG_STATE_ENUM_INVALID, "cOCTVC1_MAIN_APPLICATION_MODULE_CONFIG_STATE_ENUM_INVALID" },
	{ cOCTVC1_MAIN_APPLICATION_MODULE_CONFIG_STATE_ENUM_DEFAULT, "cOCTVC1_MAIN_APPLICATION_MODULE_CONFIG_STATE_ENUM_DEFAULT" },
	{ cOCTVC1_MAIN_APPLICATION_MODULE_CONFIG_STATE_ENUM_PARSE_OK, "cOCTVC1_MAIN_APPLICATION_MODULE_CONFIG_STATE_ENUM_PARSE_OK" },
	{ cOCTVC1_MAIN_APPLICATION_MODULE_CONFIG_STATE_ENUM_PARSE_ERROR, "cOCTVC1_MAIN_APPLICATION_MODULE_CONFIG_STATE_ENUM_PARSE_ERROR" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_MAIN_HEARTBEAT_STATE_ENUM[] = 
 {
	{ cOCTVC1_MAIN_HEARTBEAT_STATE_ENUM_STOP, "cOCTVC1_MAIN_HEARTBEAT_STATE_ENUM_STOP" },
	{ cOCTVC1_MAIN_HEARTBEAT_STATE_ENUM_START, "cOCTVC1_MAIN_HEARTBEAT_STATE_ENUM_START" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[] = 
 {
	{ cOCTVC1_MAIN_FILE_OPEN_MODE_MASK_READ, "cOCTVC1_MAIN_FILE_OPEN_MODE_MASK_READ" },
	{ cOCTVC1_MAIN_FILE_OPEN_MODE_MASK_WRITE, "cOCTVC1_MAIN_FILE_OPEN_MODE_MASK_WRITE" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_MAIN_LOG_TRACE_STATE_ENUM[] = 
 {
	{ cOCTVC1_MAIN_LOG_TRACE_STATE_ENUM_STOP, "cOCTVC1_MAIN_LOG_TRACE_STATE_ENUM_STOP" },
	{ cOCTVC1_MAIN_LOG_TRACE_STATE_ENUM_START, "cOCTVC1_MAIN_LOG_TRACE_STATE_ENUM_START" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_MAIN_EVT_PHY_MASK[] = 
 {
	{ cOCTVC1_MAIN_EVT_PHY_MASK_UP, "cOCTVC1_MAIN_EVT_PHY_MASK_UP" },
	{ cOCTVC1_MAIN_EVT_PHY_MASK_DOWN, "cOCTVC1_MAIN_EVT_PHY_MASK_DOWN" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_MAIN_APPLICATION_MODULE_INFO[1];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_APPLICATION_MODULE_INFO;

void  register_tOCTVC1_MAIN_APPLICATION_MODULE_INFO(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_APPLICATION_MODULE_INFO[0],
			{ "ulModuleId", "octvc1.main.application_module_info.ulmoduleid",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_MODULE_ID_ENUM), 0x0,
			"ulModuleId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_APPLICATION_MODULE_INFO.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_APPLICATION_MODULE_STATS[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_APPLICATION_MODULE_STATS;

void  register_tOCTVC1_MAIN_APPLICATION_MODULE_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_APPLICATION_MODULE_STATS[0],
			{ "ulState", "octvc1.main.application_module_stats.ulstate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_MAIN_APPLICATION_MODULE_STATE_ENUM), 0x0,
			"ulState",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_APPLICATION_MODULE_STATS[1],
			{ "ulConfigState", "octvc1.main.application_module_stats.ulconfigstate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_MAIN_APPLICATION_MODULE_CONFIG_STATE_ENUM), 0x0,
			"ulConfigState",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_APPLICATION_MODULE_STATS[2],
			{ "ulProcessNum", "octvc1.main.application_module_stats.ulprocessnum",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulProcessNum",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_APPLICATION_MODULE_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG;

void  register_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG[0],
			{ "ulWaitTimeMs", "octvc1.main.api_session_heartbeat_config.ulwaittimems",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulWaitTimeMs",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG[1],
			{ "ulIdleOnlyFlag", "octvc1.main.api_session_heartbeat_config.ulidleonlyflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulIdleOnlyFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS;

void  register_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS[0],
			{ "ulState", "octvc1.main.api_session_heartbeat_stats.ulstate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_MAIN_HEARTBEAT_STATE_ENUM), 0x0,
			"ulState",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS[1],
			{ "ulHeartbeatSentCnt", "octvc1.main.api_session_heartbeat_stats.ulheartbeatsentcnt",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulHeartbeatSentCnt",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR;

void  register_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR[0],
			{ "szFileName", "octvc1.main.file_system_file_cursor.szfilename",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szFileName",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR[1],
			{ "ulGetMode", "octvc1.main.file_system_file_cursor.ulgetmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_OBJECT_CURSOR_ENUM), 0x0,
			"ulGetMode",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[6];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_LOG_TRACE_STATS;

void  register_tOCTVC1_MAIN_LOG_TRACE_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[0],
			{ "ulCurrentFileSize", "octvc1.main.log_trace_stats.ulcurrentfilesize",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulCurrentFileSize",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[1],
			{ "ulCurrentFileIndex", "octvc1.main.log_trace_stats.ulcurrentfileindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulCurrentFileIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[2],
			{ "ulFileOverwriteCnt", "octvc1.main.log_trace_stats.ulfileoverwritecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulFileOverwriteCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[3],
			{ "ulNumFiles", "octvc1.main.log_trace_stats.ulnumfiles",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulNumFiles",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[4],
			{ "abyFileIndexes", "octvc1.main.log_trace_stats.abyfileindexes",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"abyFileIndexes",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[5],
			{ "szFileNamePrefix", "octvc1.main.log_trace_stats.szfilenameprefix",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szFileNamePrefix",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_LOG_TRACE_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_LOG_TRACE_INFO[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_LOG_TRACE_INFO;

void  register_tOCTVC1_MAIN_LOG_TRACE_INFO(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_LOG_TRACE_INFO[0],
			{ "ulMaxFileSize", "octvc1.main.log_trace_info.ulmaxfilesize",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMaxFileSize",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_LOG_TRACE_INFO[1],
			{ "ulMaxNumFiles", "octvc1.main.log_trace_info.ulmaxnumfiles",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMaxNumFiles",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_LOG_TRACE_INFO[2],
			{ "szFileNamePrefix", "octvc1.main.log_trace_info.szfilenameprefix",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szFileNamePrefix",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_LOG_TRACE_INFO.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	Event Registered 
 ****************************************************************************/

int ahf_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT;

void  register_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT[0],
			{ "hProcess", "octvc1.main.process.cpu_usage.report.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT[1],
			{ "ulProcessImageType", "octvc1.main.process.cpu_usage.report.ulprocessimagetype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_PROCESS_TYPE_ENUM), 0x0,
			"ulProcessImageType",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT[2],
			{ "ulProcessCpuUsagePercent", "octvc1.main.process.cpu_usage.report.ulprocesscpuusagepercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulProcessCpuUsagePercent",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT;

void  register_tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT[0],
			{ "hProcess", "octvc1.main.process.dump.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT;

void  register_tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT[0],
			{ "ulState", "octvc1.main.application.state_change.ulstate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_MAIN_APPLICATION_STATE_ENUM), 0x0,
			"ulState",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT[1],
			{ "szAppName", "octvc1.main.application.state_change.szappname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szAppName",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	CMD/RSP Registered 
 ****************************************************************************/

int ahf_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_TARGET_INFO_RSP;

void  register_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP[0],
			{ "ulTargetType", "octvc1.main.target.info.ultargettype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTDEV_DEVICES_TYPE_ENUM), 0x0,
			"ulTargetType",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP[1],
			{ "abyTargetInfo", "octvc1.main.target.info.abytargetinfo",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"abyTargetInfo",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP[2],
			{ "abyUserInfo", "octvc1.main.target.info.abyuserinfo",
			FT_BYTES, BASE_NONE, NULL, 0x0,
			"abyUserInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_TARGET_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_OPEN_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_OPEN_CMD;

void  register_tOCTVC1_MAIN_MSG_FILE_OPEN_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_OPEN_CMD[0],
			{ "szFileName", "octvc1.main.file.open.szfilename",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szFileName",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_OPEN_CMD[1],
			{ "ulAccessMode", "octvc1.main.file.open.ulaccessmode",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulAccessMode",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_OPEN_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_OPEN_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_OPEN_RSP;

void  register_tOCTVC1_MAIN_MSG_FILE_OPEN_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_OPEN_RSP[0],
			{ "hFile", "octvc1.main.file.open.hfile",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hFile",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_OPEN_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_CLOSE_CMD;

void  register_tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD[0],
			{ "hFile", "octvc1.main.file.close.hfile",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hFile",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_CLOSE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_WRITE_CMD;

void  register_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD[0],
			{ "hFile", "octvc1.main.file.write.hfile",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hFile",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD[1],
			{ "ulNumByteToWrite", "octvc1.main.file.write.ulnumbytetowrite",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulNumByteToWrite",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD[2],
			{ "abyData", "octvc1.main.file.write.abydata",
			FT_BYTES, BASE_NONE, NULL, 0x0,
			"abyData",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_WRITE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_WRITE_RSP;

void  register_tOCTVC1_MAIN_MSG_FILE_WRITE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_RSP[0],
			{ "ulNumByteWritten", "octvc1.main.file.write.ulnumbytewritten",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulNumByteWritten",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_WRITE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_READ_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_READ_CMD;

void  register_tOCTVC1_MAIN_MSG_FILE_READ_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_READ_CMD[0],
			{ "hFile", "octvc1.main.file.read.hfile",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hFile",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_READ_CMD[1],
			{ "ulMaxNumByteToRead", "octvc1.main.file.read.ulmaxnumbytetoread",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMaxNumByteToRead",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_READ_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_READ_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_READ_RSP;

void  register_tOCTVC1_MAIN_MSG_FILE_READ_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_READ_RSP[0],
			{ "ulNumByteRead", "octvc1.main.file.read.ulnumbyteread",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulNumByteRead",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_READ_RSP[1],
			{ "abyData", "octvc1.main.file.read.abydata",
			FT_BYTES, BASE_NONE, NULL, 0x0,
			"abyData",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_READ_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_INFO_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_INFO_CMD;

void  register_tOCTVC1_MAIN_MSG_FILE_INFO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_INFO_CMD[0],
			{ "hFile", "octvc1.main.file.info.hfile",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hFile",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_INFO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_INFO_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_INFO_RSP;

void  register_tOCTVC1_MAIN_MSG_FILE_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_INFO_RSP[0],
			{ "hFile", "octvc1.main.file.info.hfile",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hFile",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_INFO_RSP[1],
			{ "szFileName", "octvc1.main.file.info.szfilename",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szFileName",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_INFO_RSP[2],
			{ "ulAccessMode", "octvc1.main.file.info.ulaccessmode",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulAccessMode",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_INFO_RSP[3],
			{ "ulMinAlignBytes", "octvc1.main.file.info.ulminalignbytes",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMinAlignBytes",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_LIST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_LIST_CMD;

void  register_tOCTVC1_MAIN_MSG_FILE_LIST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_LIST_CMD[0],
			{ "ObjectCursor", "octvc1.main.file.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_LIST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_LIST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_LIST_RSP;

void  register_tOCTVC1_MAIN_MSG_FILE_LIST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_LIST_RSP[0],
			{ "ObjectCursor", "octvc1.main.file.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_LIST_RSP[1],
			{ "ObjectList", "octvc1.main.file.list.objectlist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_LIST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP;

void  register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP[0],
			{ "ulMaxFileEntry", "octvc1.main.file_system.info.ulmaxfileentry",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMaxFileEntry",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD;

void  register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD[0],
			{ "ObjectGet", "octvc1.main.file_system.file.info.objectget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[7];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP;

void  register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[0],
			{ "ObjectGet", "octvc1.main.file_system.file.info.objectget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectGet",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[1],
			{ "ulAccessMode", "octvc1.main.file_system.file.info.ulaccessmode",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulAccessMode",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[2],
			{ "ulHostOwnerFlag", "octvc1.main.file_system.file.info.ulhostownerflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulHostOwnerFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[3],
			{ "ulFileSize", "octvc1.main.file_system.file.info.ulfilesize",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFileSize",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[4],
			{ "ulMaxFileSize", "octvc1.main.file_system.file.info.ulmaxfilesize",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulMaxFileSize",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[5],
			{ "ulInstanceNum", "octvc1.main.file_system.file.info.ulinstancenum",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulInstanceNum",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD;

void  register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD[0],
			{ "szFileName", "octvc1.main.file_system.file.add.szfilename",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szFileName",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD[1],
			{ "ulMaxFilesize", "octvc1.main.file_system.file.add.ulmaxfilesize",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulMaxFilesize",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP;

void  register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP[0],
			{ "szFileName", "octvc1.main.file_system.file.add.szfilename",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szFileName",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD;

void  register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD[0],
			{ "szFileName", "octvc1.main.file_system.file.delete.szfilename",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szFileName",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP;

void  register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP[0],
			{ "szFileName", "octvc1.main.file_system.file.delete.szfilename",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szFileName",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_LOG_INFO_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_LOG_INFO_RSP;

void  register_tOCTVC1_MAIN_MSG_LOG_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_INFO_RSP[0],
			{ "LogInfo", "octvc1.main.log.info.loginfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LogInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_LOG_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_LOG_STATS_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_LOG_STATS_RSP;

void  register_tOCTVC1_MAIN_MSG_LOG_STATS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_STATS_RSP[0],
			{ "LogStats", "octvc1.main.log.stats.logstats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LogStats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_LOG_STATS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP;

void  register_tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP[0],
			{ "ulFullAutoStopFlag", "octvc1.main.log.trace.info.ulfullautostopflag",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFullAutoStopFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP[1],
			{ "ulTraceMask", "octvc1.main.log.trace.info.ultracemask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulTraceMask",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP[2],
			{ "TraceInfo", "octvc1.main.log.trace.info.traceinfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TraceInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD;

void  register_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD[0],
			{ "ulFullAutoStopFlag", "octvc1.main.log.trace.start.ulfullautostopflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulFullAutoStopFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD[1],
			{ "ulTraceMask", "octvc1.main.log.trace.start.ultracemask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulTraceMask",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD[2],
			{ "ulFileSize", "octvc1.main.log.trace.start.ulfilesize",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulFileSize",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD[3],
			{ "ulFileNumber", "octvc1.main.log.trace.start.ulfilenumber",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulFileNumber",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP;

void  register_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP[0],
			{ "ulState", "octvc1.main.log.trace.stats.ulstate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_MAIN_LOG_TRACE_STATE_ENUM), 0x0,
			"ulState",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP[1],
			{ "ulFullAutoStopFlag", "octvc1.main.log.trace.stats.ulfullautostopflag",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFullAutoStopFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP[2],
			{ "ulTraceMask", "octvc1.main.log.trace.stats.ultracemask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulTraceMask",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP[3],
			{ "TraceStats", "octvc1.main.log.trace.stats.tracestats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TraceStats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_INFO_CMD;

void  register_tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD[0],
			{ "hProcess", "octvc1.main.process.info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_INFO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_INFO_RSP;

void  register_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP[0],
			{ "hProcess", "octvc1.main.process.info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP[1],
			{ "szName", "octvc1.main.process.info.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP[2],
			{ "szProcessImageName", "octvc1.main.process.info.szprocessimagename",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szProcessImageName",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP[3],
			{ "ulType", "octvc1.main.process.info.ultype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_PROCESS_TYPE_ENUM), 0x0,
			"ulType",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_LIST_CMD;

void  register_tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD[0],
			{ "ObjectCursor", "octvc1.main.process.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_LIST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_LIST_RSP;

void  register_tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP[0],
			{ "ObjectCursor", "octvc1.main.process.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP[1],
			{ "ObjectNameList", "octvc1.main.process.list.objectnamelist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectNameList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_LIST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD;

void  register_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD[0],
			{ "hProcess", "octvc1.main.process.cpu_usage.monitor.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD[1],
			{ "ulAlarmThresholdHighPercent", "octvc1.main.process.cpu_usage.monitor.ulalarmthresholdhighpercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulAlarmThresholdHighPercent",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD[2],
			{ "ulAlarmThresholdLowPercent", "octvc1.main.process.cpu_usage.monitor.ulalarmthresholdlowpercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulAlarmThresholdLowPercent",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD[3],
			{ "ulMonitorEnableFlag", "octvc1.main.process.cpu_usage.monitor.ulmonitorenableflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulMonitorEnableFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP;

void  register_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP[0],
			{ "hProcess", "octvc1.main.process.cpu_usage.monitor.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD;

void  register_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD[0],
			{ "hProcess", "octvc1.main.process.cpu_usage.info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP;

void  register_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP[0],
			{ "hProcess", "octvc1.main.process.cpu_usage.info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP[1],
			{ "ulAlarmThresholdHighPercent", "octvc1.main.process.cpu_usage.info.ulalarmthresholdhighpercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulAlarmThresholdHighPercent",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP[2],
			{ "ulAlarmThresholdLowPercent", "octvc1.main.process.cpu_usage.info.ulalarmthresholdlowpercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulAlarmThresholdLowPercent",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP[3],
			{ "ulMonitorEnableFlag", "octvc1.main.process.cpu_usage.info.ulmonitorenableflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulMonitorEnableFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD;

void  register_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD[0],
			{ "hProcess", "octvc1.main.process.cpu_usage.stats.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP;

void  register_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP[0],
			{ "hProcess", "octvc1.main.process.cpu_usage.stats.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP[1],
			{ "ulProcessCpuUsagePercent", "octvc1.main.process.cpu_usage.stats.ulprocesscpuusagepercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulProcessCpuUsagePercent",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP[2],
			{ "ulProcessCpuUsageMinPercent", "octvc1.main.process.cpu_usage.stats.ulprocesscpuusageminpercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulProcessCpuUsageMinPercent",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP[3],
			{ "ulProcessCpuUsageMaxPercent", "octvc1.main.process.cpu_usage.stats.ulprocesscpuusagemaxpercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulProcessCpuUsageMaxPercent",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD[0],
			{ "IndexGet", "octvc1.main.api_system.session.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP[0],
			{ "IndexGet", "octvc1.main.api_system.session.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP[1],
			{ "IndexList", "octvc1.main.api_system.session.list.indexlist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.info.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.info.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP[1],
			{ "SessionInfo", "octvc1.main.api_system.session.info.sessioninfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SessionInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.evt.info.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.evt.info.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP[1],
			{ "EvtInfo", "octvc1.main.api_system.session.evt.info.evtinfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"EvtInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.evt.modify.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD[1],
			{ "ulEvtActiveFlag", "octvc1.main.api_system.session.evt.modify.ulevtactiveflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulEvtActiveFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.evt.modify.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.info.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.info.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP[1],
			{ "Config", "octvc1.main.api_system.session.heartbeat.info.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.stats.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.stats.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP[1],
			{ "Stats", "octvc1.main.api_system.session.heartbeat.stats.stats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Stats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.start.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD[1],
			{ "Config", "octvc1.main.api_system.session.heartbeat.start.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.start.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.stop.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.stop.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.modify.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD[1],
			{ "Config", "octvc1.main.api_system.session.heartbeat.modify.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP;

void  register_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP[0],
			{ "ulSessionIndex", "octvc1.main.api_system.session.heartbeat.modify.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD;

void  register_tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD[0],
			{ "ulQuit", "octvc1.main.application.stop.ulquit",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulQuit",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP;

void  register_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP[0],
			{ "szName", "octvc1.main.application.info.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP[1],
			{ "szDescription", "octvc1.main.application.info.szdescription",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szDescription",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP[2],
			{ "szVersion", "octvc1.main.application.info.szversion",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szVersion",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP[3],
			{ "abyInfo", "octvc1.main.application.info.abyinfo",
			FT_BYTES, BASE_NONE, NULL, 0x0,
			"abyInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP;

void  register_tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP[0],
			{ "szPlatform", "octvc1.main.application.system.info.szplatform",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szPlatform",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP[1],
			{ "szVersion", "octvc1.main.application.system.info.szversion",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szVersion",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD;

void  register_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD[0],
			{ "ObjectCursor", "octvc1.main.application.module.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP;

void  register_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP[0],
			{ "ObjectCursor", "octvc1.main.application.module.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP[1],
			{ "ObjectNameList", "octvc1.main.application.module.list.objectnamelist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectNameList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD;

void  register_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD[0],
			{ "hModule", "octvc1.main.application.module.info.hmodule",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hModule",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP;

void  register_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP[0],
			{ "hModule", "octvc1.main.application.module.info.hmodule",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hModule",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP[1],
			{ "szName", "octvc1.main.application.module.info.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP[2],
			{ "Info", "octvc1.main.application.module.info.info",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Info",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD;

void  register_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD[0],
			{ "hModule", "octvc1.main.application.module.stats.hmodule",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hModule",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD[1],
			{ "ulResetStatsFlag", "octvc1.main.application.module.stats.ulresetstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetStatsFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP;

void  register_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP[0],
			{ "hModule", "octvc1.main.application.module.stats.hmodule",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hModule",HFILL }

		},
		{
			&ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP[1],
			{ "Stats", "octvc1.main.application.module.stats.stats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Stats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_MAIN_APPLICATION_MODULE_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_APPLICATION_MODULE_INFO)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_APPLICATION_MODULE_INFO (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_APPLICATION_MODULE_INFO));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_MAIN_APPLICATION_MODULE_INFO);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_APPLICATION_MODULE_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_APPLICATION_MODULE_INFO, ulModuleId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_APPLICATION_MODULE_INFO[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_APPLICATION_MODULE_INFO, ulModuleId);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_MAIN_APPLICATION_MODULE_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_APPLICATION_MODULE_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_APPLICATION_MODULE_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_APPLICATION_MODULE_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_MAIN_APPLICATION_MODULE_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_APPLICATION_MODULE_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_APPLICATION_MODULE_STATS, ulState), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_APPLICATION_MODULE_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_APPLICATION_MODULE_STATS, ulState);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_APPLICATION_MODULE_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_APPLICATION_MODULE_STATS, ulConfigState), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_APPLICATION_MODULE_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_APPLICATION_MODULE_STATS, ulConfigState);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_APPLICATION_MODULE_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_APPLICATION_MODULE_STATS, ulProcessNum), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_APPLICATION_MODULE_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_APPLICATION_MODULE_STATS, ulProcessNum);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG, ulWaitTimeMs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG[0], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 500) || ( (unsigned int)temp_data > 10000 ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (500..10000)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG, ulWaitTimeMs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG, ulIdleOnlyFlag), temp_data, "%s (0x%x)", pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG, ulIdleOnlyFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG, ulIdleOnlyFlag);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS, ulState), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS, ulState);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS, ulHeartbeatSentCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS, ulHeartbeatSentCnt);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR);
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR, szFileName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR, szFileName);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR, ulGetMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR, ulGetMode);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_MAIN_LOG_TRACE_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_LOG_TRACE_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_LOG_TRACE_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_LOG_TRACE_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_MAIN_LOG_TRACE_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, ulCurrentFileSize), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, ulCurrentFileSize);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, ulCurrentFileIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, ulCurrentFileIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, ulFileOverwriteCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, ulFileOverwriteCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, ulNumFiles), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, ulNumFiles);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, abyFileIndexes), "abyFileIndexes");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<32; i++ )
			{
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, abyFileIndexes), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[4], tvb, offset,
			1, temp_data, "[%d]: 0x%02x", i, temp_data );
		}
				offset+=1;
			}
		}
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_STATS[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, szFileNamePrefix), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_STATS, szFileNamePrefix);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_MAIN_LOG_TRACE_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_LOG_TRACE_INFO)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_LOG_TRACE_INFO (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_LOG_TRACE_INFO));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_MAIN_LOG_TRACE_INFO);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_INFO, ulMaxFileSize), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_INFO[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_INFO, ulMaxFileSize);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_INFO[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_INFO, ulMaxNumFiles), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_INFO[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_INFO, ulMaxNumFiles);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_LOG_TRACE_INFO[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_INFO, szFileNamePrefix), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_LOG_TRACE_INFO, szFileNamePrefix);

	}


	return offset;

};

/****************************************************************************
	Event dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT, ulProcessImageType);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT, ulProcessCpuUsagePercent);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT, hProcess);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT, ulState);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT, szAppName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT, szAppName);

	}


	return 0;

};

/****************************************************************************
	CMD/RSP dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_TARGET_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_TARGET_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_TARGET_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_TARGET_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_TARGET_INFO_RSP, ulTargetType);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_TARGET_INFO_RSP, abyTargetInfo), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_TARGET_INFO_RSP, abyTargetInfo);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_TARGET_INFO_RSP, abyUserInfo), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_TARGET_INFO_RSP, abyUserInfo);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_OPEN_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_OPEN_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_OPEN_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_OPEN_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_OPEN_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_OPEN_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_OPEN_CMD, szFileName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_OPEN_CMD, szFileName);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_OPEN_CMD[1], tvb, offset,
			4,"ulAccessMode: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK); i++ )
			{
				if( ( vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value && 
					( vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_OPEN_CMD, ulAccessMode);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_OPEN_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_OPEN_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_OPEN_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_OPEN_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_OPEN_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_OPEN_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_OPEN_RSP, hFile);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD, hFile),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD, hFile);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_WRITE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_WRITE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_WRITE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_WRITE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_WRITE_CMD, hFile),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_WRITE_CMD, hFile);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_WRITE_CMD, ulNumByteToWrite);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_WRITE_CMD, abyData), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_WRITE_CMD, abyData);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_WRITE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_WRITE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_WRITE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_WRITE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_WRITE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_WRITE_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_WRITE_RSP, ulNumByteWritten);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_READ_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_READ_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_READ_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_READ_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_READ_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_READ_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_READ_CMD, hFile),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_READ_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_READ_CMD, hFile);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_MAIN_FILE_MAX_DATA_BYTE_SIZE)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_READ_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_READ_CMD, ulMaxNumByteToRead),temp_data, "cOCTVC1_MAIN_FILE_MAX_DATA_BYTE_SIZE (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_READ_CMD[1], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_READ_CMD, ulMaxNumByteToRead);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_READ_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_READ_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_READ_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_READ_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_READ_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_READ_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_READ_RSP, ulNumByteRead);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_READ_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_READ_RSP, abyData), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_READ_RSP, abyData);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_INFO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_INFO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_INFO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_INFO_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_INFO_CMD, hFile),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_INFO_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_INFO_CMD, hFile);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_INFO_RSP, hFile);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_INFO_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_INFO_RSP, szFileName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_INFO_RSP, szFileName);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_INFO_RSP[2], tvb, offset,
			4,"ulAccessMode: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK); i++ )
			{
				if( ( vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value && 
					( vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_INFO_RSP, ulAccessMode);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_INFO_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_INFO_RSP, ulMinAlignBytes);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_LIST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_LIST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_LIST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_LIST_CMD, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_LIST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_LIST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_LIST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_LIST_RSP, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_LIST_RSP, ObjectList), "ObjectList:tOCTVC1_LIST_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP, ulMaxFileEntry);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD, ObjectGet), "ObjectGet:tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP, ObjectGet), "ObjectGet:tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[1], tvb, offset,
			4,"ulAccessMode: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK); i++ )
			{
				if( ( vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value && 
					( vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_MAIN_FILE_OPEN_MODE_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP, ulAccessMode);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP, ulHostOwnerFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP, ulHostOwnerFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP, ulFileSize);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP, ulMaxFileSize);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP, ulInstanceNum);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD, szFileName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD, szFileName);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD, ulMaxFilesize);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP, szFileName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP, szFileName);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD, szFileName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD, szFileName);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP, szFileName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP, szFileName);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_LOG_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_LOG_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_LOG_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_LOG_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_LOG_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_INFO_RSP, LogInfo), "LogInfo:tOCTVC1_MAIN_LOG_TRACE_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_LOG_TRACE_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_LOG_STATS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_LOG_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_LOG_STATS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_LOG_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_LOG_STATS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_STATS_RSP, LogStats), "LogStats:tOCTVC1_MAIN_LOG_TRACE_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_LOG_TRACE_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP, ulFullAutoStopFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP[1], tvb, offset,
			4,"ulTraceMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_LOG_TRACE_MASK); i++ )
			{
				if( ( vals_tOCTVC1_LOG_TRACE_MASK[i].value && 
					( vals_tOCTVC1_LOG_TRACE_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_LOG_TRACE_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_LOG_TRACE_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_LOG_TRACE_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP, ulTraceMask);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP, TraceInfo), "TraceInfo:tOCTVC1_MAIN_LOG_TRACE_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_LOG_TRACE_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD, ulFullAutoStopFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD, ulFullAutoStopFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD[1], tvb, offset,
			4,"ulTraceMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_LOG_TRACE_MASK); i++ )
			{
				if( ( vals_tOCTVC1_LOG_TRACE_MASK[i].value && 
					( vals_tOCTVC1_LOG_TRACE_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_LOG_TRACE_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_LOG_TRACE_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_LOG_TRACE_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD, ulTraceMask);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD, ulFileSize);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD, ulFileNumber);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP, ulState);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP, ulFullAutoStopFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP[2], tvb, offset,
			4,"ulTraceMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_LOG_TRACE_MASK); i++ )
			{
				if( ( vals_tOCTVC1_LOG_TRACE_MASK[i].value && 
					( vals_tOCTVC1_LOG_TRACE_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_LOG_TRACE_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_LOG_TRACE_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_LOG_TRACE_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP, ulTraceMask);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP, TraceStats), "TraceStats:tOCTVC1_MAIN_LOG_TRACE_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_LOG_TRACE_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD, hProcess),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD, hProcess);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP, hProcess);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP, szName);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP, szProcessImageName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP, szProcessImageName);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP, ulType);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP, ObjectNameList), "ObjectNameList:tOCTVC1_LIST_NAME_OBJECT32_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_NAME_OBJECT32_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD[1], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > 100 ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..100)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD, ulAlarmThresholdHighPercent);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD[2], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > 100 ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..100)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD, ulAlarmThresholdLowPercent);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_DO_NOT_MODIFY)
		{		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD, ulMonitorEnableFlag),temp_data, "cOCTVC1_DO_NOT_MODIFY (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD, ulMonitorEnableFlag), temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD, ulMonitorEnableFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP, hProcess);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD, hProcess);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP, ulAlarmThresholdHighPercent);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP, ulAlarmThresholdLowPercent);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP, ulMonitorEnableFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP, ulMonitorEnableFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD, hProcess);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP, ulProcessCpuUsagePercent);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP, ulProcessCpuUsageMinPercent);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP, ulProcessCpuUsageMaxPercent);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP, IndexList), "IndexList:tOCTVC1_LIST_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP, ulSessionIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP, SessionInfo), "SessionInfo:tOCTVC1_API_SESSION_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_API_SESSION_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP, ulSessionIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP, EvtInfo), "EvtInfo:tOCTVC1_API_SESSION_EVT_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_API_SESSION_EVT_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD, ulSessionIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_DO_NOT_MODIFY)
		{		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD, ulEvtActiveFlag),temp_data, "cOCTVC1_DO_NOT_MODIFY (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD, ulEvtActiveFlag), temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD, ulEvtActiveFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP, ulSessionIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP, Config), "Config:tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP, ulSessionIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP, Stats), "Stats:tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD, ulSessionIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD, Config), "Config:tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD, ulSessionIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD, Config), "Config:tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD, ulQuit), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD, ulQuit);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP, szName);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP, szDescription), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP, szDescription);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP, szVersion), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP, szVersion);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP, abyInfo), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP, abyInfo);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP, szPlatform), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP, szPlatform);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP, szVersion), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP, szVersion);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP, ObjectNameList), "ObjectNameList:tOCTVC1_LIST_NAME_OBJECT32_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_NAME_OBJECT32_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD, hModule);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP, hModule);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP, szName);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP, Info), "Info:tOCTVC1_MAIN_APPLICATION_MODULE_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_APPLICATION_MODULE_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD, hModule);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD, ulResetStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD, ulResetStatsFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP, hModule);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP, Stats), "Stats:tOCTVC1_MAIN_APPLICATION_MODULE_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MAIN_APPLICATION_MODULE_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};
/****************************************************************************
	MODULE REGISTERED EXPORTED FUNCTION
 ****************************************************************************/

void ws_register_OCTVC1_MAIN(void)
{
	/****************************************************************************
		Register Common struct
	****************************************************************************/
	register_tOCTVC1_MAIN_APPLICATION_MODULE_INFO(); 
	register_tOCTVC1_MAIN_APPLICATION_MODULE_STATS(); 
	register_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_CONFIG(); 
	register_tOCTVC1_MAIN_API_SESSION_HEARTBEAT_STATS(); 
	register_tOCTVC1_MAIN_FILE_SYSTEM_FILE_CURSOR(); 
	register_tOCTVC1_MAIN_LOG_TRACE_STATS(); 
	register_tOCTVC1_MAIN_LOG_TRACE_INFO(); 

	/****************************************************************************
		CMD/RSP Registered 
	****************************************************************************/
	register_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP(); 
	register_tOCTVC1_MAIN_MSG_FILE_OPEN_CMD(); 
	register_tOCTVC1_MAIN_MSG_FILE_OPEN_RSP(); 
	register_tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD(); 
	register_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD(); 
	register_tOCTVC1_MAIN_MSG_FILE_WRITE_RSP(); 
	register_tOCTVC1_MAIN_MSG_FILE_READ_CMD(); 
	register_tOCTVC1_MAIN_MSG_FILE_READ_RSP(); 
	register_tOCTVC1_MAIN_MSG_FILE_INFO_CMD(); 
	register_tOCTVC1_MAIN_MSG_FILE_INFO_RSP(); 
	register_tOCTVC1_MAIN_MSG_FILE_LIST_CMD(); 
	register_tOCTVC1_MAIN_MSG_FILE_LIST_RSP(); 
	register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP(); 
	register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD(); 
	register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP(); 
	register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD(); 
	register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP(); 
	register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD(); 
	register_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP(); 
	register_tOCTVC1_MAIN_MSG_LOG_INFO_RSP(); 
	register_tOCTVC1_MAIN_MSG_LOG_STATS_RSP(); 
	register_tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP(); 
	register_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD(); 
	register_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP(); 
	register_tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD(); 
	register_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP(); 
	register_tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP(); 
	register_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD(); 
	register_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP(); 
	register_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD(); 
	register_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP(); 
	register_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD(); 
	register_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP(); 

	/****************************************************************************
		Event Registered 
	****************************************************************************/
	register_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT(); 
	register_tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT(); 
	register_tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT(); 
	register_tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT(); 

}

/****************************************************************************
	MODULE DISSECTOR FUNCTIONS
 ****************************************************************************/
int ws_dissect_OCTVC1_MAIN_CMD( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_MAIN_MSG_TARGET_INFO_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_TARGET_RESET_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_FILE_OPEN_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_OPEN_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_CLOSE_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_CLOSE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_WRITE_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_WRITE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_READ_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_READ_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_INFO_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_INFO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_LIST_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_LIST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_LOG_ERASE_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_LOG_INFO_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_LOG_STATS_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_LOG_INFO_TRACE_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_LOG_START_TRACE_CID: return dissect_tOCTVC1_MAIN_MSG_LOG_START_TRACE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_LOG_STOP_TRACE_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_LOG_ERASE_TRACE_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_LOG_STATS_TRACE_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_PROCESS_INFO_CID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_INFO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_PROCESS_LIST_CID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_LIST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_APPLICATION_START_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_APPLICATION_STOP_CID: return dissect_tOCTVC1_MAIN_MSG_APPLICATION_STOP_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_APPLICATION_INFO_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CID: return dissect_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CID: return dissect_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CID: return dissect_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CMD( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_MAIN_RSP( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_MAIN_MSG_TARGET_INFO_CID: return dissect_tOCTVC1_MAIN_MSG_TARGET_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_TARGET_RESET_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_FILE_OPEN_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_OPEN_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_CLOSE_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_FILE_WRITE_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_WRITE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_READ_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_READ_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_INFO_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_LIST_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_LIST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CID: return dissect_tOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_LOG_ERASE_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_LOG_INFO_CID: return dissect_tOCTVC1_MAIN_MSG_LOG_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_LOG_STATS_CID: return dissect_tOCTVC1_MAIN_MSG_LOG_STATS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_LOG_INFO_TRACE_CID: return dissect_tOCTVC1_MAIN_MSG_LOG_INFO_TRACE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_LOG_START_TRACE_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_LOG_STOP_TRACE_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_LOG_ERASE_TRACE_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_LOG_STATS_TRACE_CID: return dissect_tOCTVC1_MAIN_MSG_LOG_STATS_TRACE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_PROCESS_INFO_CID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_PROCESS_LIST_CID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_LIST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_APPLICATION_START_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_APPLICATION_STOP_CID: return 0; break;
			case cOCTVC1_MAIN_MSG_APPLICATION_INFO_CID: return dissect_tOCTVC1_MAIN_MSG_APPLICATION_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_CID: return dissect_tOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CID: return dissect_tOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CID: return dissect_tOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CID: return dissect_tOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_RSP( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_MAIN_EVT( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EVT( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_PROCESS_DUMP_EID: return dissect_tOCTVC1_MAIN_MSG_PROCESS_DUMP_EVT( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EID: return dissect_tOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EVT( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EID: return dissect_tOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EVT( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}

/****************************************************************************
	MODULE DISSECTOR EXPORTED FUNCTION
 ****************************************************************************/

int ws_dissect_OCTVC1_MAIN( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if (message_type == cOCTVC1_MSG_TYPE_RESPONSE)
		return ws_dissect_OCTVC1_MAIN_RSP( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_COMMAND)
		return ws_dissect_OCTVC1_MAIN_CMD( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_NOTIFICATION )
		return ws_dissect_OCTVC1_MAIN_EVT( CID, tvb, pinfo, tree);
	else
		return 1;

}

