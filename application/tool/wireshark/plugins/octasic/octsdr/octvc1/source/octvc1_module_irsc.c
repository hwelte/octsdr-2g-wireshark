/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: octvc1_module_irsc.c

      
Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark module dissector related functions

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/

#include <config.h>
#include <epan/packet.h>
#include "../../../include/oct_ws_macro.h"
#include "../include/module.h"
#include "../include/octvc1_common.h"

#include <irsc/octvc1_irsc_evt_priv.h>


/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_IRSC_IPC_PORT_TYPE_ENUM[] = 
 {
	{ cOCTVC1_IRSC_IPC_PORT_TYPE_ENUM_INVALID, "cOCTVC1_IRSC_IPC_PORT_TYPE_ENUM_INVALID" },
	{ cOCTVC1_IRSC_IPC_PORT_TYPE_ENUM_UNIDIR_RECV, "cOCTVC1_IRSC_IPC_PORT_TYPE_ENUM_UNIDIR_RECV" },
	{ cOCTVC1_IRSC_IPC_PORT_TYPE_ENUM_UNIDIR_SEND, "cOCTVC1_IRSC_IPC_PORT_TYPE_ENUM_UNIDIR_SEND" },
	{ cOCTVC1_IRSC_IPC_PORT_TYPE_ENUM_BIDIR, "cOCTVC1_IRSC_IPC_PORT_TYPE_ENUM_BIDIR" },
	{ cOCTVC1_IRSC_IPC_PORT_TYPE_ENUM_LOCAL, "cOCTVC1_IRSC_IPC_PORT_TYPE_ENUM_LOCAL" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_IRSC_IPC_PORT_STATE_ENUM[] = 
 {
	{ cOCTVC1_IRSC_IPC_PORT_STATE_ENUM_INVALID, "cOCTVC1_IRSC_IPC_PORT_STATE_ENUM_INVALID" },
	{ cOCTVC1_IRSC_IPC_PORT_STATE_ENUM_ERROR, "cOCTVC1_IRSC_IPC_PORT_STATE_ENUM_ERROR" },
	{ cOCTVC1_IRSC_IPC_PORT_STATE_ENUM_CONFIG, "cOCTVC1_IRSC_IPC_PORT_STATE_ENUM_CONFIG" },
	{ cOCTVC1_IRSC_IPC_PORT_STATE_ENUM_WAIT_REMOTE, "cOCTVC1_IRSC_IPC_PORT_STATE_ENUM_WAIT_REMOTE" },
	{ cOCTVC1_IRSC_IPC_PORT_STATE_ENUM_READY, "cOCTVC1_IRSC_IPC_PORT_STATE_ENUM_READY" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_IRSC_APPLICATION_TAP_STATE_ENUM[] = 
 {
	{ cOCTVC1_IRSC_APPLICATION_TAP_STATE_ENUM_DISABLE, "cOCTVC1_IRSC_APPLICATION_TAP_STATE_ENUM_DISABLE" },
	{ cOCTVC1_IRSC_APPLICATION_TAP_STATE_ENUM_STOP, "cOCTVC1_IRSC_APPLICATION_TAP_STATE_ENUM_STOP" },
	{ cOCTVC1_IRSC_APPLICATION_TAP_STATE_ENUM_START, "cOCTVC1_IRSC_APPLICATION_TAP_STATE_ENUM_START" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_IRSC_APPLICATION_TAP_MODE_ENUM[] = 
 {
	{ cOCTVC1_IRSC_APPLICATION_TAP_MODE_ENUM_STREAM, "cOCTVC1_IRSC_APPLICATION_TAP_MODE_ENUM_STREAM" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_IRSC_API_STATS[6];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_API_STATS;

void  register_tOCTVC1_IRSC_API_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_API_STATS[0],
			{ "ulMaxProcessingTimeUs", "octvc1.irsc.api_stats.ulmaxprocessingtimeus",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMaxProcessingTimeUs",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_API_STATS[1],
			{ "ulMinProcessingTimeUs", "octvc1.irsc.api_stats.ulminprocessingtimeus",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMinProcessingTimeUs",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_API_STATS[2],
			{ "ulTotalProcessingTimeUs", "octvc1.irsc.api_stats.ultotalprocessingtimeus",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTotalProcessingTimeUs",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_API_STATS[3],
			{ "ulCmdCnt", "octvc1.irsc.api_stats.ulcmdcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulCmdCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_API_STATS[4],
			{ "ulL1CacheMissCnt", "octvc1.irsc.api_stats.ull1cachemisscnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulL1CacheMissCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_API_STATS[5],
			{ "ulTlbCacheMissCnt", "octvc1.irsc.api_stats.ultlbcachemisscnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTlbCacheMissCnt",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_API_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_SESSION_STATS[10];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_SESSION_STATS;

void  register_tOCTVC1_IRSC_SESSION_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_SESSION_STATS[0],
			{ "ulLastCmdTimestamp", "octvc1.irsc.session_stats.ullastcmdtimestamp",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulLastCmdTimestamp",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_SESSION_STATS[1],
			{ "ulExpTransactionId", "octvc1.irsc.session_stats.ulexptransactionid",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulExpTransactionId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_SESSION_STATS[2],
			{ "ulAcceptedCmdCnt", "octvc1.irsc.session_stats.ulacceptedcmdcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulAcceptedCmdCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_SESSION_STATS[3],
			{ "ulRejectedCmdCnt", "octvc1.irsc.session_stats.ulrejectedcmdcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRejectedCmdCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_SESSION_STATS[4],
			{ "ulRetryCmdCnt", "octvc1.irsc.session_stats.ulretrycmdcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRetryCmdCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_SESSION_STATS[5],
			{ "ulResyncCnt", "octvc1.irsc.session_stats.ulresynccnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulResyncCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_SESSION_STATS[6],
			{ "ulSessionCnt", "octvc1.irsc.session_stats.ulsessioncnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_SESSION_STATS[7],
			{ "ulEvtSentCnt", "octvc1.irsc.session_stats.ulevtsentcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulEvtSentCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_SESSION_STATS[8],
			{ "ulEvtSentErrCnt", "octvc1.irsc.session_stats.ulevtsenterrcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulEvtSentErrCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_SESSION_STATS[9],
			{ "ulLastEvtTimestamp", "octvc1.irsc.session_stats.ullastevttimestamp",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulLastEvtTimestamp",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_SESSION_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[6];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_APPLICATION_TAP_INFO;

void  register_tOCTVC1_IRSC_APPLICATION_TAP_INFO(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[0],
			{ "ulModuleId", "octvc1.irsc.application_tap_info.ulmoduleid",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_MODULE_ID_ENUM), 0x0,
			"ulModuleId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[1],
			{ "hProcess", "octvc1.irsc.application_tap_info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[2],
			{ "ulDirection", "octvc1.irsc.application_tap_info.uldirection",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_TAP_DIRECTION_ENUM), 0x0,
			"ulDirection",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[3],
			{ "ulDataSubType", "octvc1.irsc.application_tap_info.uldatasubtype",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulDataSubType",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[4],
			{ "szName", "octvc1.irsc.application_tap_info.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[5],
			{ "szDescription", "octvc1.irsc.application_tap_info.szdescription",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szDescription",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_APPLICATION_TAP_INFO.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_APPLICATION_TAP_STATS[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_APPLICATION_TAP_STATS;

void  register_tOCTVC1_IRSC_APPLICATION_TAP_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_STATS[0],
			{ "ulState", "octvc1.irsc.application_tap_stats.ulstate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_IRSC_APPLICATION_TAP_STATE_ENUM), 0x0,
			"ulState",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_STATS[1],
			{ "ulFilterIndex", "octvc1.irsc.application_tap_stats.ulfilterindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFilterIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_STATS[2],
			{ "ulUserId", "octvc1.irsc.application_tap_stats.uluserid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulUserId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_APPLICATION_TAP_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_APPLICATION_TAP_START;

void  register_tOCTVC1_IRSC_APPLICATION_TAP_START(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[0],
			{ "ulMode", "octvc1.irsc.application_tap_start.ulmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_IRSC_APPLICATION_TAP_MODE_ENUM), 0x0,
			"ulMode",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[1],
			{ "ulMaxTransportDataSize", "octvc1.irsc.application_tap_start.ulmaxtransportdatasize",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulMaxTransportDataSize",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[2],
			{ "ulFilterIndex", "octvc1.irsc.application_tap_start.ulfilterindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFilterIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[3],
			{ "ulUserId", "octvc1.irsc.application_tap_start.uluserid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulUserId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_APPLICATION_TAP_START.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	Event Registered 
 ****************************************************************************/

int ahf_tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT;

void  register_tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT[0],
			{ "hProcess", "octvc1.irsc.process.dump.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	CMD/RSP Registered 
 ****************************************************************************/

int ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_CMD;

void  register_tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD[0],
			{ "hProcess", "octvc1.irsc.process.info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_RSP;

void  register_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP[0],
			{ "hProcess", "octvc1.irsc.process.info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP[1],
			{ "szName", "octvc1.irsc.process.info.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP[2],
			{ "szProcessImageName", "octvc1.irsc.process.info.szprocessimagename",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szProcessImageName",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP[3],
			{ "ulType", "octvc1.irsc.process.info.ultype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_PROCESS_TYPE_ENUM), 0x0,
			"ulType",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_CMD;

void  register_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD[0],
			{ "hProcess", "octvc1.irsc.process.stats.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD[1],
			{ "ulResetProcessStatsFlag", "octvc1.irsc.process.stats.ulresetprocessstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetProcessStatsFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD[2],
			{ "ulResetIpcStatsFlag", "octvc1.irsc.process.stats.ulresetipcstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetIpcStatsFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD[3],
			{ "ulResetTaskStatsFlag", "octvc1.irsc.process.stats.ulresettaskstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetTaskStatsFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_RSP;

void  register_tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP[0],
			{ "hProcess", "octvc1.irsc.process.stats.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP[1],
			{ "State", "octvc1.irsc.process.stats.state",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_PROCESS_STATE_ENUM), 0x0,
			"State",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP[2],
			{ "Error", "octvc1.irsc.process.stats.error",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Error",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_CMD;

void  register_tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD[0],
			{ "ObjectCursor", "octvc1.irsc.process.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_RSP;

void  register_tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP[0],
			{ "ObjectCursor", "octvc1.irsc.process.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP[1],
			{ "ObjectNameList", "octvc1.irsc.process.list.objectnamelist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectNameList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD;

void  register_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD[0],
			{ "hProcess", "octvc1.irsc.process.ipc_port.info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD[1],
			{ "ulPortId", "octvc1.irsc.process.ipc_port.info.ulportid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPortId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[14];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP;

void  register_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[0],
			{ "hProcess", "octvc1.irsc.process.ipc_port.info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[1],
			{ "ulPortId", "octvc1.irsc.process.ipc_port.info.ulportid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPortId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[2],
			{ "ulUserId", "octvc1.irsc.process.ipc_port.info.uluserid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulUserId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[3],
			{ "hRemoteProcess", "octvc1.irsc.process.ipc_port.info.hremoteprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hRemoteProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[4],
			{ "ulRemotePortId", "octvc1.irsc.process.ipc_port.info.ulremoteportid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulRemotePortId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[5],
			{ "ulRemoteUserId", "octvc1.irsc.process.ipc_port.info.ulremoteuserid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulRemoteUserId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[6],
			{ "szName", "octvc1.irsc.process.ipc_port.info.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[7],
			{ "ulPortIdx", "octvc1.irsc.process.ipc_port.info.ulportidx",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIdx",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[8],
			{ "ulType", "octvc1.irsc.process.ipc_port.info.ultype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_IRSC_IPC_PORT_TYPE_ENUM), 0x0,
			"ulType",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[9],
			{ "ulSystemFlag", "octvc1.irsc.process.ipc_port.info.ulsystemflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulSystemFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[10],
			{ "ulGhostFlag", "octvc1.irsc.process.ipc_port.info.ulghostflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulGhostFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[11],
			{ "ulRecvMsgNum", "octvc1.irsc.process.ipc_port.info.ulrecvmsgnum",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRecvMsgNum",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[12],
			{ "ulRecvMsgSize", "octvc1.irsc.process.ipc_port.info.ulrecvmsgsize",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRecvMsgSize",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD;

void  register_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD[0],
			{ "hProcess", "octvc1.irsc.process.ipc_port.stats.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD[1],
			{ "ulPortId", "octvc1.irsc.process.ipc_port.stats.ulportid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPortId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD[2],
			{ "ulResetStatsFlag", "octvc1.irsc.process.ipc_port.stats.ulresetstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetStatsFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[7];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP;

void  register_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[0],
			{ "hProcess", "octvc1.irsc.process.ipc_port.stats.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[1],
			{ "ulPortId", "octvc1.irsc.process.ipc_port.stats.ulportid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPortId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[2],
			{ "ulState", "octvc1.irsc.process.ipc_port.stats.ulstate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_IRSC_IPC_PORT_STATE_ENUM), 0x0,
			"ulState",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[3],
			{ "ulSendMsgCnt", "octvc1.irsc.process.ipc_port.stats.ulsendmsgcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSendMsgCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[4],
			{ "ulRecvMsgCnt", "octvc1.irsc.process.ipc_port.stats.ulrecvmsgcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRecvMsgCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[5],
			{ "ulRecvMsgMissCnt", "octvc1.irsc.process.ipc_port.stats.ulrecvmsgmisscnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRecvMsgMissCnt",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD;

void  register_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD[0],
			{ "SubObjectIdGet", "octvc1.irsc.process.ipc_port.list.subobjectidget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SubObjectIdGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP;

void  register_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP[0],
			{ "SubObjectIdGet", "octvc1.irsc.process.ipc_port.list.subobjectidget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SubObjectIdGet",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP[1],
			{ "ObjectNameList", "octvc1.irsc.process.ipc_port.list.objectnamelist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectNameList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD;

void  register_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD[0],
			{ "hProcess", "octvc1.irsc.process.task.info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD[1],
			{ "ulTaskIndex", "octvc1.irsc.process.task.info.ultaskindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTaskIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP;

void  register_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP[0],
			{ "hProcess", "octvc1.irsc.process.task.info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP[1],
			{ "ulTaskIndex", "octvc1.irsc.process.task.info.ultaskindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTaskIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP[2],
			{ "szName", "octvc1.irsc.process.task.info.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD;

void  register_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD[0],
			{ "hProcess", "octvc1.irsc.process.task.stats.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD[1],
			{ "ulTaskIndex", "octvc1.irsc.process.task.stats.ultaskindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTaskIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD[2],
			{ "ulResetStatsFlag", "octvc1.irsc.process.task.stats.ulresetstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetStatsFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP;

void  register_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP[0],
			{ "hProcess", "octvc1.irsc.process.task.stats.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP[1],
			{ "ulTaskIndex", "octvc1.irsc.process.task.stats.ultaskindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTaskIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP[2],
			{ "Stats", "octvc1.irsc.process.task.stats.stats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Stats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD;

void  register_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD[0],
			{ "SubObjectIdGet", "octvc1.irsc.process.task.list.subobjectidget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SubObjectIdGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP;

void  register_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP[0],
			{ "SubObjectIdGet", "octvc1.irsc.process.task.list.subobjectidget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SubObjectIdGet",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP[1],
			{ "ObjectNameList", "octvc1.irsc.process.task.list.objectnamelist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectNameList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD;

void  register_tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD[0],
			{ "ulObjType", "octvc1.irsc.objmgr.info.ulobjtype",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulObjType",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP;

void  register_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP[0],
			{ "ulObjType", "octvc1.irsc.objmgr.info.ulobjtype",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulObjType",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP[1],
			{ "ulMaxNumObj", "octvc1.irsc.objmgr.info.ulmaxnumobj",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMaxNumObj",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP[2],
			{ "ulObjByteSize", "octvc1.irsc.objmgr.info.ulobjbytesize",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulObjByteSize",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP[3],
			{ "szName", "octvc1.irsc.objmgr.info.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD;

void  register_tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD[0],
			{ "ulObjType", "octvc1.irsc.objmgr.stats.ulobjtype",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulObjType",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP;

void  register_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP[0],
			{ "ulObjType", "octvc1.irsc.objmgr.stats.ulobjtype",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulObjType",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP[1],
			{ "ulMaxNumObj", "octvc1.irsc.objmgr.stats.ulmaxnumobj",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMaxNumObj",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP[2],
			{ "ulNumObj", "octvc1.irsc.objmgr.stats.ulnumobj",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulNumObj",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD;

void  register_tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD[0],
			{ "ObjectCursor", "octvc1.irsc.objmgr.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP;

void  register_tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP[0],
			{ "ObjectCursor", "octvc1.irsc.objmgr.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP[1],
			{ "ObjectNameList", "octvc1.irsc.objmgr.list.objectnamelist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectNameList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD[0],
			{ "ulResetStatsFlag", "octvc1.irsc.api_system.monitoring.start.ulresetstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetStatsFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD[0],
			{ "ObjectCursor", "octvc1.irsc.api_system.command.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP[0],
			{ "ObjectCursor", "octvc1.irsc.api_system.command.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP[1],
			{ "ObjectList", "octvc1.irsc.api_system.command.list.objectlist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD[0],
			{ "hCmdId", "octvc1.irsc.api_system.command.stats.hcmdid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hCmdId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD[1],
			{ "ulResetStatsFlag", "octvc1.irsc.api_system.command.stats.ulresetstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetStatsFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP[0],
			{ "hCmdId", "octvc1.irsc.api_system.command.stats.hcmdid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hCmdId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP[1],
			{ "CommandStats", "octvc1.irsc.api_system.command.stats.commandstats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"CommandStats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD[0],
			{ "IndexGet", "octvc1.irsc.api_system.session.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP[0],
			{ "IndexGet", "octvc1.irsc.api_system.session.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP[1],
			{ "IndexList", "octvc1.irsc.api_system.session.list.indexlist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD[0],
			{ "ulSessionIndex", "octvc1.irsc.api_system.session.info.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP[0],
			{ "ulSessionIndex", "octvc1.irsc.api_system.session.info.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP[1],
			{ "SessionInfo", "octvc1.irsc.api_system.session.info.sessioninfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SessionInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD[0],
			{ "ulSessionIndex", "octvc1.irsc.api_system.session.stats.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD[1],
			{ "ulResetStatsFlag", "octvc1.irsc.api_system.session.stats.ulresetstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetStatsFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP[0],
			{ "ulSessionIndex", "octvc1.irsc.api_system.session.stats.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP[1],
			{ "SessionStats", "octvc1.irsc.api_system.session.stats.sessionstats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SessionStats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD[0],
			{ "ulSessionIndex", "octvc1.irsc.api_system.session.evt.info.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP[0],
			{ "ulSessionIndex", "octvc1.irsc.api_system.session.evt.info.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP[1],
			{ "EvtInfo", "octvc1.irsc.api_system.session.evt.info.evtinfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"EvtInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD[0],
			{ "ulSessionIndex", "octvc1.irsc.api_system.session.evt.stats.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP;

void  register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP[0],
			{ "ulSessionIndex", "octvc1.irsc.api_system.session.evt.stats.ulsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP[1],
			{ "Stats", "octvc1.irsc.api_system.session.evt.stats.stats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Stats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD;

void  register_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD[0],
			{ "ObjectCursor", "octvc1.irsc.application.tap.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP;

void  register_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP[0],
			{ "ObjectCursor", "octvc1.irsc.application.tap.list.objectcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP[1],
			{ "ObjectNameList", "octvc1.irsc.application.tap.list.objectnamelist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ObjectNameList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD;

void  register_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD[0],
			{ "hTapId", "octvc1.irsc.application.tap.info.htapid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTapId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP;

void  register_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP[0],
			{ "hTapId", "octvc1.irsc.application.tap.info.htapid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTapId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP[1],
			{ "Info", "octvc1.irsc.application.tap.info.info",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Info",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD;

void  register_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD[0],
			{ "hTapId", "octvc1.irsc.application.tap.stats.htapid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTapId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP;

void  register_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP[0],
			{ "hTapId", "octvc1.irsc.application.tap.stats.htapid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTapId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP[1],
			{ "Stats", "octvc1.irsc.application.tap.stats.stats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Stats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD;

void  register_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD[0],
			{ "hTapId", "octvc1.irsc.application.tap.start.htapid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTapId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD[1],
			{ "Start", "octvc1.irsc.application.tap.start.start",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Start",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP;

void  register_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP[0],
			{ "hTapId", "octvc1.irsc.application.tap.start.htapid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTapId",HFILL }

		},
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP[1],
			{ "hLogicalObj", "octvc1.irsc.application.tap.start.hlogicalobj",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hLogicalObj",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD;

void  register_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD[0],
			{ "hTapId", "octvc1.irsc.application.tap.stop.htapid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTapId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP;

void  register_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP[0],
			{ "hTapId", "octvc1.irsc.application.tap.stop.htapid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hTapId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_IRSC_API_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_API_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_API_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_API_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_IRSC_API_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_API_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulMaxProcessingTimeUs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_API_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulMaxProcessingTimeUs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_API_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulMinProcessingTimeUs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_API_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulMinProcessingTimeUs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_API_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulTotalProcessingTimeUs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_API_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulTotalProcessingTimeUs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_API_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulCmdCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_API_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulCmdCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_API_STATS[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulL1CacheMissCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_API_STATS[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulL1CacheMissCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_API_STATS[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulTlbCacheMissCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_API_STATS[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_API_STATS, ulTlbCacheMissCnt);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_IRSC_SESSION_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_SESSION_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_SESSION_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_SESSION_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_IRSC_SESSION_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulLastCmdTimestamp), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulLastCmdTimestamp);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulExpTransactionId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulExpTransactionId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulAcceptedCmdCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulAcceptedCmdCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulRejectedCmdCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulRejectedCmdCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulRetryCmdCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulRetryCmdCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulResyncCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulResyncCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulSessionCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[6], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulSessionCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[7], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulEvtSentCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[7], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulEvtSentCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[8], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulEvtSentErrCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[8], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulEvtSentErrCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[9], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulLastEvtTimestamp), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_SESSION_STATS[9], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_SESSION_STATS, ulLastEvtTimestamp);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_IRSC_APPLICATION_TAP_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_APPLICATION_TAP_INFO)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_APPLICATION_TAP_INFO (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_APPLICATION_TAP_INFO));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_IRSC_APPLICATION_TAP_INFO);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, ulModuleId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, ulModuleId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, hProcess), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, ulDirection), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, ulDirection);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, ulDataSubType), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, ulDataSubType);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, szName);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_INFO[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, szDescription), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_INFO, szDescription);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_IRSC_APPLICATION_TAP_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_APPLICATION_TAP_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_APPLICATION_TAP_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_APPLICATION_TAP_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_IRSC_APPLICATION_TAP_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_STATS, ulState), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_STATS, ulState);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_STATS, ulFilterIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_STATS, ulFilterIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_STATS, ulUserId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_STATS, ulUserId);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_IRSC_APPLICATION_TAP_START(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_APPLICATION_TAP_START)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_APPLICATION_TAP_START (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_APPLICATION_TAP_START));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_IRSC_APPLICATION_TAP_START);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_START, ulMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_START, ulMode);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_IRSC_APPLICATION_TRANSPORT_MAX_DATA_SIZE)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_START, ulMaxTransportDataSize),temp_data, "cOCTVC1_IRSC_APPLICATION_TRANSPORT_MAX_DATA_SIZE (0x%08x)",temp_data);
		}else {
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_START, ulMaxTransportDataSize), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[1], tvb, offset,
			4, temp_data);
		if( (unsigned int)temp_data < 128) 
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (128..)]" );
		}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_START, ulMaxTransportDataSize);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_INDEX_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_START, ulFilterIndex),temp_data, "cOCTVC1_INDEX_INVALID (0x%08x)",temp_data);
		}else {
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_START, ulFilterIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[2], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_START, ulFilterIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_START, ulUserId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_APPLICATION_TAP_START[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_APPLICATION_TAP_START, ulUserId);

	}


	return offset;

};

/****************************************************************************
	Event dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT, hProcess);

	}


	return 0;

};

/****************************************************************************
	CMD/RSP dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD, hProcess),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD, hProcess);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP, hProcess);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP, szName);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP, szProcessImageName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP, szProcessImageName);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP, ulType);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD, hProcess),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD, ulResetProcessStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD, ulResetProcessStatsFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD, ulResetIpcStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD, ulResetIpcStatsFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD, ulResetTaskStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD, ulResetTaskStatsFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP, State);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP, Error), "Error:tOCTVC1_PROCESS_ERROR");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_PROCESS_ERROR( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP, ObjectNameList), "ObjectNameList:tOCTVC1_LIST_NAME_OBJECT32_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_NAME_OBJECT32_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD, ulPortId);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulPortId);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulUserId);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, hRemoteProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulRemotePortId);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulRemoteUserId);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, szName);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[7], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulPortIdx);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[8], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulType);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[9], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulSystemFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulSystemFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[10], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulGhostFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulGhostFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[11], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulRecvMsgNum);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP[12], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP, ulRecvMsgSize);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD, ulPortId);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD, ulResetStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD, ulResetStatsFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP, ulPortId);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP, ulState);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP, ulSendMsgCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP, ulRecvMsgCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP, ulRecvMsgMissCnt);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD, SubObjectIdGet), "SubObjectIdGet:tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP, SubObjectIdGet), "SubObjectIdGet:tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP, ObjectNameList), "ObjectNameList:tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD, ulTaskIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP, ulTaskIndex);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP, szName);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD, ulTaskIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD, ulResetStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD, ulResetStatsFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP, ulTaskIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP, Stats), "Stats:tOCTVC1_PROCESS_TASK_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_PROCESS_TASK_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD, SubObjectIdGet), "SubObjectIdGet:tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP, SubObjectIdGet), "SubObjectIdGet:tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP, ObjectNameList), "ObjectNameList:tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD, ulObjType),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD, ulObjType);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP, ulObjType);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP, ulMaxNumObj);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP, ulObjByteSize);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP, szName);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD, ulObjType);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP, ulObjType);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP, ulMaxNumObj);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP, ulNumObj);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP, ObjectNameList), "ObjectNameList:tOCTVC1_LIST_NAME_OBJECT32_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_NAME_OBJECT32_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD, ulResetStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD, ulResetStatsFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP, ObjectList), "ObjectList:tOCTVC1_LIST_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD, hCmdId);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD, ulResetStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD, ulResetStatsFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP, hCmdId);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP, CommandStats), "CommandStats:tOCTVC1_IRSC_API_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_IRSC_API_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP, IndexList), "IndexList:tOCTVC1_LIST_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP, ulSessionIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP, SessionInfo), "SessionInfo:tOCTVC1_API_SESSION_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_API_SESSION_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD, ulSessionIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD, ulResetStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD, ulResetStatsFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP, ulSessionIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP, SessionStats), "SessionStats:tOCTVC1_IRSC_SESSION_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_IRSC_SESSION_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP, ulSessionIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP, EvtInfo), "EvtInfo:tOCTVC1_API_SESSION_EVT_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_API_SESSION_EVT_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD, ulSessionIndex),temp_data, "cOCTVC1_API_SESSION_INDEX_CURRENT_TRANSPORT (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD, ulSessionIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP, ulSessionIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP, Stats), "Stats:tOCTVC1_IRSC_API_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_IRSC_API_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP, ObjectCursor), "ObjectCursor:tOCTVC1_CURSOR_HANDLE_OBJECT_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP, ObjectNameList), "ObjectNameList:tOCTVC1_LIST_NAME_OBJECT32_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_NAME_OBJECT32_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD, hTapId),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD, hTapId);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP, hTapId);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP, Info), "Info:tOCTVC1_IRSC_APPLICATION_TAP_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_IRSC_APPLICATION_TAP_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD, hTapId),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD, hTapId);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP, hTapId);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP, Stats), "Stats:tOCTVC1_IRSC_APPLICATION_TAP_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_IRSC_APPLICATION_TAP_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD, hTapId),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD, hTapId);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD, Start), "Start:tOCTVC1_IRSC_APPLICATION_TAP_START");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_IRSC_APPLICATION_TAP_START( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP, hTapId);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP, hLogicalObj);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD, hTapId),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD, hTapId);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP, hTapId);

	}


	return 0;

};
/****************************************************************************
	MODULE REGISTERED EXPORTED FUNCTION
 ****************************************************************************/

void ws_register_OCTVC1_IRSC(void)
{
	/****************************************************************************
		Register Common struct
	****************************************************************************/
	register_tOCTVC1_IRSC_API_STATS(); 
	register_tOCTVC1_IRSC_SESSION_STATS(); 
	register_tOCTVC1_IRSC_APPLICATION_TAP_INFO(); 
	register_tOCTVC1_IRSC_APPLICATION_TAP_STATS(); 
	register_tOCTVC1_IRSC_APPLICATION_TAP_START(); 

	/****************************************************************************
		CMD/RSP Registered 
	****************************************************************************/
	register_tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD(); 
	register_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP(); 
	register_tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD(); 
	register_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP(); 
	register_tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD(); 
	register_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP(); 
	register_tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD(); 
	register_tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD(); 
	register_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP(); 
	register_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD(); 
	register_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP(); 
	register_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD(); 
	register_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP(); 
	register_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD(); 
	register_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP(); 
	register_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD(); 
	register_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP(); 
	register_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD(); 
	register_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP(); 

	/****************************************************************************
		Event Registered 
	****************************************************************************/
	register_tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT(); 

}

/****************************************************************************
	MODULE DISSECTOR FUNCTIONS
 ****************************************************************************/
int ws_dissect_OCTVC1_IRSC_CMD( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_IRSC_MSG_PROCESS_INFO_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_STATS_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_LIST_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_OBJMGR_INFO_CID: return dissect_tOCTVC1_IRSC_MSG_OBJMGR_INFO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_OBJMGR_STATS_CID: return dissect_tOCTVC1_IRSC_MSG_OBJMGR_STATS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_OBJMGR_LIST_CID: return dissect_tOCTVC1_IRSC_MSG_OBJMGR_LIST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_STOP_MONITORING_CID: return 0; break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CID: return dissect_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CID: return dissect_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CID: return dissect_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CID: return dissect_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CID: return dissect_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CMD( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_IRSC_RSP( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_IRSC_MSG_PROCESS_INFO_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_STATS_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_LIST_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_OBJMGR_INFO_CID: return dissect_tOCTVC1_IRSC_MSG_OBJMGR_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_OBJMGR_STATS_CID: return dissect_tOCTVC1_IRSC_MSG_OBJMGR_STATS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_OBJMGR_LIST_CID: return dissect_tOCTVC1_IRSC_MSG_OBJMGR_LIST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CID: return 0; break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_STOP_MONITORING_CID: return 0; break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CID: return dissect_tOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CID: return dissect_tOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CID: return dissect_tOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CID: return dissect_tOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CID: return dissect_tOCTVC1_IRSC_MSG_APPLICATION_START_TAP_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CID: return dissect_tOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_RSP( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_IRSC_EVT( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_IRSC_MSG_PROCESS_DUMP_EID: return dissect_tOCTVC1_IRSC_MSG_PROCESS_DUMP_EVT( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}

/****************************************************************************
	MODULE DISSECTOR EXPORTED FUNCTION
 ****************************************************************************/

int ws_dissect_OCTVC1_IRSC( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if (message_type == cOCTVC1_MSG_TYPE_RESPONSE)
		return ws_dissect_OCTVC1_IRSC_RSP( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_COMMAND)
		return ws_dissect_OCTVC1_IRSC_CMD( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_NOTIFICATION )
		return ws_dissect_OCTVC1_IRSC_EVT( CID, tvb, pinfo, tree);
	else
		return 1;

}

