/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: octvc1_module_gsm.c

      
Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark module dissector related functions

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/

#include <config.h>
#include <epan/packet.h>
#include "../../../include/oct_ws_macro.h"
#include "../include/module.h"
#include "../include/octvc1_common.h"

#include <gsm/octvc1_gsm_evt_priv.h>


/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_GSM_CMI_PHASE_ENUM[] = 
 {
	{ cOCTVC1_GSM_CMI_PHASE_ENUM_EVEN, "cOCTVC1_GSM_CMI_PHASE_ENUM_EVEN" },
	{ cOCTVC1_GSM_CMI_PHASE_ENUM_ODD, "cOCTVC1_GSM_CMI_PHASE_ENUM_ODD" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_AMR_CODEC_MODE_ENUM[] = 
 {
	{ cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_4_75, "cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_4_75" },
	{ cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_5_15, "cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_5_15" },
	{ cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_5_90, "cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_5_90" },
	{ cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_6_70, "cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_6_70" },
	{ cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_7_40, "cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_7_40" },
	{ cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_7_95, "cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_7_95" },
	{ cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_10_2, "cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_10_2" },
	{ cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_12_2, "cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_RATE_12_2" },
	{ cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_UNSET, "cOCTVC1_GSM_AMR_CODEC_MODE_ENUM_UNSET" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_ID_DIRECTION_ENUM[] = 
 {
	{ cOCTVC1_GSM_ID_DIRECTION_ENUM_NONE, "cOCTVC1_GSM_ID_DIRECTION_ENUM_NONE" },
	{ cOCTVC1_GSM_ID_DIRECTION_ENUM_RX_BTS_MS, "cOCTVC1_GSM_ID_DIRECTION_ENUM_RX_BTS_MS" },
	{ cOCTVC1_GSM_ID_DIRECTION_ENUM_TX_BTS_MS, "cOCTVC1_GSM_ID_DIRECTION_ENUM_TX_BTS_MS" },
	{ cOCTVC1_GSM_ID_DIRECTION_ENUM_TX_RX_BTS_MS, "cOCTVC1_GSM_ID_DIRECTION_ENUM_TX_RX_BTS_MS" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_DIRECTION_ENUM[] = 
 {
	{ cOCTVC1_GSM_DIRECTION_ENUM_NONE, "cOCTVC1_GSM_DIRECTION_ENUM_NONE" },
	{ cOCTVC1_GSM_DIRECTION_ENUM_RX_BTS_MS, "cOCTVC1_GSM_DIRECTION_ENUM_RX_BTS_MS" },
	{ cOCTVC1_GSM_DIRECTION_ENUM_TX_BTS_MS, "cOCTVC1_GSM_DIRECTION_ENUM_TX_BTS_MS" },
	{ cOCTVC1_GSM_DIRECTION_ENUM_TX_RX_BTS_MS, "cOCTVC1_GSM_DIRECTION_ENUM_TX_RX_BTS_MS" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_ID_TIMESLOT_NB_ENUM[] = 
 {
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_0, "cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_0" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_1, "cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_1" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_2, "cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_2" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_3, "cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_3" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_4, "cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_4" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_5, "cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_5" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_6, "cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_6" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_7, "cOCTVC1_GSM_ID_TIMESLOT_NB_ENUM_7" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_TIMESLOT_NB_ENUM[] = 
 {
	{ cOCTVC1_GSM_TIMESLOT_NB_ENUM_0, "cOCTVC1_GSM_TIMESLOT_NB_ENUM_0" },
	{ cOCTVC1_GSM_TIMESLOT_NB_ENUM_1, "cOCTVC1_GSM_TIMESLOT_NB_ENUM_1" },
	{ cOCTVC1_GSM_TIMESLOT_NB_ENUM_2, "cOCTVC1_GSM_TIMESLOT_NB_ENUM_2" },
	{ cOCTVC1_GSM_TIMESLOT_NB_ENUM_3, "cOCTVC1_GSM_TIMESLOT_NB_ENUM_3" },
	{ cOCTVC1_GSM_TIMESLOT_NB_ENUM_4, "cOCTVC1_GSM_TIMESLOT_NB_ENUM_4" },
	{ cOCTVC1_GSM_TIMESLOT_NB_ENUM_5, "cOCTVC1_GSM_TIMESLOT_NB_ENUM_5" },
	{ cOCTVC1_GSM_TIMESLOT_NB_ENUM_6, "cOCTVC1_GSM_TIMESLOT_NB_ENUM_6" },
	{ cOCTVC1_GSM_TIMESLOT_NB_ENUM_7, "cOCTVC1_GSM_TIMESLOT_NB_ENUM_7" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM[] = 
 {
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_0, "cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_0" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_1, "cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_1" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_2, "cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_2" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_3, "cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_3" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_4, "cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_4" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_5, "cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_5" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_6, "cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_6" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_7, "cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_7" },
	{ cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_ALL, "cOCTVC1_GSM_ID_TIMESLOT_NB_STUB_ENUM_ALL" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM[] = 
 {
	{ cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_0, "cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_0" },
	{ cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_1, "cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_1" },
	{ cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_2, "cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_2" },
	{ cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_3, "cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_3" },
	{ cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_4, "cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_4" },
	{ cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_5, "cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_5" },
	{ cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_6, "cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_6" },
	{ cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_7, "cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_7" },
	{ cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_ALL, "cOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM_ALL" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM[] = 
 {
	{ cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_0, "cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_0" },
	{ cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_1, "cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_1" },
	{ cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_2, "cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_2" },
	{ cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_2, "cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_2" },
	{ cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_3, "cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_3" },
	{ cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_4, "cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_4" },
	{ cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_5, "cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_5" },
	{ cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_6, "cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_6" },
	{ cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_7, "cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_7" },
	{ cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_ALL, "cOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM_ALL" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_SUB_CHANNEL_NB_ENUM[] = 
 {
	{ cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_0, "cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_0" },
	{ cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_1, "cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_1" },
	{ cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_2, "cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_2" },
	{ cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_3, "cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_3" },
	{ cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_4, "cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_4" },
	{ cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_5, "cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_5" },
	{ cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_6, "cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_6" },
	{ cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_7, "cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_7" },
	{ cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_ALL, "cOCTVC1_GSM_SUB_CHANNEL_NB_ENUM_ALL" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_CIPHERING_ID_ENUM[] = 
 {
	{ cOCTVC1_GSM_CIPHERING_ID_ENUM_UNUSED, "cOCTVC1_GSM_CIPHERING_ID_ENUM_UNUSED" },
	{ cOCTVC1_GSM_CIPHERING_ID_ENUM_A5_0, "cOCTVC1_GSM_CIPHERING_ID_ENUM_A5_0" },
	{ cOCTVC1_GSM_CIPHERING_ID_ENUM_A5_1, "cOCTVC1_GSM_CIPHERING_ID_ENUM_A5_1" },
	{ cOCTVC1_GSM_CIPHERING_ID_ENUM_A5_2, "cOCTVC1_GSM_CIPHERING_ID_ENUM_A5_2" },
	{ cOCTVC1_GSM_CIPHERING_ID_ENUM_A5_3, "cOCTVC1_GSM_CIPHERING_ID_ENUM_A5_3" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_SUBCHANNEL_MASK[] = 
 {
	{ cOCTVC1_GSM_SUBCHANNEL_MASK_0, "cOCTVC1_GSM_SUBCHANNEL_MASK_0" },
	{ cOCTVC1_GSM_SUBCHANNEL_MASK_1, "cOCTVC1_GSM_SUBCHANNEL_MASK_1" },
	{ cOCTVC1_GSM_SUBCHANNEL_MASK_2, "cOCTVC1_GSM_SUBCHANNEL_MASK_2" },
	{ cOCTVC1_GSM_SUBCHANNEL_MASK_3, "cOCTVC1_GSM_SUBCHANNEL_MASK_3" },
	{ cOCTVC1_GSM_SUBCHANNEL_MASK_4, "cOCTVC1_GSM_SUBCHANNEL_MASK_4" },
	{ cOCTVC1_GSM_SUBCHANNEL_MASK_5, "cOCTVC1_GSM_SUBCHANNEL_MASK_5" },
	{ cOCTVC1_GSM_SUBCHANNEL_MASK_6, "cOCTVC1_GSM_SUBCHANNEL_MASK_6" },
	{ cOCTVC1_GSM_SUBCHANNEL_MASK_7, "cOCTVC1_GSM_SUBCHANNEL_MASK_7" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_BURST_TYPE_ENUM[] = 
 {
	{ cOCTVC1_GSM_BURST_TYPE_ENUM_SYNC, "cOCTVC1_GSM_BURST_TYPE_ENUM_SYNC" },
	{ cOCTVC1_GSM_BURST_TYPE_ENUM_NORMAL, "cOCTVC1_GSM_BURST_TYPE_ENUM_NORMAL" },
	{ cOCTVC1_GSM_BURST_TYPE_ENUM_DUMMY, "cOCTVC1_GSM_BURST_TYPE_ENUM_DUMMY" },
	{ cOCTVC1_GSM_BURST_TYPE_ENUM_NORMAL_8PSK, "cOCTVC1_GSM_BURST_TYPE_ENUM_NORMAL_8PSK" },
	{ cOCTVC1_GSM_BURST_TYPE_ENUM_ACCESS_TRAINING_0, "cOCTVC1_GSM_BURST_TYPE_ENUM_ACCESS_TRAINING_0" },
	{ cOCTVC1_GSM_BURST_TYPE_ENUM_ACCESS_TRAINING_1, "cOCTVC1_GSM_BURST_TYPE_ENUM_ACCESS_TRAINING_1" },
	{ cOCTVC1_GSM_BURST_TYPE_ENUM_ACCESS_TRAINING_2, "cOCTVC1_GSM_BURST_TYPE_ENUM_ACCESS_TRAINING_2" },
	{ cOCTVC1_GSM_BURST_TYPE_ENUM_FREQ_CORRECTION, "cOCTVC1_GSM_BURST_TYPE_ENUM_FREQ_CORRECTION" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM[] = 
 {
	{ cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_EMPTY, "cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_EMPTY" },
	{ cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_TCHF_FACCHF_SACCHTF, "cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_TCHF_FACCHF_SACCHTF" },
	{ cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_TCHH_FACCHH_SACCHTH, "cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_TCHH_FACCHH_SACCHTH" },
	{ cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_FCCH_SCH_BCCH_CCCH, "cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_FCCH_SCH_BCCH_CCCH" },
	{ cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_FCCH_SCH_BCCH_CCCH_SDCCH4_SACCHC4, "cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_FCCH_SCH_BCCH_CCCH_SDCCH4_SACCHC4" },
	{ cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_SDCCH8_SACCHC8, "cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_SDCCH8_SACCHC8" },
	{ cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_PDTCHF_PACCHF_PTCCHF, "cOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM_PDTCHF_PACCHF_PTCCHF" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_PAYLOAD_TYPE_ENUM[] = 
 {
	{ cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_NONE, "cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_NONE" },
	{ cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_FULL_RATE, "cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_FULL_RATE" },
	{ cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_ENH_FULL_RATE, "cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_ENH_FULL_RATE" },
	{ cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_HALF_RATE, "cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_HALF_RATE" },
	{ cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_AMR_FULL_RATE, "cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_AMR_FULL_RATE" },
	{ cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_AMR_HALF_RATE, "cOCTVC1_GSM_PAYLOAD_TYPE_ENUM_AMR_HALF_RATE" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_SAPI_ENUM[] = 
 {
	{ cOCTVC1_GSM_SAPI_ENUM_IDLE, "cOCTVC1_GSM_SAPI_ENUM_IDLE" },
	{ cOCTVC1_GSM_SAPI_ENUM_FCCH, "cOCTVC1_GSM_SAPI_ENUM_FCCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_SCH, "cOCTVC1_GSM_SAPI_ENUM_SCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_SACCH, "cOCTVC1_GSM_SAPI_ENUM_SACCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_SDCCH, "cOCTVC1_GSM_SAPI_ENUM_SDCCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_BCCH, "cOCTVC1_GSM_SAPI_ENUM_BCCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_PCH_AGCH, "cOCTVC1_GSM_SAPI_ENUM_PCH_AGCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_CBCH, "cOCTVC1_GSM_SAPI_ENUM_CBCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_RACH, "cOCTVC1_GSM_SAPI_ENUM_RACH" },
	{ cOCTVC1_GSM_SAPI_ENUM_TCHF, "cOCTVC1_GSM_SAPI_ENUM_TCHF" },
	{ cOCTVC1_GSM_SAPI_ENUM_FACCHF, "cOCTVC1_GSM_SAPI_ENUM_FACCHF" },
	{ cOCTVC1_GSM_SAPI_ENUM_TCHH, "cOCTVC1_GSM_SAPI_ENUM_TCHH" },
	{ cOCTVC1_GSM_SAPI_ENUM_FACCHH, "cOCTVC1_GSM_SAPI_ENUM_FACCHH" },
	{ cOCTVC1_GSM_SAPI_ENUM_NCH, "cOCTVC1_GSM_SAPI_ENUM_NCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_PDTCH, "cOCTVC1_GSM_SAPI_ENUM_PDTCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_PACCH, "cOCTVC1_GSM_SAPI_ENUM_PACCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_PBCCH, "cOCTVC1_GSM_SAPI_ENUM_PBCCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_PAGCH, "cOCTVC1_GSM_SAPI_ENUM_PAGCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_PPCH, "cOCTVC1_GSM_SAPI_ENUM_PPCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_PNCH, "cOCTVC1_GSM_SAPI_ENUM_PNCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_PTCCH, "cOCTVC1_GSM_SAPI_ENUM_PTCCH" },
	{ cOCTVC1_GSM_SAPI_ENUM_PRACH, "cOCTVC1_GSM_SAPI_ENUM_PRACH" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_SAPI_MASK[] = 
 {
	{ cOCTVC1_GSM_SAPI_MASK_IDLE, "cOCTVC1_GSM_SAPI_MASK_IDLE" },
	{ cOCTVC1_GSM_SAPI_MASK_FCCH, "cOCTVC1_GSM_SAPI_MASK_FCCH" },
	{ cOCTVC1_GSM_SAPI_MASK_SCH, "cOCTVC1_GSM_SAPI_MASK_SCH" },
	{ cOCTVC1_GSM_SAPI_MASK_SACCH, "cOCTVC1_GSM_SAPI_MASK_SACCH" },
	{ cOCTVC1_GSM_SAPI_MASK_SDCCH, "cOCTVC1_GSM_SAPI_MASK_SDCCH" },
	{ cOCTVC1_GSM_SAPI_MASK_BCCH, "cOCTVC1_GSM_SAPI_MASK_BCCH" },
	{ cOCTVC1_GSM_SAPI_MASK_PCH_AGCH, "cOCTVC1_GSM_SAPI_MASK_PCH_AGCH" },
	{ cOCTVC1_GSM_SAPI_MASK_CBCH, "cOCTVC1_GSM_SAPI_MASK_CBCH" },
	{ cOCTVC1_GSM_SAPI_MASK_RACH, "cOCTVC1_GSM_SAPI_MASK_RACH" },
	{ cOCTVC1_GSM_SAPI_MASK_TCHF, "cOCTVC1_GSM_SAPI_MASK_TCHF" },
	{ cOCTVC1_GSM_SAPI_MASK_FACCHF, "cOCTVC1_GSM_SAPI_MASK_FACCHF" },
	{ cOCTVC1_GSM_SAPI_MASK_TCHH, "cOCTVC1_GSM_SAPI_MASK_TCHH" },
	{ cOCTVC1_GSM_SAPI_MASK_FACCHH, "cOCTVC1_GSM_SAPI_MASK_FACCHH" },
	{ cOCTVC1_GSM_SAPI_MASK_NCH, "cOCTVC1_GSM_SAPI_MASK_NCH" },
	{ cOCTVC1_GSM_SAPI_MASK_PDTCH, "cOCTVC1_GSM_SAPI_MASK_PDTCH" },
	{ cOCTVC1_GSM_SAPI_MASK_PACCH, "cOCTVC1_GSM_SAPI_MASK_PACCH" },
	{ cOCTVC1_GSM_SAPI_MASK_PBCCH, "cOCTVC1_GSM_SAPI_MASK_PBCCH" },
	{ cOCTVC1_GSM_SAPI_MASK_PAGCH, "cOCTVC1_GSM_SAPI_MASK_PAGCH" },
	{ cOCTVC1_GSM_SAPI_MASK_PPCH, "cOCTVC1_GSM_SAPI_MASK_PPCH" },
	{ cOCTVC1_GSM_SAPI_MASK_PNCH, "cOCTVC1_GSM_SAPI_MASK_PNCH" },
	{ cOCTVC1_GSM_SAPI_MASK_PTCCH, "cOCTVC1_GSM_SAPI_MASK_PTCCH" },
	{ cOCTVC1_GSM_SAPI_MASK_PRACH, "cOCTVC1_GSM_SAPI_MASK_PRACH" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK[] = 
 {
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT0, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT0" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT1, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT1" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT2, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT2" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT3, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT3" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT4, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT4" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT5, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TRX_ID_BIT5" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_BAND_BIT0, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_BAND_BIT0" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_BAND_BIT1, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_BAND_BIT1" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_BAND_BIT2, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_BAND_BIT2" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_BAND_BIT3, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_BAND_BIT3" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_HOPPING, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_HOPPING" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TSC_BIT0, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TSC_BIT0" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TSC_BIT1, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TSC_BIT1" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TSC_BIT2, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TSC_BIT2" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TSC_BIT3, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TSC_BIT3" },
	{ cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TSC_BIT4, "cOCTVC1_GSM_TRX_BAND_HOPPING_TSC_MASK_TSC_BIT4" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_TRX_STATUS_ENUM[] = 
 {
	{ cOCTVC1_GSM_TRX_STATUS_ENUM_INVALID, "cOCTVC1_GSM_TRX_STATUS_ENUM_INVALID" },
	{ cOCTVC1_GSM_TRX_STATUS_ENUM_RADIO_READY, "cOCTVC1_GSM_TRX_STATUS_ENUM_RADIO_READY" },
	{ cOCTVC1_GSM_TRX_STATUS_ENUM_RADIO_ERROR, "cOCTVC1_GSM_TRX_STATUS_ENUM_RADIO_ERROR" },
	{ cOCTVC1_GSM_TRX_STATUS_ENUM_PROCESSING_STOP, "cOCTVC1_GSM_TRX_STATUS_ENUM_PROCESSING_STOP" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_TAP_FILTER_TYPE_ENUM[] = 
 {
	{ cOCTVC1_GSM_TAP_FILTER_TYPE_ENUM_NONE, "cOCTVC1_GSM_TAP_FILTER_TYPE_ENUM_NONE" },
	{ cOCTVC1_GSM_TAP_FILTER_TYPE_ENUM_TRX, "cOCTVC1_GSM_TAP_FILTER_TYPE_ENUM_TRX" },
	{ cOCTVC1_GSM_TAP_FILTER_TYPE_ENUM_PHYSICAL_CHANNEL, "cOCTVC1_GSM_TAP_FILTER_TYPE_ENUM_PHYSICAL_CHANNEL" },
	{ cOCTVC1_GSM_TAP_FILTER_TYPE_ENUM_LOGICAL_CHANNEL, "cOCTVC1_GSM_TAP_FILTER_TYPE_ENUM_LOGICAL_CHANNEL" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_GSM_TAP_FILTER_MASK[] = 
 {
	{ cOCTVC1_GSM_TAP_FILTER_MASK_NONE, "cOCTVC1_GSM_TAP_FILTER_MASK_NONE" },
	{ cOCTVC1_GSM_TAP_FILTER_MASK_REMOVE_REJECTED_DATA, "cOCTVC1_GSM_TAP_FILTER_MASK_REMOVE_REJECTED_DATA" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_GSM_TRX_ID[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TRX_ID;

void  register_tOCTVC1_GSM_TRX_ID(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TRX_ID[0],
			{ "byTrxId", "octvc1.gsm.trx_id.bytrxid",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"byTrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_ID[1],
			{ "abyPad", "octvc1.gsm.trx_id.abypad",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"abyPad",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TRX_ID.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TRX_ID_CURSOR[1];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TRX_ID_CURSOR;

void  register_tOCTVC1_GSM_TRX_ID_CURSOR(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TRX_ID_CURSOR[0],
			{ "TrxId", "octvc1.gsm.trx_id_cursor.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TRX_ID_CURSOR.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TRX_ID_GET_CURSOR[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TRX_ID_GET_CURSOR;

void  register_tOCTVC1_GSM_TRX_ID_GET_CURSOR(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TRX_ID_GET_CURSOR[0],
			{ "TrxId", "octvc1.gsm.trx_id_get_cursor.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_ID_GET_CURSOR[1],
			{ "ulGetMode", "octvc1.gsm.trx_id_get_cursor.ulgetmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_OBJECT_CURSOR_ENUM), 0x0,
			"ulGetMode",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TRX_ID_GET_CURSOR.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_RF_CONFIG[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_RF_CONFIG;

void  register_tOCTVC1_GSM_RF_CONFIG(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_RF_CONFIG[0],
			{ "ulRxGainDb", "octvc1.gsm.rf_config.ulrxgaindb",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxGainDb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_RF_CONFIG[1],
			{ "ulTxAttndB", "octvc1.gsm.rf_config.ultxattndb",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxAttndB",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_RF_CONFIG.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TRX_CONFIG[11];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TRX_CONFIG;

void  register_tOCTVC1_GSM_TRX_CONFIG(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[0],
			{ "ulBand", "octvc1.gsm.trx_config.ulband",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM), 0x0,
			"ulBand",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[1],
			{ "usTsc", "octvc1.gsm.trx_config.ustsc",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usTsc",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[2],
			{ "usArfcn", "octvc1.gsm.trx_config.usarfcn",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usArfcn",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[3],
			{ "usBcchArfcn", "octvc1.gsm.trx_config.usbccharfcn",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usBcchArfcn",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[4],
			{ "usCentreArfcn", "octvc1.gsm.trx_config.uscentrearfcn",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usCentreArfcn",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[5],
			{ "usHsn", "octvc1.gsm.trx_config.ushsn",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usHsn",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[6],
			{ "usMaio", "octvc1.gsm.trx_config.usmaio",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usMaio",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[7],
			{ "usReserve", "octvc1.gsm.trx_config.usreserve",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usReserve",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[8],
			{ "ulHoppingFlag", "octvc1.gsm.trx_config.ulhoppingflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulHoppingFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[9],
			{ "ulHoppingListLength", "octvc1.gsm.trx_config.ulhoppinglistlength",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulHoppingListLength",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TRX_CONFIG[10],
			{ "ausHoppingList", "octvc1.gsm.trx_config.aushoppinglist",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"ausHoppingList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TRX_CONFIG.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_PHYSICAL_CHANNEL_ID;

void  register_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID[0],
			{ "byTimeslotNb", "octvc1.gsm.physical_channel_id.bytimeslotnb",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_ID_TIMESLOT_NB_ENUM), 0x0,
			"byTimeslotNb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID[1],
			{ "abyPad", "octvc1.gsm.physical_channel_id.abypad",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"abyPad",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_PHYSICAL_CHANNEL_ID.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR[1];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR;

void  register_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR[0],
			{ "PchId", "octvc1.gsm.physical_channel_id_cursor.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR;

void  register_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR[0],
			{ "TrxId", "octvc1.gsm.physical_channel_id_get_cursor.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR[1],
			{ "PchId", "octvc1.gsm.physical_channel_id_get_cursor.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR[2],
			{ "ulGetMode", "octvc1.gsm.physical_channel_id_get_cursor.ulgetmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_OBJECT_CURSOR_ENUM), 0x0,
			"ulGetMode",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_SUBCHANNEL_STATUS[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_SUBCHANNEL_STATUS;

void  register_tOCTVC1_GSM_SUBCHANNEL_STATUS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_SUBCHANNEL_STATUS[0],
			{ "ulSubchannelNb", "octvc1.gsm.subchannel_status.ulsubchannelnb",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulSubchannelNb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_SUBCHANNEL_STATUS[1],
			{ "ulActiveUplinkSAPIMask", "octvc1.gsm.subchannel_status.ulactiveuplinksapimask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulActiveUplinkSAPIMask",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_SUBCHANNEL_STATUS[2],
			{ "ulActiveDownlinkSAPIMask", "octvc1.gsm.subchannel_status.ulactivedownlinksapimask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulActiveDownlinkSAPIMask",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_SUBCHANNEL_STATUS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_CIPHER_CONFIG[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_CIPHER_CONFIG;

void  register_tOCTVC1_GSM_CIPHER_CONFIG(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_CIPHER_CONFIG[0],
			{ "ulCipherId", "octvc1.gsm.cipher_config.ulcipherid",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_CIPHERING_ID_ENUM), 0x0,
			"ulCipherId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_CIPHER_CONFIG[1],
			{ "abyKey", "octvc1.gsm.cipher_config.abykey",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"abyKey",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_CIPHER_CONFIG.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_BUFF_ADDR[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_BUFF_ADDR;

void  register_tOCTVC1_GSM_BUFF_ADDR(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_BUFF_ADDR[0],
			{ "BuffAddr_TCHFS", "octvc1.gsm.buff_addr.buffaddr_tchfs",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"BuffAddr_TCHFS",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_BUFF_ADDR[1],
			{ "BuffAddr_RACH", "octvc1.gsm.buff_addr.buffaddr_rach",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"BuffAddr_RACH",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_BUFF_ADDR.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_STUB_CHANNEL_ID[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_STUB_CHANNEL_ID;

void  register_tOCTVC1_GSM_STUB_CHANNEL_ID(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_STUB_CHANNEL_ID[0],
			{ "byTimeslotNb", "octvc1.gsm.stub_channel_id.bytimeslotnb",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM), 0x0,
			"byTimeslotNb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_STUB_CHANNEL_ID[1],
			{ "bySubChannelNb", "octvc1.gsm.stub_channel_id.bysubchannelnb",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM), 0x0,
			"bySubChannelNb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_STUB_CHANNEL_ID[2],
			{ "CHANNEL_TYPE", "octvc1.gsm.stub_channel_id.channel_type",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_SAPI_ENUM), 0x0,
			"CHANNEL_TYPE",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_STUB_CHANNEL_ID.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_LOGICAL_CHANNEL_ID;

void  register_tOCTVC1_GSM_LOGICAL_CHANNEL_ID(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[0],
			{ "byTimeslotNb", "octvc1.gsm.logical_channel_id.bytimeslotnb",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_ID_TIMESLOT_NB_ENUM), 0x0,
			"byTimeslotNb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[1],
			{ "bySubChannelNb", "octvc1.gsm.logical_channel_id.bysubchannelnb",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM), 0x0,
			"bySubChannelNb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[2],
			{ "bySAPI", "octvc1.gsm.logical_channel_id.bysapi",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_SAPI_ENUM), 0x0,
			"bySAPI",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[3],
			{ "byDirection", "octvc1.gsm.logical_channel_id.bydirection",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_ID_DIRECTION_ENUM), 0x0,
			"byDirection",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_LOGICAL_CHANNEL_ID.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR[1];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR;

void  register_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR[0],
			{ "LchId", "octvc1.gsm.logical_channel_id_cursor.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR;

void  register_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR[0],
			{ "TrxId", "octvc1.gsm.logical_channel_id_get_cursor.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR[1],
			{ "LchId", "octvc1.gsm.logical_channel_id_get_cursor.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR[2],
			{ "ulGetMode", "octvc1.gsm.logical_channel_id_get_cursor.ulgetmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_OBJECT_CURSOR_ENUM), 0x0,
			"ulGetMode",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG;

void  register_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[0],
			{ "byTimingAdvance", "octvc1.gsm.logical_channel_config.bytimingadvance",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"byTimingAdvance",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[1],
			{ "byBSIC", "octvc1.gsm.logical_channel_config.bybsic",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"byBSIC",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[2],
			{ "byCmiPhase", "octvc1.gsm.logical_channel_config.bycmiphase",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_CMI_PHASE_ENUM), 0x0,
			"byCmiPhase",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[3],
			{ "byInitRate", "octvc1.gsm.logical_channel_config.byinitrate",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_AMR_CODEC_MODE_ENUM), 0x0,
			"byInitRate",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[4],
			{ "abyRate", "octvc1.gsm.logical_channel_config.abyrate",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"abyRate",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_LOGICAL_CHANNEL_DATA;

void  register_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[0],
			{ "ulFrameNumber", "octvc1.gsm.logical_channel_data.ulframenumber",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFrameNumber",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[1],
			{ "ulPayloadType", "octvc1.gsm.logical_channel_data.ulpayloadtype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_PAYLOAD_TYPE_ENUM), 0x0,
			"ulPayloadType",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[2],
			{ "ulDataLength", "octvc1.gsm.logical_channel_data.uldatalength",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulDataLength",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[3],
			{ "abyDataContent", "octvc1.gsm.logical_channel_data.abydatacontent",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"abyDataContent",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_LOGICAL_CHANNEL_DATA.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA;

void  register_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA[0],
			{ "ulFrameNumber", "octvc1.gsm.logical_channel_raw_data.ulframenumber",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFrameNumber",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA[1],
			{ "ulDataLength", "octvc1.gsm.logical_channel_raw_data.uldatalength",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulDataLength",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA[2],
			{ "abyDataContent", "octvc1.gsm.logical_channel_raw_data.abydatacontent",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"abyDataContent",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MEASUREMENT_INFO[8];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MEASUREMENT_INFO;

void  register_tOCTVC1_GSM_MEASUREMENT_INFO(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MEASUREMENT_INFO[0],
			{ "sSNRDb", "octvc1.gsm.measurement_info.ssnrdb",
			FT_INT16,BASE_DEC, NULL, 0x0,
			"sSNRDb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MEASUREMENT_INFO[1],
			{ "sRSSIDbm", "octvc1.gsm.measurement_info.srssidbm",
			FT_INT16,BASE_DEC, NULL, 0x0,
			"sRSSIDbm",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MEASUREMENT_INFO[2],
			{ "sBurstTiming", "octvc1.gsm.measurement_info.sbursttiming",
			FT_INT16,BASE_DEC, NULL, 0x0,
			"sBurstTiming",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MEASUREMENT_INFO[3],
			{ "sBurstTiming4x", "octvc1.gsm.measurement_info.sbursttiming4x",
			FT_INT16,BASE_DEC, NULL, 0x0,
			"sBurstTiming4x",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MEASUREMENT_INFO[4],
			{ "usBERCnt", "octvc1.gsm.measurement_info.usbercnt",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usBERCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MEASUREMENT_INFO[5],
			{ "usBERTotalBitCnt", "octvc1.gsm.measurement_info.usbertotalbitcnt",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usBERTotalBitCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MEASUREMENT_INFO[6],
			{ "usRxQualFullUp", "octvc1.gsm.measurement_info.usrxqualfullup",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usRxQualFullUp",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MEASUREMENT_INFO[7],
			{ "usRxLevelFullUp", "octvc1.gsm.measurement_info.usrxlevelfullup",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usRxLevelFullUp",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MEASUREMENT_INFO.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[9];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_RAW_MEASUREMENT_INFO;

void  register_tOCTVC1_GSM_RAW_MEASUREMENT_INFO(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[0],
			{ "sSNRDb", "octvc1.gsm.raw_measurement_info.ssnrdb",
			FT_INT16,BASE_DEC, NULL, 0x0,
			"sSNRDb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[1],
			{ "sRSSIDbm", "octvc1.gsm.raw_measurement_info.srssidbm",
			FT_INT16,BASE_DEC, NULL, 0x0,
			"sRSSIDbm",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[2],
			{ "sBurstTiming", "octvc1.gsm.raw_measurement_info.sbursttiming",
			FT_INT16,BASE_DEC, NULL, 0x0,
			"sBurstTiming",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[3],
			{ "sBurstTiming4x", "octvc1.gsm.raw_measurement_info.sbursttiming4x",
			FT_INT16,BASE_DEC, NULL, 0x0,
			"sBurstTiming4x",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[4],
			{ "usBERCnt", "octvc1.gsm.raw_measurement_info.usbercnt",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usBERCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[5],
			{ "usBERTotalBitCnt", "octvc1.gsm.raw_measurement_info.usbertotalbitcnt",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usBERTotalBitCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[6],
			{ "usTrSqErrCnt", "octvc1.gsm.raw_measurement_info.ustrsqerrcnt",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usTrSqErrCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[7],
			{ "usTrSqTotalBitCnt", "octvc1.gsm.raw_measurement_info.ustrsqtotalbitcnt",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usTrSqTotalBitCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[8],
			{ "sFreqErrHz", "octvc1.gsm.raw_measurement_info.sfreqerrhz",
			FT_INT16,BASE_DEC, NULL, 0x0,
			"sFreqErrHz",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_RAW_MEASUREMENT_INFO.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TIMESLOT_POWER[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TIMESLOT_POWER;

void  register_tOCTVC1_GSM_TIMESLOT_POWER(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TIMESLOT_POWER[0],
			{ "ulPowerMax", "octvc1.gsm.timeslot_power.ulpowermax",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPowerMax",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TIMESLOT_POWER[1],
			{ "ulPowerAverage", "octvc1.gsm.timeslot_power.ulpoweraverage",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPowerAverage",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TIMESLOT_POWER[2],
			{ "ulCount", "octvc1.gsm.timeslot_power.ulcount",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulCount",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TIMESLOT_POWER.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_PHYSICAL_STATUS[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_PHYSICAL_STATUS;

void  register_tOCTVC1_GSM_PHYSICAL_STATUS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_STATUS[0],
			{ "ulChannelType", "octvc1.gsm.physical_status.ulchanneltype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM), 0x0,
			"ulChannelType",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_STATUS[1],
			{ "ulSubchannelCount", "octvc1.gsm.physical_status.ulsubchannelcount",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulSubchannelCount",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_STATUS[2],
			{ "ulTchDataCrcPassCount", "octvc1.gsm.physical_status.ultchdatacrcpasscount",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTchDataCrcPassCount",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_STATUS[3],
			{ "ulTchDataCrcFailCount", "octvc1.gsm.physical_status.ultchdatacrcfailcount",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTchDataCrcFailCount",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_PHYSICAL_STATUS[4],
			{ "ulRachCount", "octvc1.gsm.physical_status.ulrachcount",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulRachCount",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_PHYSICAL_STATUS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TIMESLOT_STATUS[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TIMESLOT_STATUS;

void  register_tOCTVC1_GSM_TIMESLOT_STATUS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TIMESLOT_STATUS[0],
			{ "PhysicalStatus", "octvc1.gsm.timeslot_status.physicalstatus",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PhysicalStatus",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TIMESLOT_STATUS[1],
			{ "UplinkPower", "octvc1.gsm.timeslot_status.uplinkpower",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"UplinkPower",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TIMESLOT_STATUS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_DL_STATUS[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_DL_STATUS;

void  register_tOCTVC1_GSM_DL_STATUS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_DL_STATUS[0],
			{ "ulElapseRspCmdMaxUs", "octvc1.gsm.dl_status.ulelapserspcmdmaxus",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulElapseRspCmdMaxUs",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_DL_STATUS[1],
			{ "ulElapseRspCmdAvgUs", "octvc1.gsm.dl_status.ulelapserspcmdavgus",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulElapseRspCmdAvgUs",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_DL_STATUS[2],
			{ "ulExpectedDelayExceedCnt", "octvc1.gsm.dl_status.ulexpecteddelayexceedcnt",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulExpectedDelayExceedCnt",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_DL_STATUS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_UL_STATUS[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_UL_STATUS;

void  register_tOCTVC1_GSM_UL_STATUS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_UL_STATUS[0],
			{ "ulExceedByteMaxCnt", "octvc1.gsm.ul_status.ulexceedbytemaxcnt",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulExceedByteMaxCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_UL_STATUS[1],
			{ "ulExpectedDataExceedCnt", "octvc1.gsm.ul_status.ulexpecteddataexceedcnt",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulExpectedDataExceedCnt",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_UL_STATUS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_SCHED_STATUS[1];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_SCHED_STATUS;

void  register_tOCTVC1_GSM_SCHED_STATUS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_SCHED_STATUS[0],
			{ "ulErrorDataInactiveCnt", "octvc1.gsm.sched_status.ulerrordatainactivecnt",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulErrorDataInactiveCnt",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_SCHED_STATUS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY;

void  register_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY[0],
			{ "TrxId", "octvc1.gsm.tap_filter_physical_channel_entry.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY[1],
			{ "PchId", "octvc1.gsm.tap_filter_physical_channel_entry.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY;

void  register_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY[0],
			{ "TrxId", "octvc1.gsm.tap_filter_logical_channel_entry.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY[1],
			{ "LchId", "octvc1.gsm.tap_filter_logical_channel_entry.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TAP_FILTER_TRX[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TAP_FILTER_TRX;

void  register_tOCTVC1_GSM_TAP_FILTER_TRX(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_TRX[0],
			{ "ulAllTrxFlag", "octvc1.gsm.tap_filter_trx.ulalltrxflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulAllTrxFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_TRX[1],
			{ "TrxId", "octvc1.gsm.tap_filter_trx.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TAP_FILTER_TRX.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL;

void  register_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL[0],
			{ "ulEntryCnt", "octvc1.gsm.tap_filter_physical_channel.ulentrycnt",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulEntryCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL[1],
			{ "aEntry", "octvc1.gsm.tap_filter_physical_channel.aentry",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"aEntry",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL;

void  register_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL[0],
			{ "ulEntryCnt", "octvc1.gsm.tap_filter_logical_channel.ulentrycnt",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulEntryCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL[1],
			{ "aEntry", "octvc1.gsm.tap_filter_logical_channel.aentry",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"aEntry",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TAP_FILTER[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TAP_FILTER;

void  register_tOCTVC1_GSM_TAP_FILTER(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER[0],
			{ "ulType", "octvc1.gsm.tap_filter.ultype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_TAP_FILTER_TYPE_ENUM), 0x0,
			"ulType",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER[1],
			{ "ulMask", "octvc1.gsm.tap_filter.ulmask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulMask",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER[2],
			{ "Trx", "octvc1.gsm.tap_filter.trx",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Trx",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER[3],
			{ "PhysicalChannel", "octvc1.gsm.tap_filter.physicalchannel",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PhysicalChannel",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER[4],
			{ "LogicalChannel", "octvc1.gsm.tap_filter.logicalchannel",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LogicalChannel",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TAP_FILTER.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_TAP_FILTER_STATS[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_TAP_FILTER_STATS;

void  register_tOCTVC1_GSM_TAP_FILTER_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_STATS[0],
			{ "ulTapIdCnt", "octvc1.gsm.tap_filter_stats.ultapidcnt",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTapIdCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_TAP_FILTER_STATS[1],
			{ "ahTapId", "octvc1.gsm.tap_filter_stats.ahtapid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ahTapId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_TAP_FILTER_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[11];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER;

void  register_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[0],
			{ "ModuleData", "octvc1.gsm.module_data_rf_uplink_input_header.moduledata",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ModuleData",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[1],
			{ "TrxId", "octvc1.gsm.module_data_rf_uplink_input_header.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[2],
			{ "LchId", "octvc1.gsm.module_data_rf_uplink_input_header.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[3],
			{ "ulAntennaIndex", "octvc1.gsm.module_data_rf_uplink_input_header.ulantennaindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulAntennaIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[4],
			{ "ulFrameNumber", "octvc1.gsm.module_data_rf_uplink_input_header.ulframenumber",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFrameNumber",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[5],
			{ "ulPower", "octvc1.gsm.module_data_rf_uplink_input_header.ulpower",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPower",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[6],
			{ "ulDataSizeInBytes", "octvc1.gsm.module_data_rf_uplink_input_header.uldatasizeinbytes",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulDataSizeInBytes",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[7],
			{ "byBadFlag", "octvc1.gsm.module_data_rf_uplink_input_header.bybadflag",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"byBadFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[8],
			{ "byBurstId", "octvc1.gsm.module_data_rf_uplink_input_header.byburstid",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"byBurstId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[9],
			{ "byReserved0", "octvc1.gsm.module_data_rf_uplink_input_header.byreserved0",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"byReserved0",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[10],
			{ "byReserved1", "octvc1.gsm.module_data_rf_uplink_input_header.byreserved1",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"byReserved1",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	Event Registered 
 ****************************************************************************/

int ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT;

void  register_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.data_indication.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.data_indication.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT[2],
			{ "MeasurementInfo", "octvc1.gsm.trx.logical_channel.data_indication.measurementinfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"MeasurementInfo",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT[3],
			{ "Data", "octvc1.gsm.trx.logical_channel.data_indication.data",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Data",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT;

void  register_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.ready_to_send_indication.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.ready_to_send_indication.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT[2],
			{ "ulFrameNumber", "octvc1.gsm.trx.logical_channel.ready_to_send_indication.ulframenumber",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFrameNumber",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[8];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT;

void  register_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.rach_indication.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.rach_indication.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[2],
			{ "ulBurstType", "octvc1.gsm.trx.logical_channel.rach_indication.ulbursttype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_BURST_TYPE_ENUM), 0x0,
			"ulBurstType",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[3],
			{ "ulFrameNumber", "octvc1.gsm.trx.logical_channel.rach_indication.ulframenumber",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFrameNumber",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[4],
			{ "MeasurementInfo", "octvc1.gsm.trx.logical_channel.rach_indication.measurementinfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"MeasurementInfo",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[5],
			{ "ulMsgLength", "octvc1.gsm.trx.logical_channel.rach_indication.ulmsglength",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulMsgLength",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[6],
			{ "abyMsg", "octvc1.gsm.trx.logical_channel.rach_indication.abymsg",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"abyMsg",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT;

void  register_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.raw_data_indication.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.raw_data_indication.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT[2],
			{ "MeasurementInfo", "octvc1.gsm.trx.logical_channel.raw_data_indication.measurementinfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"MeasurementInfo",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT[3],
			{ "Data", "octvc1.gsm.trx.logical_channel.raw_data_indication.data",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Data",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT;

void  register_tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT[0],
			{ "TrxId", "octvc1.gsm.trx.time_indication.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT[1],
			{ "ulFrameNumber", "octvc1.gsm.trx.time_indication.ulframenumber",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFrameNumber",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT;

void  register_tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT[0],
			{ "TrxId", "octvc1.gsm.trx.status_change.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT[1],
			{ "ulStatus", "octvc1.gsm.trx.status_change.ulstatus",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_TRX_STATUS_ENUM), 0x0,
			"ulStatus",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT[2],
			{ "ulFrameNumber", "octvc1.gsm.trx.status_change.ulframenumber",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFrameNumber",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	CMD/RSP Registered 
 ****************************************************************************/

int ahf_tOCTVC1_GSM_MSG_TRX_OPEN_CMD[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_OPEN_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_OPEN_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_OPEN_CMD[0],
			{ "ulRfPortIndex", "octvc1.gsm.trx.open.ulrfportindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulRfPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_OPEN_CMD[1],
			{ "TrxId", "octvc1.gsm.trx.open.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_OPEN_CMD[2],
			{ "Config", "octvc1.gsm.trx.open.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_OPEN_CMD[3],
			{ "RfConfig", "octvc1.gsm.trx.open.rfconfig",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"RfConfig",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_OPEN_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_OPEN_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_OPEN_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_OPEN_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_OPEN_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.open.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_OPEN_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_CLOSE_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_CLOSE_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_CLOSE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_CLOSE_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.close.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_CLOSE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_CLOSE_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_CLOSE_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_CLOSE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_CLOSE_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.close.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_CLOSE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STATUS_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STATUS_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_STATUS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.status.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_CMD[1],
			{ "ulResetFlag", "octvc1.gsm.trx.status.ulresetflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STATUS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STATUS_RSP[6];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STATUS_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_STATUS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.status.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_RSP[1],
			{ "DlStatus", "octvc1.gsm.trx.status.dlstatus",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"DlStatus",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_RSP[2],
			{ "UlStatus", "octvc1.gsm.trx.status.ulstatus",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"UlStatus",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_RSP[3],
			{ "SchedStatus", "octvc1.gsm.trx.status.schedstatus",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SchedStatus",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_RSP[4],
			{ "aTSlotStatus", "octvc1.gsm.trx.status.atslotstatus",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"aTSlotStatus",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STATUS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_RSP[1],
			{ "ulRfPortIndex", "octvc1.gsm.trx.info.ulrfportindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulRfPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_RSP[2],
			{ "Config", "octvc1.gsm.trx.info.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_RESET_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_RESET_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_RESET_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_RESET_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.reset.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_RESET_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_RESET_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_RESET_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_RESET_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_RESET_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.reset.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_RESET_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_MODIFY_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_MODIFY_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.modify.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_CMD[1],
			{ "Config", "octvc1.gsm.trx.modify.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_MODIFY_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_MODIFY_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_MODIFY_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.modify.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_MODIFY_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_LIST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_LIST_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_LIST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_CMD[0],
			{ "TrxIdCursor", "octvc1.gsm.trx.list.trxidcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxIdCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_LIST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_LIST_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_LIST_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_LIST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_RSP[0],
			{ "TrxIdCursor", "octvc1.gsm.trx.list.trxidcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxIdCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_RSP[1],
			{ "ulNumTrxId", "octvc1.gsm.trx.list.ulnumtrxid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulNumTrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_RSP[2],
			{ "aTrxId", "octvc1.gsm.trx.list.atrxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"aTrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_LIST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP[0],
			{ "ulCloseCount", "octvc1.gsm.trx.close_all.ulclosecount",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulCloseCount",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_START_RECORD_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.record.start.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_START_RECORD_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_START_RECORD_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.record.start.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_START_RECORD_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.record.stop.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.record.stop.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_RF_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.rf.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_RF_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_RF_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.rf.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP[1],
			{ "RfConfig", "octvc1.gsm.trx.rf.info.rfconfig",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"RfConfig",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_RF_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.rf.modify.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD[1],
			{ "RfConfig", "octvc1.gsm.trx.rf.modify.rfconfig",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"RfConfig",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.rf.modify.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.stubb_loopback_test.request.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.stubb_loopback_test.request.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP[1],
			{ "BuffADDR", "octvc1.gsm.trx.stubb_loopback_test.request.buffaddr",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"BuffADDR",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.stubb_loopback_test.start.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD[1],
			{ "STUB_CHANNEL_ID", "octvc1.gsm.trx.stubb_loopback_test.start.stub_channel_id",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"STUB_CHANNEL_ID",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.stubb_loopback_test.stop.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD[1],
			{ "byTimeslotNb", "octvc1.gsm.trx.stubb_loopback_test.stop.bytimeslotnb",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_TIMESLOT_NB_STUB_ENUM), 0x0,
			"byTimeslotNb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD[2],
			{ "bySubChannelNb", "octvc1.gsm.trx.stubb_loopback_test.stop.bysubchannelnb",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_ID_SUB_CHANNEL_NB_ENUM), 0x0,
			"bySubChannelNb",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.activate.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.activate.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD[2],
			{ "Config", "octvc1.gsm.trx.logical_channel.activate.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.activate.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.activate.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.deactivate.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.deactivate.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.deactivate.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.deactivate.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.status.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.status.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.status.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.status.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP[2],
			{ "usStatus", "octvc1.gsm.trx.logical_channel.status.usstatus",
			FT_UINT16,BASE_HEX, NULL, 0x0,
			"usStatus",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.info.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.info.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP[2],
			{ "Config", "octvc1.gsm.trx.logical_channel.info.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD[0],
			{ "LchIdCursor", "octvc1.gsm.trx.logical_channel.list.lchidcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchIdCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP[0],
			{ "LchIdCursor", "octvc1.gsm.trx.logical_channel.list.lchidcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchIdCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP[1],
			{ "ulNumLchId", "octvc1.gsm.trx.logical_channel.list.ulnumlchid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulNumLchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP[2],
			{ "aLchId", "octvc1.gsm.trx.logical_channel.list.alchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"aLchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.raw_data_indications.start.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.raw_data_indications.start.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.raw_data_indications.start.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.raw_data_indications.start.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.raw_data_indications.stop.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.raw_data_indications.stop.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.raw_data_indications.stop.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.raw_data_indications.stop.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.empty_frame.request.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.empty_frame.request.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD[2],
			{ "ulFrameNumber", "octvc1.gsm.trx.logical_channel.empty_frame.request.ulframenumber",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFrameNumber",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.empty_frame.request.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.empty_frame.request.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.data.request.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.data.request.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD[2],
			{ "Data", "octvc1.gsm.trx.logical_channel.data.request.data",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Data",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.data.request.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.data.request.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.cmu_loopback_test.request.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.cmu_loopback_test.request.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD[2],
			{ "ulResetFlag", "octvc1.gsm.trx.logical_channel.cmu_loopback_test.request.ulresetflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.logical_channel.cmu_loopback_test.request.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP[1],
			{ "LchId", "octvc1.gsm.trx.logical_channel.cmu_loopback_test.request.lchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"LchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.activate.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.activate.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD[2],
			{ "ulChannelType", "octvc1.gsm.trx.physical_channel.activate.ulchanneltype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM), 0x0,
			"ulChannelType",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD[3],
			{ "ulPayloadType", "octvc1.gsm.trx.physical_channel.activate.ulpayloadtype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_PAYLOAD_TYPE_ENUM), 0x0,
			"ulPayloadType",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.activate.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.activate.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.deactivate.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.deactivate.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.deactivate.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.deactivate.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.status.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.status.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD[2],
			{ "ulResetFlag", "octvc1.gsm.trx.physical_channel.status.ulresetflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.status.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.status.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP[2],
			{ "PhysicalStatus", "octvc1.gsm.trx.physical_channel.status.physicalstatus",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PhysicalStatus",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP[3],
			{ "aSubchannnelStatus", "octvc1.gsm.trx.physical_channel.status.asubchannnelstatus",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"aSubchannnelStatus",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.reset.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.reset.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.reset.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.reset.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD[0],
			{ "PchIdCursor", "octvc1.gsm.trx.physical_channel.list.pchidcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchIdCursor",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP[0],
			{ "PchIdCursor", "octvc1.gsm.trx.physical_channel.list.pchidcursor",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchIdCursor",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP[1],
			{ "ulNumPchId", "octvc1.gsm.trx.physical_channel.list.ulnumpchid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulNumPchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP[2],
			{ "aPchId", "octvc1.gsm.trx.physical_channel.list.apchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"aPchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.info.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.info.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP[2],
			{ "ulChannelType", "octvc1.gsm.trx.physical_channel.info.ulchanneltype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_LOGICAL_CHANNEL_COMBINATION_ENUM), 0x0,
			"ulChannelType",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP[3],
			{ "ulPayloadType", "octvc1.gsm.trx.physical_channel.info.ulpayloadtype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_PAYLOAD_TYPE_ENUM), 0x0,
			"ulPayloadType",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD[7];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.ciphering.modify.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.ciphering.modify.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD[2],
			{ "ulSubchannelNb", "octvc1.gsm.trx.physical_channel.ciphering.modify.ulsubchannelnb",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_GSM_SUB_CHANNEL_NB_ENUM), 0x0,
			"ulSubchannelNb",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD[3],
			{ "ulDirection", "octvc1.gsm.trx.physical_channel.ciphering.modify.uldirection",
			FT_UINT8,BASE_HEX, VALS(vals_tOCTVC1_GSM_DIRECTION_ENUM), 0x0,
			"ulDirection",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD[4],
			{ "Config", "octvc1.gsm.trx.physical_channel.ciphering.modify.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD[5],
			{ "abyPad", "octvc1.gsm.trx.physical_channel.ciphering.modify.abypad",
			FT_UINT8,BASE_HEX, NULL, 0x0,
			"abyPad",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.ciphering.modify.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.ciphering.modify.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.ciphering.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.ciphering.info.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP[6];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.ciphering.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.ciphering.info.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP[2],
			{ "ulTxSubChannelMask", "octvc1.gsm.trx.physical_channel.ciphering.info.ultxsubchannelmask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulTxSubChannelMask",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP[3],
			{ "ulRxSubChannelMask", "octvc1.gsm.trx.physical_channel.ciphering.info.ulrxsubchannelmask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulRxSubChannelMask",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP[4],
			{ "Config", "octvc1.gsm.trx.physical_channel.ciphering.info.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.measurement.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.measurement.info.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP;

void  register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP[0],
			{ "TrxId", "octvc1.gsm.trx.physical_channel.measurement.info.trxid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TrxId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP[1],
			{ "PchId", "octvc1.gsm.trx.physical_channel.measurement.info.pchid",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"PchId",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP[2],
			{ "MeasurementInfo", "octvc1.gsm.trx.physical_channel.measurement.info.measurementinfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"MeasurementInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD;

void  register_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD[0],
			{ "IndexGet", "octvc1.gsm.tap_filter.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP;

void  register_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP[0],
			{ "IndexGet", "octvc1.gsm.tap_filter.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP[1],
			{ "IndexList", "octvc1.gsm.tap_filter.list.indexlist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD;

void  register_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD[0],
			{ "ulFilterIndex", "octvc1.gsm.tap_filter.info.ulfilterindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFilterIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP;

void  register_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP[0],
			{ "ulFilterIndex", "octvc1.gsm.tap_filter.info.ulfilterindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFilterIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP[1],
			{ "Filter", "octvc1.gsm.tap_filter.info.filter",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Filter",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD;

void  register_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD[0],
			{ "ulFilterIndex", "octvc1.gsm.tap_filter.stats.ulfilterindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFilterIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP;

void  register_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP[0],
			{ "ulFilterIndex", "octvc1.gsm.tap_filter.stats.ulfilterindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFilterIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP[1],
			{ "Stats", "octvc1.gsm.tap_filter.stats.stats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Stats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD;

void  register_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD[0],
			{ "ulFilterIndex", "octvc1.gsm.tap_filter.modify.ulfilterindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFilterIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD[1],
			{ "Filter", "octvc1.gsm.tap_filter.modify.filter",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Filter",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP;

void  register_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP[0],
			{ "ulFilterIndex", "octvc1.gsm.tap_filter.modify.ulfilterindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulFilterIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_GSM_TRX_ID(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TRX_ID)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TRX_ID (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TRX_ID));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TRX_ID);
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_ID[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_ID, byTrxId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_ID[0], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_ID, byTrxId);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_ID, abyPad), "abyPad");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<
                              3
                            ; i++ )
			{
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_GSM_TRX_ID[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_ID, abyPad), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_GSM_TRX_ID[1], tvb, offset,
			1, temp_data, "[%d]: 0x%02x", i, temp_data );
		}
				offset+=1;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TRX_ID_CURSOR(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TRX_ID_CURSOR)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TRX_ID_CURSOR (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TRX_ID_CURSOR));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TRX_ID_CURSOR);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_ID_CURSOR, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TRX_ID_GET_CURSOR(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TRX_ID_GET_CURSOR)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TRX_ID_GET_CURSOR (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TRX_ID_GET_CURSOR));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TRX_ID_GET_CURSOR);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_ID_GET_CURSOR, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_ID_GET_CURSOR[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_ID_GET_CURSOR, ulGetMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_ID_GET_CURSOR[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_ID_GET_CURSOR, ulGetMode);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_RF_CONFIG(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_RF_CONFIG)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_RF_CONFIG (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_RF_CONFIG));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_RF_CONFIG);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_RF_CONFIG[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RF_CONFIG, ulRxGainDb), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_RF_CONFIG[0], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > 73 ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..73)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RF_CONFIG, ulRxGainDb);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_RF_CONFIG[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RF_CONFIG, ulTxAttndB), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_RF_CONFIG[1], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > 359 ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..359)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RF_CONFIG, ulTxAttndB);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TRX_CONFIG(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TRX_CONFIG)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TRX_CONFIG (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TRX_CONFIG));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TRX_CONFIG);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, ulBand), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, ulBand);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usTsc), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[1], tvb, offset,
			2, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > 31 ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..31)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usTsc);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usArfcn), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[2], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usArfcn);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usBcchArfcn), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[3], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usBcchArfcn);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usCentreArfcn), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[4], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usCentreArfcn);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usHsn), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[5], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usHsn);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usMaio), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[6], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usMaio);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[7], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usReserve), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[7], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, usReserve);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[8], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, ulHoppingFlag), temp_data, "%s (0x%x)", pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[8], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, ulHoppingFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, ulHoppingFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[9], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, ulHoppingListLength), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[9], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, ulHoppingListLength);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, ausHoppingList), "ausHoppingList");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<64; i++ )
			{
		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[10], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TRX_CONFIG, ausHoppingList), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_GSM_TRX_CONFIG[10], tvb, offset,
			2, temp_data, "[%d]: 0x%04x", i, temp_data );
		}
				offset+=2;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_PHYSICAL_CHANNEL_ID (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID);
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID, byTimeslotNb), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID[0], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID, byTimeslotNb);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID, abyPad), "abyPad");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<
                              3
                            ; i++ )
			{
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID, abyPad), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID[1], tvb, offset,
			1, temp_data, "[%d]: 0x%02x", i, temp_data );
		}
				offset+=1;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR, ulGetMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR, ulGetMode);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_SUBCHANNEL_STATUS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_SUBCHANNEL_STATUS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_SUBCHANNEL_STATUS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_SUBCHANNEL_STATUS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_SUBCHANNEL_STATUS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_SUBCHANNEL_STATUS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_SUBCHANNEL_STATUS, ulSubchannelNb), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_SUBCHANNEL_STATUS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_SUBCHANNEL_STATUS, ulSubchannelNb);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_GSM_SUBCHANNEL_STATUS[1], tvb, offset,
			4,"ulActiveUplinkSAPIMask:%s (0x%08x)", pExtValue->pszValue, temp_data);
		}else{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_GSM_SUBCHANNEL_STATUS[1], tvb, offset,
			4,"ulActiveUplinkSAPIMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_GSM_SAPI_MASK); i++ )
			{
				if( ( vals_tOCTVC1_GSM_SAPI_MASK[i].value && 
					( vals_tOCTVC1_GSM_SAPI_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_GSM_SAPI_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_GSM_SAPI_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_GSM_SAPI_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_SUBCHANNEL_STATUS, ulActiveUplinkSAPIMask);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_GSM_SUBCHANNEL_STATUS[2], tvb, offset,
			4,"ulActiveDownlinkSAPIMask:%s (0x%08x)", pExtValue->pszValue, temp_data);
		}else{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_GSM_SUBCHANNEL_STATUS[2], tvb, offset,
			4,"ulActiveDownlinkSAPIMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_GSM_SAPI_MASK); i++ )
			{
				if( ( vals_tOCTVC1_GSM_SAPI_MASK[i].value && 
					( vals_tOCTVC1_GSM_SAPI_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_GSM_SAPI_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_GSM_SAPI_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_GSM_SAPI_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_SUBCHANNEL_STATUS, ulActiveDownlinkSAPIMask);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_CIPHER_CONFIG(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_CIPHER_CONFIG)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_CIPHER_CONFIG (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_CIPHER_CONFIG));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_CIPHER_CONFIG);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_CIPHER_CONFIG[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_CIPHER_CONFIG, ulCipherId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_CIPHER_CONFIG[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_CIPHER_CONFIG, ulCipherId);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_CIPHER_CONFIG, abyKey), "abyKey");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<8; i++ )
			{
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_GSM_CIPHER_CONFIG[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_CIPHER_CONFIG, abyKey), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_GSM_CIPHER_CONFIG[1], tvb, offset,
			1, temp_data, "[%d]: 0x%02x", i, temp_data );
		}
				offset+=1;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_BUFF_ADDR(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_BUFF_ADDR)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_BUFF_ADDR (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_BUFF_ADDR));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_BUFF_ADDR);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_BUFF_ADDR[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_BUFF_ADDR, BuffAddr_TCHFS), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_BUFF_ADDR[0], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > 2147483647 ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..2147483647)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_BUFF_ADDR, BuffAddr_TCHFS);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_BUFF_ADDR[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_BUFF_ADDR, BuffAddr_RACH), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_BUFF_ADDR[1], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > 2147483647 ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..2147483647)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_BUFF_ADDR, BuffAddr_RACH);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_STUB_CHANNEL_ID(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_STUB_CHANNEL_ID)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_STUB_CHANNEL_ID (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_STUB_CHANNEL_ID));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_STUB_CHANNEL_ID);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_STUB_CHANNEL_ID[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_STUB_CHANNEL_ID, byTimeslotNb), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_STUB_CHANNEL_ID[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_STUB_CHANNEL_ID, byTimeslotNb);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_STUB_CHANNEL_ID[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_STUB_CHANNEL_ID, bySubChannelNb), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_STUB_CHANNEL_ID[1], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_STUB_CHANNEL_ID, bySubChannelNb);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_STUB_CHANNEL_ID[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_STUB_CHANNEL_ID, CHANNEL_TYPE), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_STUB_CHANNEL_ID[2], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_STUB_CHANNEL_ID, CHANNEL_TYPE);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_ID)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_LOGICAL_CHANNEL_ID (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_ID));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_ID);
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID, byTimeslotNb), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[0], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID, byTimeslotNb);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID, bySubChannelNb), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[1], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID, bySubChannelNb);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID, bySAPI), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[2], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID, bySAPI);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID, byDirection), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID[3], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID, byDirection);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR, ulGetMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR, ulGetMode);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG);
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG, byTimingAdvance), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[0], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG, byTimingAdvance);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG, byBSIC), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[1], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG, byBSIC);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG, byCmiPhase), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[2], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG, byCmiPhase);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG, byInitRate), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[3], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG, byInitRate);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG, abyRate), "abyRate:tOCTVC1_GSM_AMR_CODEC_MODE_ENUM");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<4; i++ )
			{
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG, abyRate), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG[4], tvb, offset,
			1, temp_data, "[%d]:%s (0x%02x)", i, val_to_str( temp_data, vals_tOCTVC1_GSM_AMR_CODEC_MODE_ENUM, "Unknown (%x)" ), temp_data );
		}
				offset+=1;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_LOGICAL_CHANNEL_DATA (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA, ulFrameNumber), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[0], tvb, offset,
			4, temp_data);
		if( (unsigned int)temp_data > cOCTVC1_GSM_MAX_FRAME_COUNT) 
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (..cOCTVC1_GSM_MAX_FRAME_COUNT)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA, ulFrameNumber);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA, ulPayloadType), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA, ulPayloadType);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA, ulDataLength), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[2], tvb, offset,
			4, temp_data);
		if( (unsigned int)temp_data > cOCTVC1_GSM_DATA_CONTENT_SIZE) 
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (..cOCTVC1_GSM_DATA_CONTENT_SIZE)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA, ulDataLength);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA, abyDataContent), "abyDataContent");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<468; i++ )
			{
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_DATA, abyDataContent), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA[3], tvb, offset,
			1, temp_data, "[%d]: 0x%02x", i, temp_data );
		}
				offset+=1;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA, ulFrameNumber), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA[0], tvb, offset,
			4, temp_data);
		if( (unsigned int)temp_data > cOCTVC1_GSM_MAX_FRAME_COUNT) 
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (..cOCTVC1_GSM_MAX_FRAME_COUNT)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA, ulFrameNumber);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA, ulDataLength), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA[1], tvb, offset,
			4, temp_data);
		if( (unsigned int)temp_data > cOCTVC1_GSM_DATA_CONTENT_SIZE) 
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (..cOCTVC1_GSM_DATA_CONTENT_SIZE)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA, ulDataLength);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA, abyDataContent), "abyDataContent");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<468; i++ )
			{
		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA, abyDataContent), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA[2], tvb, offset,
			1, temp_data, "[%d]: 0x%02x", i, temp_data );
		}
				offset+=1;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_MEASUREMENT_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MEASUREMENT_INFO)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MEASUREMENT_INFO (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MEASUREMENT_INFO));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_MEASUREMENT_INFO);
		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_int_format_value(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, sSNRDb), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[0], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, sSNRDb);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_int_format_value(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, sRSSIDbm), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[1], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, sRSSIDbm);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_int_format_value(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, sBurstTiming), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[2], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, sBurstTiming);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_int_format_value(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, sBurstTiming4x), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[3], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, sBurstTiming4x);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, usBERCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[4], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, usBERCnt);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, usBERTotalBitCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[5], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, usBERTotalBitCnt);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, usRxQualFullUp), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[6], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, usRxQualFullUp);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[7], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, usRxLevelFullUp), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MEASUREMENT_INFO[7], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MEASUREMENT_INFO, usRxLevelFullUp);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_RAW_MEASUREMENT_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_RAW_MEASUREMENT_INFO)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_RAW_MEASUREMENT_INFO (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_RAW_MEASUREMENT_INFO));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_RAW_MEASUREMENT_INFO);
		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_int_format_value(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, sSNRDb), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[0], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, sSNRDb);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_int_format_value(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, sRSSIDbm), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[1], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, sRSSIDbm);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_int_format_value(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, sBurstTiming), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[2], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, sBurstTiming);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_int_format_value(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, sBurstTiming4x), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[3], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, sBurstTiming4x);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, usBERCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[4], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, usBERCnt);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, usBERTotalBitCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[5], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, usBERTotalBitCnt);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, usTrSqErrCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[6], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, usTrSqErrCnt);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[7], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, usTrSqTotalBitCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[7], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, usTrSqTotalBitCnt);

		temp_data = tvb_get_ntohs( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_int_format_value(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[8], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, sFreqErrHz), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_GSM_RAW_MEASUREMENT_INFO[8], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_RAW_MEASUREMENT_INFO, sFreqErrHz);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TIMESLOT_POWER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TIMESLOT_POWER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TIMESLOT_POWER (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TIMESLOT_POWER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TIMESLOT_POWER);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TIMESLOT_POWER[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TIMESLOT_POWER, ulPowerMax), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TIMESLOT_POWER[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TIMESLOT_POWER, ulPowerMax);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TIMESLOT_POWER[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TIMESLOT_POWER, ulPowerAverage), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TIMESLOT_POWER[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TIMESLOT_POWER, ulPowerAverage);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TIMESLOT_POWER[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TIMESLOT_POWER, ulCount), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TIMESLOT_POWER[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TIMESLOT_POWER, ulCount);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_PHYSICAL_STATUS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_PHYSICAL_STATUS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_PHYSICAL_STATUS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_PHYSICAL_STATUS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_PHYSICAL_STATUS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_STATUS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_STATUS, ulChannelType), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_STATUS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_STATUS, ulChannelType);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_STATUS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_STATUS, ulSubchannelCount), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_STATUS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_STATUS, ulSubchannelCount);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_STATUS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_STATUS, ulTchDataCrcPassCount), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_STATUS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_STATUS, ulTchDataCrcPassCount);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_STATUS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_STATUS, ulTchDataCrcFailCount), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_STATUS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_STATUS, ulTchDataCrcFailCount);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_STATUS[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_STATUS, ulRachCount), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_PHYSICAL_STATUS[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_PHYSICAL_STATUS, ulRachCount);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TIMESLOT_STATUS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TIMESLOT_STATUS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TIMESLOT_STATUS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TIMESLOT_STATUS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TIMESLOT_STATUS);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TIMESLOT_STATUS, PhysicalStatus), "PhysicalStatus:tOCTVC1_GSM_PHYSICAL_STATUS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_STATUS( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TIMESLOT_STATUS, UplinkPower), "UplinkPower:tOCTVC1_GSM_TIMESLOT_POWER");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TIMESLOT_POWER( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_DL_STATUS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_DL_STATUS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_DL_STATUS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_DL_STATUS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_DL_STATUS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_DL_STATUS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_DL_STATUS, ulElapseRspCmdMaxUs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_DL_STATUS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_DL_STATUS, ulElapseRspCmdMaxUs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_DL_STATUS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_DL_STATUS, ulElapseRspCmdAvgUs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_DL_STATUS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_DL_STATUS, ulElapseRspCmdAvgUs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_DL_STATUS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_DL_STATUS, ulExpectedDelayExceedCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_DL_STATUS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_DL_STATUS, ulExpectedDelayExceedCnt);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_UL_STATUS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_UL_STATUS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_UL_STATUS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_UL_STATUS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_UL_STATUS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_UL_STATUS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_UL_STATUS, ulExceedByteMaxCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_UL_STATUS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_UL_STATUS, ulExceedByteMaxCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_UL_STATUS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_UL_STATUS, ulExpectedDataExceedCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_UL_STATUS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_UL_STATUS, ulExpectedDataExceedCnt);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_SCHED_STATUS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_SCHED_STATUS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_SCHED_STATUS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_SCHED_STATUS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_SCHED_STATUS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_SCHED_STATUS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_SCHED_STATUS, ulErrorDataInactiveCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_SCHED_STATUS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_SCHED_STATUS, ulErrorDataInactiveCnt);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TAP_FILTER_TRX(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TAP_FILTER_TRX)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TAP_FILTER_TRX (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TAP_FILTER_TRX));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TAP_FILTER_TRX);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER_TRX[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_TRX, ulAllTrxFlag), temp_data, "%s (0x%x)", pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER_TRX[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_TRX, ulAllTrxFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_TRX, ulAllTrxFlag);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_TRX, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL, ulEntryCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL[0], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > cOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY_COUNT_MAX ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..cOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY_COUNT_MAX)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL, ulEntryCnt);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL, aEntry), "aEntry:tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<8; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL, ulEntryCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL[0], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > cOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY_COUNT_MAX ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..cOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY_COUNT_MAX)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL, ulEntryCnt);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL, aEntry), "aEntry:tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<8; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TAP_FILTER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TAP_FILTER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TAP_FILTER (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TAP_FILTER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TAP_FILTER);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER, ulType), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER, ulType);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER[1], tvb, offset,
			4,"ulMask:%s (0x%08x)", pExtValue->pszValue, temp_data);
		}else{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER[1], tvb, offset,
			4,"ulMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_GSM_TAP_FILTER_MASK); i++ )
			{
				if( ( vals_tOCTVC1_GSM_TAP_FILTER_MASK[i].value && 
					( vals_tOCTVC1_GSM_TAP_FILTER_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_GSM_TAP_FILTER_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_GSM_TAP_FILTER_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_GSM_TAP_FILTER_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER, ulMask);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER, Trx), "Trx:tOCTVC1_GSM_TAP_FILTER_TRX");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TAP_FILTER_TRX( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER, PhysicalChannel), "PhysicalChannel:tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER, LogicalChannel), "LogicalChannel:tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_TAP_FILTER_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_TAP_FILTER_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_TAP_FILTER_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_TAP_FILTER_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_TAP_FILTER_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_STATS, ulTapIdCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_TAP_FILTER_STATS[0], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > cOCTVC1_GSM_TAP_FILTER_TAP_COUNT_MAX ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..cOCTVC1_GSM_TAP_FILTER_TAP_COUNT_MAX)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_STATS, ulTapIdCnt);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_STATS, ahTapId), "ahTapId");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<16; i++ )
			{
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_GSM_TAP_FILTER_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_TAP_FILTER_STATS, ahTapId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_GSM_TAP_FILTER_STATS[1], tvb, offset,
			4, temp_data, "[%d]: 0x%08x", i, temp_data );
		}
				offset+=4;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, ModuleData), "ModuleData:tOCTVC1_MODULE_DATA");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_MODULE_DATA( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, ulAntennaIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, ulAntennaIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, ulFrameNumber), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[4], tvb, offset,
			4, temp_data);
		if( (unsigned int)temp_data > cOCTVC1_GSM_MAX_FRAME_COUNT) 
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (..cOCTVC1_GSM_MAX_FRAME_COUNT)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, ulFrameNumber);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, ulPower), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, ulPower);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, ulDataSizeInBytes), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[6], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, ulDataSizeInBytes);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[7], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, byBadFlag), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[7], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, byBadFlag);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[8], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, byBurstId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[8], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, byBurstId);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[9], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, byReserved0), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[9], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, byReserved0);

		temp_data = tvb_get_guint8( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[10], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, byReserved1), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER[10], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER, byReserved1);

	}


	return offset;

};

/****************************************************************************
	Event dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT, MeasurementInfo), "MeasurementInfo:tOCTVC1_GSM_MEASUREMENT_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_MEASUREMENT_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT, Data), "Data:tOCTVC1_GSM_LOGICAL_CHANNEL_DATA");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT[2], tvb, offset,
			4, temp_data);
		if( (unsigned int)temp_data > cOCTVC1_GSM_MAX_FRAME_COUNT) 
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (..cOCTVC1_GSM_MAX_FRAME_COUNT)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT, ulFrameNumber);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT, ulBurstType);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[3], tvb, offset,
			4, temp_data);
		if( (unsigned int)temp_data > cOCTVC1_GSM_MAX_FRAME_COUNT) 
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (..cOCTVC1_GSM_MAX_FRAME_COUNT)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT, ulFrameNumber);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT, MeasurementInfo), "MeasurementInfo:tOCTVC1_GSM_MEASUREMENT_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_MEASUREMENT_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[5], tvb, offset,
			4, temp_data);
		if( (unsigned int)temp_data > cOCTVC1_GSM_RACH_IND_MSG_SIZE) 
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (..cOCTVC1_GSM_RACH_IND_MSG_SIZE)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT, ulMsgLength);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT, abyMsg), "abyMsg");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<32; i++ )
			{
		temp_data = tvb_get_guint8( tvb, offset );
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT[6], tvb, offset,
			1, temp_data, "[%d]: 0x%02x", i, temp_data );
		}
				offset+=1;
			}
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT, MeasurementInfo), "MeasurementInfo:tOCTVC1_GSM_RAW_MEASUREMENT_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_RAW_MEASUREMENT_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT, Data), "Data:tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT, ulFrameNumber);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT, ulStatus);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT, ulFrameNumber);

	}


	return 0;

};

/****************************************************************************
	CMD/RSP dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_OPEN_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_OPEN_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_OPEN_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_OPEN_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_OPEN_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_OPEN_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_OPEN_CMD, ulRfPortIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_OPEN_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_OPEN_CMD, Config), "Config:tOCTVC1_GSM_TRX_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_OPEN_CMD, RfConfig), "RfConfig:tOCTVC1_GSM_RF_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_RF_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_OPEN_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_OPEN_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_OPEN_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_OPEN_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_OPEN_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_OPEN_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_CLOSE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_CLOSE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_CLOSE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_CLOSE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_CLOSE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_CLOSE_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_CLOSE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_CLOSE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_CLOSE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_CLOSE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_CLOSE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_CLOSE_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STATUS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_STATUS_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_CMD, ulResetFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_CMD, ulResetFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STATUS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_RSP, DlStatus), "DlStatus:tOCTVC1_GSM_DL_STATUS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_DL_STATUS( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_RSP, UlStatus), "UlStatus:tOCTVC1_GSM_UL_STATUS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_UL_STATUS( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_RSP, SchedStatus), "SchedStatus:tOCTVC1_GSM_SCHED_STATUS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_SCHED_STATUS( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_RSP, aTSlotStatus), "aTSlotStatus:tOCTVC1_GSM_TIMESLOT_STATUS");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<8; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_GSM_TIMESLOT_STATUS), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TIMESLOT_STATUS( tvb, pinfo, sub_tree2, offset, NULL );
		}
			}
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_INFO_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_RSP, ulRfPortIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_RSP, Config), "Config:tOCTVC1_GSM_TRX_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_RESET_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_RESET_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_RESET_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_RESET_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_RESET_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_RESET_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_RESET_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_RESET_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_RESET_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_RESET_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_RESET_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_RESET_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_CMD, Config), "Config:tOCTVC1_GSM_TRX_CONFIG");
		{
			proto_tree*	sub_tree2;
			tWS_EXTRA_VALUE ExtraVal;
		ExtraVal.lValue = cOCTVC1_DO_NOT_MODIFY;
		ExtraVal.pszValue = "cOCTVC1_DO_NOT_MODIFY";
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_CONFIG( tvb, pinfo, sub_tree2, offset, &ExtraVal );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_LIST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_CMD, TrxIdCursor), "TrxIdCursor:tOCTVC1_GSM_TRX_ID_GET_CURSOR");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID_GET_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_LIST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_RSP, TrxIdCursor), "TrxIdCursor:tOCTVC1_GSM_TRX_ID_GET_CURSOR");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID_GET_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_LIST_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_RSP, ulNumTrxId);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_RSP, aTrxId), "aTrxId:tOCTVC1_GSM_TRX_ID_CURSOR");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<32; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_GSM_TRX_ID_CURSOR), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
			}
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP, ulCloseCount);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP, RfConfig), "RfConfig:tOCTVC1_GSM_RF_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_RF_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD, RfConfig), "RfConfig:tOCTVC1_GSM_RF_CONFIG");
		{
			proto_tree*	sub_tree2;
			tWS_EXTRA_VALUE ExtraVal;
		ExtraVal.lValue = cOCTVC1_DO_NOT_MODIFY;
		ExtraVal.pszValue = "cOCTVC1_DO_NOT_MODIFY";
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_RF_CONFIG( tvb, pinfo, sub_tree2, offset, &ExtraVal );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP, BuffADDR), "BuffADDR:tOCTVC1_GSM_BUFF_ADDR");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_BUFF_ADDR( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD, STUB_CHANNEL_ID), "STUB_CHANNEL_ID:tOCTVC1_GSM_STUB_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_STUB_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD, byTimeslotNb);

		temp_data = tvb_get_guint8( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD[2], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD, bySubChannelNb);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD, Config), "Config:tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohs( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP[2], tvb, offset,
			2, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP, usStatus);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP, Config), "Config:tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD, LchIdCursor), "LchIdCursor:tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP, LchIdCursor), "LchIdCursor:tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP, ulNumLchId);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP, aLchId), "aLchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<128; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
			}
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD[2], tvb, offset,
			4, temp_data);
		if( (unsigned int)temp_data > cOCTVC1_GSM_MAX_FRAME_COUNT) 
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (..cOCTVC1_GSM_MAX_FRAME_COUNT)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD, ulFrameNumber);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD, Data), "Data:tOCTVC1_GSM_LOGICAL_CHANNEL_DATA");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD, ulResetFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD, ulResetFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP, LchId), "LchId:tOCTVC1_GSM_LOGICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_LOGICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD, ulChannelType);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD, ulPayloadType);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD, ulResetFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD, ulResetFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP, PhysicalStatus), "PhysicalStatus:tOCTVC1_GSM_PHYSICAL_STATUS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_STATUS( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP, aSubchannnelStatus), "aSubchannnelStatus:tOCTVC1_GSM_SUBCHANNEL_STATUS");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<9; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_GSM_SUBCHANNEL_STATUS), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_SUBCHANNEL_STATUS( tvb, pinfo, sub_tree2, offset, NULL );
		}
			}
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD, PchIdCursor), "PchIdCursor:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP, PchIdCursor), "PchIdCursor:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP, ulNumPchId);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP, aPchId), "aPchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<32; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR( tvb, pinfo, sub_tree2, offset, NULL );
		}
			}
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP, ulChannelType);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP, ulPayloadType);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD, ulSubchannelNb);

		temp_data = tvb_get_guint8( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD[3], tvb, offset,
			1, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD, ulDirection);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD, Config), "Config:tOCTVC1_GSM_CIPHER_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_CIPHER_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD, abyPad), "abyPad");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<
		                              3
		                            ; i++ )
			{
		temp_data = tvb_get_guint8( tvb, offset );
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD[5], tvb, offset,
			1, temp_data, "[%d]: 0x%02x", i, temp_data );
		}
				offset+=1;
			}
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP[2], tvb, offset,
			4,"ulTxSubChannelMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_GSM_SUBCHANNEL_MASK); i++ )
			{
				if( ( vals_tOCTVC1_GSM_SUBCHANNEL_MASK[i].value && 
					( vals_tOCTVC1_GSM_SUBCHANNEL_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_GSM_SUBCHANNEL_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_GSM_SUBCHANNEL_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_GSM_SUBCHANNEL_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP, ulTxSubChannelMask);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP[3], tvb, offset,
			4,"ulRxSubChannelMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_GSM_SUBCHANNEL_MASK); i++ )
			{
				if( ( vals_tOCTVC1_GSM_SUBCHANNEL_MASK[i].value && 
					( vals_tOCTVC1_GSM_SUBCHANNEL_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_GSM_SUBCHANNEL_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_GSM_SUBCHANNEL_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_GSM_SUBCHANNEL_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP, ulRxSubChannelMask);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP, Config), "Config:tOCTVC1_GSM_CIPHER_CONFIG");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<9; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_GSM_CIPHER_CONFIG), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_CIPHER_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
			}
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP, TrxId), "TrxId:tOCTVC1_GSM_TRX_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TRX_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP, PchId), "PchId:tOCTVC1_GSM_PHYSICAL_CHANNEL_ID");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP, MeasurementInfo), "MeasurementInfo:tOCTVC1_GSM_MEASUREMENT_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_MEASUREMENT_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP, IndexList), "IndexList:tOCTVC1_LIST_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD, ulFilterIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP, ulFilterIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP, Filter), "Filter:tOCTVC1_GSM_TAP_FILTER");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TAP_FILTER( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD, ulFilterIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP, ulFilterIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP, Stats), "Stats:tOCTVC1_GSM_TAP_FILTER_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TAP_FILTER_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD, ulFilterIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD, Filter), "Filter:tOCTVC1_GSM_TAP_FILTER");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_GSM_TAP_FILTER( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP, ulFilterIndex);

	}


	return 0;

};
/****************************************************************************
	MODULE REGISTERED EXPORTED FUNCTION
 ****************************************************************************/

void ws_register_OCTVC1_GSM(void)
{
	/****************************************************************************
		Register Common struct
	****************************************************************************/
	register_tOCTVC1_GSM_TRX_ID(); 
	register_tOCTVC1_GSM_TRX_ID_CURSOR(); 
	register_tOCTVC1_GSM_TRX_ID_GET_CURSOR(); 
	register_tOCTVC1_GSM_RF_CONFIG(); 
	register_tOCTVC1_GSM_TRX_CONFIG(); 
	register_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID(); 
	register_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_CURSOR(); 
	register_tOCTVC1_GSM_PHYSICAL_CHANNEL_ID_GET_CURSOR(); 
	register_tOCTVC1_GSM_SUBCHANNEL_STATUS(); 
	register_tOCTVC1_GSM_CIPHER_CONFIG(); 
	register_tOCTVC1_GSM_BUFF_ADDR(); 
	register_tOCTVC1_GSM_STUB_CHANNEL_ID(); 
	register_tOCTVC1_GSM_LOGICAL_CHANNEL_ID(); 
	register_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_CURSOR(); 
	register_tOCTVC1_GSM_LOGICAL_CHANNEL_ID_GET_CURSOR(); 
	register_tOCTVC1_GSM_LOGICAL_CHANNEL_CONFIG(); 
	register_tOCTVC1_GSM_LOGICAL_CHANNEL_DATA(); 
	register_tOCTVC1_GSM_LOGICAL_CHANNEL_RAW_DATA(); 
	register_tOCTVC1_GSM_MEASUREMENT_INFO(); 
	register_tOCTVC1_GSM_RAW_MEASUREMENT_INFO(); 
	register_tOCTVC1_GSM_TIMESLOT_POWER(); 
	register_tOCTVC1_GSM_PHYSICAL_STATUS(); 
	register_tOCTVC1_GSM_TIMESLOT_STATUS(); 
	register_tOCTVC1_GSM_DL_STATUS(); 
	register_tOCTVC1_GSM_UL_STATUS(); 
	register_tOCTVC1_GSM_SCHED_STATUS(); 
	register_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL_ENTRY(); 
	register_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL_ENTRY(); 
	register_tOCTVC1_GSM_TAP_FILTER_TRX(); 
	register_tOCTVC1_GSM_TAP_FILTER_PHYSICAL_CHANNEL(); 
	register_tOCTVC1_GSM_TAP_FILTER_LOGICAL_CHANNEL(); 
	register_tOCTVC1_GSM_TAP_FILTER(); 
	register_tOCTVC1_GSM_TAP_FILTER_STATS(); 
	register_tOCTVC1_GSM_MODULE_DATA_RF_UPLINK_INPUT_HEADER(); 

	/****************************************************************************
		CMD/RSP Registered 
	****************************************************************************/
	register_tOCTVC1_GSM_MSG_TRX_OPEN_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_OPEN_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_CLOSE_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_CLOSE_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_STATUS_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_STATUS_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_RESET_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_RESET_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_MODIFY_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_MODIFY_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_LIST_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_LIST_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD(); 
	register_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP(); 
	register_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD(); 
	register_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP(); 
	register_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD(); 
	register_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP(); 
	register_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD(); 
	register_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP(); 
	register_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD(); 
	register_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP(); 

	/****************************************************************************
		Event Registered 
	****************************************************************************/
	register_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT(); 
	register_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT(); 
	register_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT(); 
	register_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT(); 
	register_tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT(); 
	register_tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT(); 

}

/****************************************************************************
	MODULE DISSECTOR FUNCTIONS
 ****************************************************************************/
int ws_dissect_OCTVC1_GSM_CMD( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_GSM_MSG_TRX_OPEN_CID: return dissect_tOCTVC1_GSM_MSG_TRX_OPEN_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_CLOSE_CID: return dissect_tOCTVC1_GSM_MSG_TRX_CLOSE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STATUS_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STATUS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_RESET_CID: return dissect_tOCTVC1_GSM_MSG_TRX_RESET_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_MODIFY_CID: return dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_LIST_CID: return dissect_tOCTVC1_GSM_MSG_TRX_LIST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_CLOSE_ALL_CID: return 0; break;
			case cOCTVC1_GSM_MSG_TRX_START_RECORD_CID: return dissect_tOCTVC1_GSM_MSG_TRX_START_RECORD_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STOP_RECORD_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_RF_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_RF_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_MODIFY_RF_CID: return dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CID: return dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CID: return dissect_tOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CID: return dissect_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CID: return dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CID: return dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CID: return dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CID: return dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TAP_FILTER_LIST_CID: return dissect_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TAP_FILTER_INFO_CID: return dissect_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TAP_FILTER_STATS_CID: return dissect_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CID: return dissect_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CMD( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_GSM_RSP( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_GSM_MSG_TRX_OPEN_CID: return dissect_tOCTVC1_GSM_MSG_TRX_OPEN_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_CLOSE_CID: return dissect_tOCTVC1_GSM_MSG_TRX_CLOSE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STATUS_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STATUS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_RESET_CID: return dissect_tOCTVC1_GSM_MSG_TRX_RESET_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_MODIFY_CID: return dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_LIST_CID: return dissect_tOCTVC1_GSM_MSG_TRX_LIST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_CLOSE_ALL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_CLOSE_ALL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_START_RECORD_CID: return dissect_tOCTVC1_GSM_MSG_TRX_START_RECORD_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STOP_RECORD_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STOP_RECORD_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_RF_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_RF_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_MODIFY_RF_CID: return dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_RF_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CID: return dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CID: return 0; break;
			case cOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CID: return 0; break;
			case cOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CID: return dissect_tOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CID: return dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CID: return dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CID: return dissect_tOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CID: return dissect_tOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CID: return dissect_tOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TAP_FILTER_LIST_CID: return dissect_tOCTVC1_GSM_MSG_TAP_FILTER_LIST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TAP_FILTER_INFO_CID: return dissect_tOCTVC1_GSM_MSG_TAP_FILTER_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TAP_FILTER_STATS_CID: return dissect_tOCTVC1_GSM_MSG_TAP_FILTER_STATS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CID: return dissect_tOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_RSP( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_GSM_EVT( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EID: return dissect_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EVT( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EID: return dissect_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EVT( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EID: return dissect_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EVT( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EID: return dissect_tOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EVT( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EID: return dissect_tOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EVT( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EID: return dissect_tOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EVT( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}

/****************************************************************************
	MODULE DISSECTOR EXPORTED FUNCTION
 ****************************************************************************/

int ws_dissect_OCTVC1_GSM( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if (message_type == cOCTVC1_MSG_TYPE_RESPONSE)
		return ws_dissect_OCTVC1_GSM_RSP( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_COMMAND)
		return ws_dissect_OCTVC1_GSM_CMD( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_NOTIFICATION )
		return ws_dissect_OCTVC1_GSM_EVT( CID, tvb, pinfo, tree);
	else
		return 1;

}

