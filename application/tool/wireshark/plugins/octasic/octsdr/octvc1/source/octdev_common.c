/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: octdev_common.c

      
Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark module dissector related functions

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/

#include <config.h>
#include <epan/packet.h>
#include "../../../include/oct_ws_macro.h"
#include "../include/module.h"
#include "../include/octdev_common.h"


#include <octdev_types.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTDEV_IP_VERSION_ENUM[] = 
 {
	{ cOCTDEV_IP_VERSION_ENUM_4, "cOCTDEV_IP_VERSION_ENUM_4" },
	{ cOCTDEV_IP_VERSION_ENUM_6, "cOCTDEV_IP_VERSION_ENUM_6" },
	{ cOCTDEV_IP_VERSION_ENUM_INVALID, "cOCTDEV_IP_VERSION_ENUM_INVALID" },
	{ 0,		NULL }
 };

#include <octdev_devices.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTDEV_DEVICES_TYPE_ENUM[] = 
 {
	{ cOCTDEV_DEVICES_TYPE_ENUM_INVALID, "cOCTDEV_DEVICES_TYPE_ENUM_INVALID" },
	{ cOCTDEV_DEVICES_TYPE_ENUM_OCT1010, "cOCTDEV_DEVICES_TYPE_ENUM_OCT1010" },
	{ cOCTDEV_DEVICES_TYPE_ENUM_OCT2200, "cOCTDEV_DEVICES_TYPE_ENUM_OCT2200" },
	{ cOCTDEV_DEVICES_TYPE_ENUM_CPU, "cOCTDEV_DEVICES_TYPE_ENUM_CPU" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTDEV_DEVICES_DSP_CORE_TYPE_ENUM[] = 
 {
	{ cOCTDEV_DEVICES_DSP_CORE_TYPE_ENUM_INVALID, "cOCTDEV_DEVICES_DSP_CORE_TYPE_ENUM_INVALID" },
	{ cOCTDEV_DEVICES_DSP_CORE_TYPE_ENUM_OPUS1, "cOCTDEV_DEVICES_DSP_CORE_TYPE_ENUM_OPUS1" },
	{ cOCTDEV_DEVICES_DSP_CORE_TYPE_ENUM_OPUS2, "cOCTDEV_DEVICES_DSP_CORE_TYPE_ENUM_OPUS2" },
	{ 0,		NULL }
 };
