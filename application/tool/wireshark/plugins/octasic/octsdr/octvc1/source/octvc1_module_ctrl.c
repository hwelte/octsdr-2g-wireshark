/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: octvc1_module_ctrl.c

      
Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark module dissector related functions

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/

#include <config.h>
#include <epan/packet.h>
#include "../../../include/oct_ws_macro.h"
#include "../include/module.h"
#include "../include/octvc1_common.h"

#include <ctrl/octvc1_ctrl_api_priv.h>


/****************************************************************************
	Event Registered 
 ****************************************************************************/

int ahf_tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_CTRL_MSG_MODULE_REJECT_SPV;

void  register_tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV[0],
			{ "ulExpectedTransactionId", "octvc1.ctrl.module.reject.ulexpectedtransactionid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulExpectedTransactionId",HFILL }

		},
		{
			&ahf_tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV[1],
			{ "ulRejectedCmdId", "octvc1.ctrl.module.reject.ulrejectedcmdid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulRejectedCmdId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_CTRL_MSG_MODULE_REJECT_SPV.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	CMD/RSP Registered 
 ****************************************************************************/

/****************************************************************************
	Event dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV, ulExpectedTransactionId);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CTRL_MSG_MODULE_REJECT_SPV, ulRejectedCmdId);

	}


	return 0;

};

/****************************************************************************
	CMD/RSP dissectors 
 ****************************************************************************/
/****************************************************************************
	MODULE REGISTERED EXPORTED FUNCTION
 ****************************************************************************/

void ws_register_OCTVC1_CTRL(void)
{
	/****************************************************************************
		Register Common struct
	****************************************************************************/

	/****************************************************************************
		CMD/RSP Registered 
	****************************************************************************/

	/****************************************************************************
		Event Registered 
	****************************************************************************/

}

/****************************************************************************
	MODULE DISSECTOR FUNCTIONS
 ****************************************************************************/
int ws_dissect_OCTVC1_CTRL_CMD( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_CTRL_RSP( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_CTRL_EVT( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			default: return 1;
		}
	}
	return 0;

}

/****************************************************************************
	MODULE DISSECTOR EXPORTED FUNCTION
 ****************************************************************************/

int ws_dissect_OCTVC1_CTRL( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if (message_type == cOCTVC1_MSG_TYPE_RESPONSE)
		return ws_dissect_OCTVC1_CTRL_RSP( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_COMMAND)
		return ws_dissect_OCTVC1_CTRL_CMD( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_NOTIFICATION )
		return ws_dissect_OCTVC1_CTRL_EVT( CID, tvb, pinfo, tree);
	else
		return 1;

}

