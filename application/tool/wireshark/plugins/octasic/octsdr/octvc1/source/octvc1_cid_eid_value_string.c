/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: OCTVC1_cid_eid_value_string.c

Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark value_string of all CID and EID

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/

#include <config.h>
#include <epan/packet.h>
#include "../../../include/oct_ws_macro.h"
#include "../include/module.h"
#include "../include/octvc1_common.h"

#include <main/octvc1_main_evt_priv.h>
#include <ctrl/octvc1_ctrl_api_priv.h>
#include <test/octvc1_test_api_priv.h>
#include <hw/octvc1_hw_evt_priv.h>
#include <irsc/octvc1_irsc_evt_priv.h>
#include <gsm/octvc1_gsm_evt_priv.h>


/****************************************************************************
	Module unique entry
 ****************************************************************************/
const value_string vals_OCTVC1_module_UID[] =
{
	{ (cOCTVC1_MAIN_UID),                                        "MAIN" },
	{ (cOCTVC1_CTRL_UID),                                        "CTRL" },
	{ (cOCTVC1_TEST_UID),                                        "TEST" },
	{ (cOCTVC1_HW_UID),                                          "HW" },
	{ (cOCTVC1_IRSC_UID),                                        "IRSC" },
	{ (cOCTVC1_GSM_UID),                                         "GSM" },
	{ 0,		NULL },
};

/****************************************************************************
	CID arrays
 ****************************************************************************/
const value_string vals_OCTVC1_MAIN_CID[] =
{
	{ (cOCTVC1_MAIN_MSG_TARGET_INFO_CID& 0x00000FFF),                     "TARGET_INFO" },
	{ (cOCTVC1_MAIN_MSG_TARGET_RESET_CID& 0x00000FFF),                    "TARGET_RESET" },
	{ (cOCTVC1_MAIN_MSG_FILE_OPEN_CID& 0x00000FFF),                       "FILE_OPEN" },
	{ (cOCTVC1_MAIN_MSG_FILE_CLOSE_CID& 0x00000FFF),                      "FILE_CLOSE" },
	{ (cOCTVC1_MAIN_MSG_FILE_WRITE_CID& 0x00000FFF),                      "FILE_WRITE" },
	{ (cOCTVC1_MAIN_MSG_FILE_READ_CID& 0x00000FFF),                       "FILE_READ" },
	{ (cOCTVC1_MAIN_MSG_FILE_INFO_CID& 0x00000FFF),                       "FILE_INFO" },
	{ (cOCTVC1_MAIN_MSG_FILE_LIST_CID& 0x00000FFF),                       "FILE_LIST" },
	{ (cOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_CID& 0x00000FFF),                "FILE_SYSTEM_INFO" },
	{ (cOCTVC1_MAIN_MSG_FILE_SYSTEM_INFO_FILE_CID& 0x00000FFF),           "FILE_SYSTEM_INFO_FILE" },
	{ (cOCTVC1_MAIN_MSG_FILE_SYSTEM_ADD_FILE_CID& 0x00000FFF),            "FILE_SYSTEM_ADD_FILE" },
	{ (cOCTVC1_MAIN_MSG_FILE_SYSTEM_DELETE_FILE_CID& 0x00000FFF),         "FILE_SYSTEM_DELETE_FILE" },
	{ (cOCTVC1_MAIN_MSG_LOG_ERASE_CID& 0x00000FFF),                       "LOG_ERASE" },
	{ (cOCTVC1_MAIN_MSG_LOG_INFO_CID& 0x00000FFF),                        "LOG_INFO" },
	{ (cOCTVC1_MAIN_MSG_LOG_STATS_CID& 0x00000FFF),                       "LOG_STATS" },
	{ (cOCTVC1_MAIN_MSG_LOG_INFO_TRACE_CID& 0x00000FFF),                  "LOG_INFO_TRACE" },
	{ (cOCTVC1_MAIN_MSG_LOG_START_TRACE_CID& 0x00000FFF),                 "LOG_START_TRACE" },
	{ (cOCTVC1_MAIN_MSG_LOG_STOP_TRACE_CID& 0x00000FFF),                  "LOG_STOP_TRACE" },
	{ (cOCTVC1_MAIN_MSG_LOG_ERASE_TRACE_CID& 0x00000FFF),                 "LOG_ERASE_TRACE" },
	{ (cOCTVC1_MAIN_MSG_LOG_STATS_TRACE_CID& 0x00000FFF),                 "LOG_STATS_TRACE" },
	{ (cOCTVC1_MAIN_MSG_PROCESS_INFO_CID& 0x00000FFF),                    "PROCESS_INFO" },
	{ (cOCTVC1_MAIN_MSG_PROCESS_LIST_CID& 0x00000FFF),                    "PROCESS_LIST" },
	{ (cOCTVC1_MAIN_MSG_PROCESS_MONITOR_CPU_USAGE_CID& 0x00000FFF),       "PROCESS_MONITOR_CPU_USAGE" },
	{ (cOCTVC1_MAIN_MSG_PROCESS_INFO_CPU_USAGE_CID& 0x00000FFF),          "PROCESS_INFO_CPU_USAGE" },
	{ (cOCTVC1_MAIN_MSG_PROCESS_STATS_CPU_USAGE_CID& 0x00000FFF),         "PROCESS_STATS_CPU_USAGE" },
	{ (cOCTVC1_MAIN_MSG_API_SYSTEM_LIST_SESSION_CID& 0x00000FFF),         "API_SYSTEM_LIST_SESSION" },
	{ (cOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_CID& 0x00000FFF),         "API_SYSTEM_INFO_SESSION" },
	{ (cOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_EVT_CID& 0x00000FFF),     "API_SYSTEM_INFO_SESSION_EVT" },
	{ (cOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_EVT_CID& 0x00000FFF),   "API_SYSTEM_MODIFY_SESSION_EVT" },
	{ (cOCTVC1_MAIN_MSG_API_SYSTEM_INFO_SESSION_HEARTBEAT_CID& 0x00000FFF), "API_SYSTEM_INFO_SESSION_HEARTBEAT" },
	{ (cOCTVC1_MAIN_MSG_API_SYSTEM_STATS_SESSION_HEARTBEAT_CID& 0x00000FFF), "API_SYSTEM_STATS_SESSION_HEARTBEAT" },
	{ (cOCTVC1_MAIN_MSG_API_SYSTEM_START_SESSION_HEARTBEAT_CID& 0x00000FFF), "API_SYSTEM_START_SESSION_HEARTBEAT" },
	{ (cOCTVC1_MAIN_MSG_API_SYSTEM_STOP_SESSION_HEARTBEAT_CID& 0x00000FFF), "API_SYSTEM_STOP_SESSION_HEARTBEAT" },
	{ (cOCTVC1_MAIN_MSG_API_SYSTEM_MODIFY_SESSION_HEARTBEAT_CID& 0x00000FFF), "API_SYSTEM_MODIFY_SESSION_HEARTBEAT" },
	{ (cOCTVC1_MAIN_MSG_APPLICATION_START_CID& 0x00000FFF),               "APPLICATION_START" },
	{ (cOCTVC1_MAIN_MSG_APPLICATION_STOP_CID& 0x00000FFF),                "APPLICATION_STOP" },
	{ (cOCTVC1_MAIN_MSG_APPLICATION_INFO_CID& 0x00000FFF),                "APPLICATION_INFO" },
	{ (cOCTVC1_MAIN_MSG_APPLICATION_INFO_SYSTEM_CID& 0x00000FFF),         "APPLICATION_INFO_SYSTEM" },
	{ (cOCTVC1_MAIN_MSG_APPLICATION_LIST_MODULE_CID& 0x00000FFF),         "APPLICATION_LIST_MODULE" },
	{ (cOCTVC1_MAIN_MSG_APPLICATION_INFO_MODULE_CID& 0x00000FFF),         "APPLICATION_INFO_MODULE" },
	{ (cOCTVC1_MAIN_MSG_APPLICATION_STATS_MODULE_CID& 0x00000FFF),        "APPLICATION_STATS_MODULE" },
	{ 0,		NULL },
};
const value_string vals_OCTVC1_MAIN_CID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_CTRL_CID[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_CTRL_CID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_TEST_CID[] =
{
	{ (cOCTVC1_TEST_MSG_DESCR_LIST_CID& 0x00000FFF),                      cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_DESCR_INFO_CID& 0x00000FFF),                      cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CID& 0x00000FFF),              cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_DESCR_STATS_CID& 0x00000FFF),                     cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_CID& 0x00000FFF),              cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_CID& 0x00000FFF),             cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_START_CID& 0x00000FFF),             cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_CID& 0x00000FFF),              cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CID& 0x00000FFF),          cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CID& 0x00000FFF),       cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CID& 0x00000FFF),         cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CID& 0x00000FFF),         cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CID& 0x00000FFF),        cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CID& 0x00000FFF),        cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CID& 0x00000FFF),         cOCTVC1_PRIVATE_ID_STRING },
	{ 0,		NULL },
};
const value_string vals_OCTVC1_TEST_CID_PRIV[] =
{
	{ (cOCTVC1_TEST_MSG_DESCR_LIST_CID& 0x00000FFF),                      "DESCR_LIST:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_DESCR_INFO_CID& 0x00000FFF),                      "DESCR_INFO:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_DESCR_INFO_BY_NAME_CID& 0x00000FFF),              "DESCR_INFO_BY_NAME:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_DESCR_STATS_CID& 0x00000FFF),                     "DESCR_STATS:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_CID& 0x00000FFF),              "RUNNER_SERVER_INFO:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_CID& 0x00000FFF),             "RUNNER_SERVER_STATS:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_START_CID& 0x00000FFF),             "RUNNER_SERVER_START:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_CID& 0x00000FFF),              "RUNNER_SERVER_STOP:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_ADD_TEST_CID& 0x00000FFF),          "RUNNER_SERVER_ADD_TEST:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_REMOVE_TEST_CID& 0x00000FFF),       "RUNNER_SERVER_REMOVE_TEST:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_LIST_TEST_CID& 0x00000FFF),         "RUNNER_SERVER_LIST_TEST:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_INFO_TEST_CID& 0x00000FFF),         "RUNNER_SERVER_INFO_TEST:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_STATS_TEST_CID& 0x00000FFF),        "RUNNER_SERVER_STATS_TEST:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_START_TEST_CID& 0x00000FFF),        "RUNNER_SERVER_START_TEST:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_TEST_MSG_RUNNER_SERVER_STOP_TEST_CID& 0x00000FFF),         "RUNNER_SERVER_STOP_TEST:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ 0,		NULL },
};
const value_string vals_OCTVC1_HW_CID[] =
{
	{ (cOCTVC1_HW_MSG_PCB_INFO_CID& 0x00000FFF),                          "PCB_INFO" },
	{ (cOCTVC1_HW_MSG_CPU_CORE_STATS_CID& 0x00000FFF),                    "CPU_CORE_STATS" },
	{ (cOCTVC1_HW_MSG_CPU_CORE_INFO_CID& 0x00000FFF),                     "CPU_CORE_INFO" },
	{ (cOCTVC1_HW_MSG_CPU_CORE_LIST_CID& 0x00000FFF),                     "CPU_CORE_LIST" },
	{ (cOCTVC1_HW_MSG_ETH_PORT_INFO_CID& 0x00000FFF),                     "ETH_PORT_INFO" },
	{ (cOCTVC1_HW_MSG_ETH_PORT_LIST_CID& 0x00000FFF),                     "ETH_PORT_LIST" },
	{ (cOCTVC1_HW_MSG_ETH_PORT_STATS_CID& 0x00000FFF),                    "ETH_PORT_STATS" },
	{ (cOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CID& 0x00000FFF),       cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_HW_MSG_ETH_PORT_MODIFY_CID& 0x00000FFF),                   "ETH_PORT_MODIFY" },
	{ (cOCTVC1_HW_MSG_RF_PORT_INFO_CID& 0x00000FFF),                      "RF_PORT_INFO" },
	{ (cOCTVC1_HW_MSG_RF_PORT_STATS_CID& 0x00000FFF),                     "RF_PORT_STATS" },
	{ (cOCTVC1_HW_MSG_RF_PORT_LIST_CID& 0x00000FFF),                      "RF_PORT_LIST" },
	{ (cOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CID& 0x00000FFF),              "RF_PORT_LIST_ANTENNA" },
	{ (cOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CID& 0x00000FFF),    "RF_PORT_INFO_ANTENNA_RX_CONFIG" },
	{ (cOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CID& 0x00000FFF),    "RF_PORT_INFO_ANTENNA_TX_CONFIG" },
	{ (cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_CID& 0x00000FFF),               "CLOCK_SYNC_MGR_INFO" },
	{ (cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CID& 0x00000FFF),              "CLOCK_SYNC_MGR_STATS" },
	{ (cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_CID& 0x00000FFF),        "CLOCK_SYNC_MGR_INFO_SOURCE" },
	{ (cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CID& 0x00000FFF),      "CLOCK_SYNC_MGR_MODIFY_SOURCE" },
	{ (cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CID& 0x00000FFF),      cOCTVC1_PRIVATE_ID_STRING },
	{ (cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STOP_SYNCHRO_CID& 0x00000FFF),       cOCTVC1_PRIVATE_ID_STRING },
	{ 0,		NULL },
};
const value_string vals_OCTVC1_HW_CID_PRIV[] =
{
	{ (cOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CID& 0x00000FFF),       "ETH_PORT_RESTRICTED_UNBLOCK:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CID& 0x00000FFF),      "CLOCK_SYNC_MGR_START_SYNCHRO:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ (cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STOP_SYNCHRO_CID& 0x00000FFF),       "CLOCK_SYNC_MGR_STOP_SYNCHRO:" \
						cOCTVC1_PRIVATE_VISIBILITY_USER \
						","},
	{ 0,		NULL },
};
const value_string vals_OCTVC1_IRSC_CID[] =
{
	{ (cOCTVC1_IRSC_MSG_PROCESS_INFO_CID& 0x00000FFF),                    "PROCESS_INFO" },
	{ (cOCTVC1_IRSC_MSG_PROCESS_STATS_CID& 0x00000FFF),                   "PROCESS_STATS" },
	{ (cOCTVC1_IRSC_MSG_PROCESS_LIST_CID& 0x00000FFF),                    "PROCESS_LIST" },
	{ (cOCTVC1_IRSC_MSG_PROCESS_INFO_IPC_PORT_CID& 0x00000FFF),           "PROCESS_INFO_IPC_PORT" },
	{ (cOCTVC1_IRSC_MSG_PROCESS_STATS_IPC_PORT_CID& 0x00000FFF),          "PROCESS_STATS_IPC_PORT" },
	{ (cOCTVC1_IRSC_MSG_PROCESS_LIST_IPC_PORT_CID& 0x00000FFF),           "PROCESS_LIST_IPC_PORT" },
	{ (cOCTVC1_IRSC_MSG_PROCESS_INFO_TASK_CID& 0x00000FFF),               "PROCESS_INFO_TASK" },
	{ (cOCTVC1_IRSC_MSG_PROCESS_STATS_TASK_CID& 0x00000FFF),              "PROCESS_STATS_TASK" },
	{ (cOCTVC1_IRSC_MSG_PROCESS_LIST_TASK_CID& 0x00000FFF),               "PROCESS_LIST_TASK" },
	{ (cOCTVC1_IRSC_MSG_OBJMGR_INFO_CID& 0x00000FFF),                     "OBJMGR_INFO" },
	{ (cOCTVC1_IRSC_MSG_OBJMGR_STATS_CID& 0x00000FFF),                    "OBJMGR_STATS" },
	{ (cOCTVC1_IRSC_MSG_OBJMGR_LIST_CID& 0x00000FFF),                     "OBJMGR_LIST" },
	{ (cOCTVC1_IRSC_MSG_API_SYSTEM_START_MONITORING_CID& 0x00000FFF),     "API_SYSTEM_START_MONITORING" },
	{ (cOCTVC1_IRSC_MSG_API_SYSTEM_STOP_MONITORING_CID& 0x00000FFF),      "API_SYSTEM_STOP_MONITORING" },
	{ (cOCTVC1_IRSC_MSG_API_SYSTEM_LIST_COMMAND_CID& 0x00000FFF),         "API_SYSTEM_LIST_COMMAND" },
	{ (cOCTVC1_IRSC_MSG_API_SYSTEM_STATS_COMMAND_CID& 0x00000FFF),        "API_SYSTEM_STATS_COMMAND" },
	{ (cOCTVC1_IRSC_MSG_API_SYSTEM_LIST_SESSION_CID& 0x00000FFF),         "API_SYSTEM_LIST_SESSION" },
	{ (cOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_CID& 0x00000FFF),         "API_SYSTEM_INFO_SESSION" },
	{ (cOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_CID& 0x00000FFF),        "API_SYSTEM_STATS_SESSION" },
	{ (cOCTVC1_IRSC_MSG_API_SYSTEM_INFO_SESSION_EVT_CID& 0x00000FFF),     "API_SYSTEM_INFO_SESSION_EVT" },
	{ (cOCTVC1_IRSC_MSG_API_SYSTEM_STATS_SESSION_EVT_CID& 0x00000FFF),    "API_SYSTEM_STATS_SESSION_EVT" },
	{ (cOCTVC1_IRSC_MSG_APPLICATION_LIST_TAP_CID& 0x00000FFF),            "APPLICATION_LIST_TAP" },
	{ (cOCTVC1_IRSC_MSG_APPLICATION_INFO_TAP_CID& 0x00000FFF),            "APPLICATION_INFO_TAP" },
	{ (cOCTVC1_IRSC_MSG_APPLICATION_STATS_TAP_CID& 0x00000FFF),           "APPLICATION_STATS_TAP" },
	{ (cOCTVC1_IRSC_MSG_APPLICATION_START_TAP_CID& 0x00000FFF),           "APPLICATION_START_TAP" },
	{ (cOCTVC1_IRSC_MSG_APPLICATION_STOP_TAP_CID& 0x00000FFF),            "APPLICATION_STOP_TAP" },
	{ 0,		NULL },
};
const value_string vals_OCTVC1_IRSC_CID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_GSM_CID[] =
{
	{ (cOCTVC1_GSM_MSG_TRX_OPEN_CID& 0x00000FFF),                         "TRX_OPEN" },
	{ (cOCTVC1_GSM_MSG_TRX_CLOSE_CID& 0x00000FFF),                        "TRX_CLOSE" },
	{ (cOCTVC1_GSM_MSG_TRX_STATUS_CID& 0x00000FFF),                       "TRX_STATUS" },
	{ (cOCTVC1_GSM_MSG_TRX_INFO_CID& 0x00000FFF),                         "TRX_INFO" },
	{ (cOCTVC1_GSM_MSG_TRX_RESET_CID& 0x00000FFF),                        "TRX_RESET" },
	{ (cOCTVC1_GSM_MSG_TRX_MODIFY_CID& 0x00000FFF),                       "TRX_MODIFY" },
	{ (cOCTVC1_GSM_MSG_TRX_LIST_CID& 0x00000FFF),                         "TRX_LIST" },
	{ (cOCTVC1_GSM_MSG_TRX_CLOSE_ALL_CID& 0x00000FFF),                    "TRX_CLOSE_ALL" },
	{ (cOCTVC1_GSM_MSG_TRX_START_RECORD_CID& 0x00000FFF),                 "TRX_START_RECORD" },
	{ (cOCTVC1_GSM_MSG_TRX_STOP_RECORD_CID& 0x00000FFF),                  "TRX_STOP_RECORD" },
	{ (cOCTVC1_GSM_MSG_TRX_INFO_RF_CID& 0x00000FFF),                      "TRX_INFO_RF" },
	{ (cOCTVC1_GSM_MSG_TRX_MODIFY_RF_CID& 0x00000FFF),                    "TRX_MODIFY_RF" },
	{ (cOCTVC1_GSM_MSG_TRX_REQUEST_STUBB_LOOPBACK_TEST_CID& 0x00000FFF),  "TRX_REQUEST_STUBB_LOOPBACK_TEST" },
	{ (cOCTVC1_GSM_MSG_TRX_START_STUBB_LOOPBACK_TEST_CID& 0x00000FFF),    "TRX_START_STUBB_LOOPBACK_TEST" },
	{ (cOCTVC1_GSM_MSG_TRX_STOP_STUBB_LOOPBACK_TEST_CID& 0x00000FFF),     "TRX_STOP_STUBB_LOOPBACK_TEST" },
	{ (cOCTVC1_GSM_MSG_TRX_ACTIVATE_LOGICAL_CHANNEL_CID& 0x00000FFF),     "TRX_ACTIVATE_LOGICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_DEACTIVATE_LOGICAL_CHANNEL_CID& 0x00000FFF),   "TRX_DEACTIVATE_LOGICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_STATUS_LOGICAL_CHANNEL_CID& 0x00000FFF),       "TRX_STATUS_LOGICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_INFO_LOGICAL_CHANNEL_CID& 0x00000FFF),         "TRX_INFO_LOGICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_LIST_LOGICAL_CHANNEL_CID& 0x00000FFF),         "TRX_LIST_LOGICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CID& 0x00000FFF), "TRX_START_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS" },
	{ (cOCTVC1_GSM_MSG_TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS_CID& 0x00000FFF), "TRX_STOP_LOGICAL_CHANNEL_RAW_DATA_INDICATIONS" },
	{ (cOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME_CID& 0x00000FFF), "TRX_REQUEST_LOGICAL_CHANNEL_EMPTY_FRAME" },
	{ (cOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_DATA_CID& 0x00000FFF), "TRX_REQUEST_LOGICAL_CHANNEL_DATA" },
	{ (cOCTVC1_GSM_MSG_TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST_CID& 0x00000FFF), "TRX_REQUEST_LOGICAL_CHANNEL_CMU_LOOPBACK_TEST" },
	{ (cOCTVC1_GSM_MSG_TRX_ACTIVATE_PHYSICAL_CHANNEL_CID& 0x00000FFF),    "TRX_ACTIVATE_PHYSICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_DEACTIVATE_PHYSICAL_CHANNEL_CID& 0x00000FFF),  "TRX_DEACTIVATE_PHYSICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_STATUS_PHYSICAL_CHANNEL_CID& 0x00000FFF),      "TRX_STATUS_PHYSICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_RESET_PHYSICAL_CHANNEL_CID& 0x00000FFF),       "TRX_RESET_PHYSICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_LIST_PHYSICAL_CHANNEL_CID& 0x00000FFF),        "TRX_LIST_PHYSICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CID& 0x00000FFF),        "TRX_INFO_PHYSICAL_CHANNEL" },
	{ (cOCTVC1_GSM_MSG_TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING_CID& 0x00000FFF), "TRX_MODIFY_PHYSICAL_CHANNEL_CIPHERING" },
	{ (cOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_CIPHERING_CID& 0x00000FFF), "TRX_INFO_PHYSICAL_CHANNEL_CIPHERING" },
	{ (cOCTVC1_GSM_MSG_TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT_CID& 0x00000FFF), "TRX_INFO_PHYSICAL_CHANNEL_MEASUREMENT" },
	{ (cOCTVC1_GSM_MSG_TAP_FILTER_LIST_CID& 0x00000FFF),                  "TAP_FILTER_LIST" },
	{ (cOCTVC1_GSM_MSG_TAP_FILTER_INFO_CID& 0x00000FFF),                  "TAP_FILTER_INFO" },
	{ (cOCTVC1_GSM_MSG_TAP_FILTER_STATS_CID& 0x00000FFF),                 "TAP_FILTER_STATS" },
	{ (cOCTVC1_GSM_MSG_TAP_FILTER_MODIFY_CID& 0x00000FFF),                "TAP_FILTER_MODIFY" },
	{ 0,		NULL },
};
const value_string vals_OCTVC1_GSM_CID_PRIV[] =
{
	{ 0,		NULL },
};

/****************************************************************************
	EID arrays
 ****************************************************************************/
const value_string vals_OCTVC1_MAIN_EID[] =
{
	{ (cOCTVC1_MAIN_MSG_PROCESS_CPU_USAGE_REPORT_EID& 0x0000FFFF),        "PROCESS_CPU_USAGE_REPORT" },
	{ (cOCTVC1_MAIN_MSG_PROCESS_DUMP_EID& 0x0000FFFF),                    "PROCESS_DUMP" },
	{ (cOCTVC1_MAIN_MSG_API_SYSTEM_SESSION_HEARTBEAT_EID& 0x0000FFFF),    "API_SYSTEM_SESSION_HEARTBEAT" },
	{ (cOCTVC1_MAIN_MSG_APPLICATION_STATE_CHANGE_EID& 0x0000FFFF),        "APPLICATION_STATE_CHANGE" },
	{ 0,		NULL },
};

const value_string vals_OCTVC1_MAIN_EID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_CTRL_EID[] =
{
	{ 0,		NULL },
};

const value_string vals_OCTVC1_CTRL_EID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_TEST_EID[] =
{
	{ 0,		NULL },
};

const value_string vals_OCTVC1_TEST_EID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_HW_EID[] =
{
	{ (cOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EID& 0x0000FFFF),              "CPU_CORE_EXEC_REPORT" },
	{ (cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EID& 0x0000FFFF),      "CLOCK_SYNC_MGR_STATUS_CHANGE" },
	{ 0,		NULL },
};

const value_string vals_OCTVC1_HW_EID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_IRSC_EID[] =
{
	{ (cOCTVC1_IRSC_MSG_PROCESS_DUMP_EID& 0x0000FFFF),                    "PROCESS_DUMP" },
	{ 0,		NULL },
};

const value_string vals_OCTVC1_IRSC_EID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_GSM_EID[] =
{
	{ (cOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_DATA_INDICATION_EID& 0x0000FFFF), "TRX_LOGICAL_CHANNEL_DATA_INDICATION" },
	{ (cOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION_EID& 0x0000FFFF), "TRX_LOGICAL_CHANNEL_READY_TO_SEND_INDICATION" },
	{ (cOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RACH_INDICATION_EID& 0x0000FFFF), "TRX_LOGICAL_CHANNEL_RACH_INDICATION" },
	{ (cOCTVC1_GSM_MSG_TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION_EID& 0x0000FFFF), "TRX_LOGICAL_CHANNEL_RAW_DATA_INDICATION" },
	{ (cOCTVC1_GSM_MSG_TRX_TIME_INDICATION_EID& 0x0000FFFF),              "TRX_TIME_INDICATION" },
	{ (cOCTVC1_GSM_MSG_TRX_STATUS_CHANGE_EID& 0x0000FFFF),                "TRX_STATUS_CHANGE" },
	{ 0,		NULL },
};

const value_string vals_OCTVC1_GSM_EID_PRIV[] =
{
	{ 0,		NULL },
};

/****************************************************************************
	SID arrays
 ****************************************************************************/
const value_string vals_OCTVC1_MAIN_SID[] =
{
	{ 0,		NULL },
};

const value_string vals_OCTVC1_MAIN_SID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_CTRL_SID[] =
{
	{ (cOCTVC1_CTRL_MSG_MODULE_REJECT_SID& 0x00000FFF),                   "MODULE_REJECT" },
	{ 0,		NULL },
};

const value_string vals_OCTVC1_CTRL_SID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_TEST_SID[] =
{
	{ 0,		NULL },
};

const value_string vals_OCTVC1_TEST_SID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_HW_SID[] =
{
	{ 0,		NULL },
};

const value_string vals_OCTVC1_HW_SID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_IRSC_SID[] =
{
	{ 0,		NULL },
};

const value_string vals_OCTVC1_IRSC_SID_PRIV[] =
{
	{ 0,		NULL },
};
const value_string vals_OCTVC1_GSM_SID[] =
{
	{ 0,		NULL },
};

const value_string vals_OCTVC1_GSM_SID_PRIV[] =
{
	{ 0,		NULL },
};

/****************************************************************************
	typedef struct tModuleColInfo
 ****************************************************************************/
extern void ws_register_OCTVC1_MAIN(void);
extern int ws_dissect_OCTVC1_MAIN( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree );
extern void ws_register_OCTVC1_CTRL(void);
extern int ws_dissect_OCTVC1_CTRL( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree );
extern void ws_register_OCTVC1_TEST(void);
extern int ws_dissect_OCTVC1_TEST( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree );
extern void ws_register_OCTVC1_HW(void);
extern int ws_dissect_OCTVC1_HW( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree );
extern void ws_register_OCTVC1_IRSC(void);
extern int ws_dissect_OCTVC1_IRSC( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree );
extern void ws_register_OCTVC1_GSM(void);
extern int ws_dissect_OCTVC1_GSM( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree );
;
tModuleColInfo aOCTVC1ModuleColInfo[] =
{
	{ "MAIN", vals_OCTVC1_MAIN_CID,vals_OCTVC1_MAIN_EID,vals_OCTVC1_MAIN_SID, ws_register_OCTVC1_MAIN, ws_dissect_OCTVC1_MAIN,vals_OCTVC1_MAIN_CID_PRIV,vals_OCTVC1_MAIN_EID_PRIV },
	{ "CTRL", vals_OCTVC1_CTRL_CID,vals_OCTVC1_CTRL_EID,vals_OCTVC1_CTRL_SID, ws_register_OCTVC1_CTRL, ws_dissect_OCTVC1_CTRL,vals_OCTVC1_CTRL_CID_PRIV,vals_OCTVC1_CTRL_EID_PRIV },
	{ "TEST", vals_OCTVC1_TEST_CID,vals_OCTVC1_TEST_EID,vals_OCTVC1_TEST_SID, ws_register_OCTVC1_TEST, ws_dissect_OCTVC1_TEST,vals_OCTVC1_TEST_CID_PRIV,vals_OCTVC1_TEST_EID_PRIV },
	{ "HW", vals_OCTVC1_HW_CID,vals_OCTVC1_HW_EID,vals_OCTVC1_HW_SID, ws_register_OCTVC1_HW, ws_dissect_OCTVC1_HW,vals_OCTVC1_HW_CID_PRIV,vals_OCTVC1_HW_EID_PRIV },
	{ "IRSC", vals_OCTVC1_IRSC_CID,vals_OCTVC1_IRSC_EID,vals_OCTVC1_IRSC_SID, ws_register_OCTVC1_IRSC, ws_dissect_OCTVC1_IRSC,vals_OCTVC1_IRSC_CID_PRIV,vals_OCTVC1_IRSC_EID_PRIV },
	{ "GSM", vals_OCTVC1_GSM_CID,vals_OCTVC1_GSM_EID,vals_OCTVC1_GSM_SID, ws_register_OCTVC1_GSM, ws_dissect_OCTVC1_GSM,vals_OCTVC1_GSM_CID_PRIV,vals_OCTVC1_GSM_EID_PRIV },

};


/*************************************************************************
  Registered dissector module 
**************************************************************************/
void ws_register_dissector_module(void)
{
  /* Register all modules */
  unsigned int i;
  for( i=0; i<mWS_COUNTOF(aOCTVC1ModuleColInfo); i++ )
		aOCTVC1ModuleColInfo[i].Register_fnc();

	ws_register_OCTVC1_common();
}
