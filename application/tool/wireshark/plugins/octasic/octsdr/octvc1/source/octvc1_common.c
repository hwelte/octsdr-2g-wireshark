/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: octvc1_common.c

      
Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark module dissector related functions

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/

#include <config.h>
#include <epan/packet.h>
#include "../../../include/oct_ws_macro.h"
#include "../include/module.h"
#include "../include/octvc1_common.h"


#include <octvc1_cursor.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_OBJECT_CURSOR_ENUM[] = 
 {
	{ cOCTVC1_OBJECT_CURSOR_ENUM_DONE, "cOCTVC1_OBJECT_CURSOR_ENUM_DONE" },
	{ cOCTVC1_OBJECT_CURSOR_ENUM_SPECIFIC, "cOCTVC1_OBJECT_CURSOR_ENUM_SPECIFIC" },
	{ cOCTVC1_OBJECT_CURSOR_ENUM_FIRST, "cOCTVC1_OBJECT_CURSOR_ENUM_FIRST" },
	{ cOCTVC1_OBJECT_CURSOR_ENUM_NEXT, "cOCTVC1_OBJECT_CURSOR_ENUM_NEXT" },
	{ cOCTVC1_OBJECT_CURSOR_ENUM_SUB_OBJECT_FIRST, "cOCTVC1_OBJECT_CURSOR_ENUM_SUB_OBJECT_FIRST" },
	{ cOCTVC1_OBJECT_CURSOR_ENUM_SUB_OBJECT_NEXT, "cOCTVC1_OBJECT_CURSOR_ENUM_SUB_OBJECT_NEXT" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_CURSOR_HANDLE_OBJECT_GET;

void  register_tOCTVC1_CURSOR_HANDLE_OBJECT_GET(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET[0],
			{ "hObject", "octvc1.cursor.handle_object_get.hobject",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hObject",HFILL }

		},
		{
			&ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET[1],
			{ "ulGetMode", "octvc1.cursor.handle_object_get.ulgetmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_OBJECT_CURSOR_ENUM), 0x0,
			"ulGetMode",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_CURSOR_HANDLE_OBJECT_GET.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT;

void  register_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT[0],
			{ "hObject", "octvc1.cursor.handle_object_get_handle_object.hobject",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hObject",HFILL }

		},
		{
			&ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT[1],
			{ "ulGetMode", "octvc1.cursor.handle_object_get_handle_object.ulgetmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_OBJECT_CURSOR_ENUM), 0x0,
			"ulGetMode",HFILL }

		},
		{
			&ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT[2],
			{ "hHandleObjectGet", "octvc1.cursor.handle_object_get_handle_object.hhandleobjectget",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hHandleObjectGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID;

void  register_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID[0],
			{ "hObject", "octvc1.cursor.handle_object_get_sub_object_id.hobject",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hObject",HFILL }

		},
		{
			&ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID[1],
			{ "ulGetMode", "octvc1.cursor.handle_object_get_sub_object_id.ulgetmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_OBJECT_CURSOR_ENUM), 0x0,
			"ulGetMode",HFILL }

		},
		{
			&ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID[2],
			{ "ulSubObjectId", "octvc1.cursor.handle_object_get_sub_object_id.ulsubobjectid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulSubObjectId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_CURSOR_INDEX_GET[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_CURSOR_INDEX_GET;

void  register_tOCTVC1_CURSOR_INDEX_GET(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_CURSOR_INDEX_GET[0],
			{ "ulIndex", "octvc1.cursor.index_get.ulindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_CURSOR_INDEX_GET[1],
			{ "ulGetMode", "octvc1.cursor.index_get.ulgetmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_OBJECT_CURSOR_ENUM), 0x0,
			"ulGetMode",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_CURSOR_INDEX_GET.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_CURSOR_INDEX_GET_SUB_INDEX;

void  register_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX[0],
			{ "ulParentIndex", "octvc1.cursor.index_get_sub_index.ulparentindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulParentIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX[1],
			{ "ulGetMode", "octvc1.cursor.index_get_sub_index.ulgetmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_OBJECT_CURSOR_ENUM), 0x0,
			"ulGetMode",HFILL }

		},
		{
			&ahf_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX[2],
			{ "ulSubIndex", "octvc1.cursor.index_get_sub_index.ulsubindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulSubIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_CURSOR_INDEX_GET_SUB_INDEX.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_CURSOR_HANDLE_OBJECT_GET)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_CURSOR_HANDLE_OBJECT_GET (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_CURSOR_HANDLE_OBJECT_GET));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_CURSOR_HANDLE_OBJECT_GET);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET, hObject), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET, hObject);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET, ulGetMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET, ulGetMode);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT, hObject), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT, hObject);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT, ulGetMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT, ulGetMode);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT, hHandleObjectGet), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT, hHandleObjectGet);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID, hObject), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID, hObject);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID, ulGetMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID, ulGetMode);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID, ulSubObjectId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID, ulSubObjectId);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_CURSOR_INDEX_GET(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_CURSOR_INDEX_GET)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_CURSOR_INDEX_GET (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_CURSOR_INDEX_GET));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_CURSOR_INDEX_GET);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_INDEX_GET[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_INDEX_GET, ulIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_INDEX_GET[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_INDEX_GET, ulIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_INDEX_GET[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_INDEX_GET, ulGetMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_INDEX_GET[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_INDEX_GET, ulGetMode);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX, ulParentIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX, ulParentIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX, ulGetMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX, ulGetMode);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX, ulSubIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX, ulSubIndex);

	}


	return offset;

};

#include <octvc1_buffer.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_BUFFER_FORMAT_ENUM[] = 
 {
	{ cOCTVC1_BUFFER_FORMAT_ENUM_UNKNOWN, "cOCTVC1_BUFFER_FORMAT_ENUM_UNKNOWN" },
	{ cOCTVC1_BUFFER_FORMAT_MAIN_ENUM_PCAP_TRACE, "cOCTVC1_BUFFER_FORMAT_MAIN_ENUM_PCAP_TRACE" },
	{ cOCTVC1_BUFFER_FORMAT_MAIN_ENUM_FILE_SYS, "cOCTVC1_BUFFER_FORMAT_MAIN_ENUM_FILE_SYS" },
	{ 0,		NULL }
 };

#include <octvc1_eth.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_ETH_PORT_ID_ENUM[] = 
 {
	{ cOCTVC1_ETH_PORT_ID_ENUM_0, "cOCTVC1_ETH_PORT_ID_ENUM_0" },
	{ cOCTVC1_ETH_PORT_ID_ENUM_1, "cOCTVC1_ETH_PORT_ID_ENUM_1" },
	{ cOCTVC1_ETH_PORT_ID_ENUM_2, "cOCTVC1_ETH_PORT_ID_ENUM_2" },
	{ cOCTVC1_ETH_PORT_ID_ENUM_3, "cOCTVC1_ETH_PORT_ID_ENUM_3" },
	{ cOCTVC1_ETH_PORT_ID_ENUM_INVALID, "cOCTVC1_ETH_PORT_ID_ENUM_INVALID" },
	{ 0,		NULL }
 };

#include <octvc1_handle.h>

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_OBJECT32_NAME[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_OBJECT32_NAME;

void  register_tOCTVC1_OBJECT32_NAME(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_OBJECT32_NAME[0],
			{ "ulObject32", "octvc1.handle.toctvc1_object32_name.ulobject32",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulObject32",HFILL }

		},
		{
			&ahf_tOCTVC1_OBJECT32_NAME[1],
			{ "szDisplayName", "octvc1.handle.toctvc1_object32_name.szdisplayname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szDisplayName",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_OBJECT32_NAME.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_OBJECT32_NAME(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_OBJECT32_NAME)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_OBJECT32_NAME (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_OBJECT32_NAME));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_OBJECT32_NAME);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_OBJECT32_NAME[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_OBJECT32_NAME, ulObject32), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_OBJECT32_NAME[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_OBJECT32_NAME, ulObject32);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_OBJECT32_NAME[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_OBJECT32_NAME, szDisplayName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_OBJECT32_NAME, szDisplayName);

	}


	return offset;

};

#include <octvc1_ip.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_IP_VERSION_ENUM[] = 
 {
	{ cOCTVC1_IP_VERSION_ENUM_4, "cOCTVC1_IP_VERSION_ENUM_4" },
	{ cOCTVC1_IP_VERSION_ENUM_6, "cOCTVC1_IP_VERSION_ENUM_6" },
	{ cOCTVC1_IP_VERSION_ENUM_INVALID, "cOCTVC1_IP_VERSION_ENUM_INVALID" },
	{ 0,		NULL }
 };

#include <octvc1_udp.h>

#include <octvc1_vlan.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_VLAN_PROTOCOL_ID_ENUM[] = 
 {
	{ cOCTVC1_VLAN_PROTOCOL_ID_ENUM_8100, "cOCTVC1_VLAN_PROTOCOL_ID_ENUM_8100" },
	{ cOCTVC1_VLAN_PROTOCOL_ID_ENUM_88A8, "cOCTVC1_VLAN_PROTOCOL_ID_ENUM_88A8" },
	{ cOCTVC1_VLAN_PROTOCOL_ID_ENUM_9100, "cOCTVC1_VLAN_PROTOCOL_ID_ENUM_9100" },
	{ cOCTVC1_VLAN_PROTOCOL_ID_ENUM_9200, "cOCTVC1_VLAN_PROTOCOL_ID_ENUM_9200" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_VLAN_TAG[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_VLAN_TAG;

void  register_tOCTVC1_VLAN_TAG(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_VLAN_TAG[0],
			{ "ulPriority", "octvc1.vlan.tag.ulpriority",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPriority",HFILL }

		},
		{
			&ahf_tOCTVC1_VLAN_TAG[1],
			{ "ulVlanId", "octvc1.vlan.tag.ulvlanid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulVlanId",HFILL }

		},
		{
			&ahf_tOCTVC1_VLAN_TAG[2],
			{ "ulProtocolId", "octvc1.vlan.tag.ulprotocolid",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_VLAN_PROTOCOL_ID_ENUM), 0x0,
			"ulProtocolId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_VLAN_TAG.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_VLAN_HEADER_INFO[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_VLAN_HEADER_INFO;

void  register_tOCTVC1_VLAN_HEADER_INFO(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_VLAN_HEADER_INFO[0],
			{ "ulNumVlanTag", "octvc1.vlan.header_info.ulnumvlantag",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulNumVlanTag",HFILL }

		},
		{
			&ahf_tOCTVC1_VLAN_HEADER_INFO[1],
			{ "aVlanTag", "octvc1.vlan.header_info.avlantag",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"aVlanTag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_VLAN_HEADER_INFO.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_VLAN_TAG(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_VLAN_TAG)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_VLAN_TAG (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_VLAN_TAG));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_VLAN_TAG);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_VLAN_TAG[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_VLAN_TAG, ulPriority), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_VLAN_TAG[0], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > 7 ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..7)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_VLAN_TAG, ulPriority);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_VLAN_TAG[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_VLAN_TAG, ulVlanId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_VLAN_TAG[1], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > 4095 ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..4095)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_VLAN_TAG, ulVlanId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_VLAN_TAG[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_VLAN_TAG, ulProtocolId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_VLAN_TAG[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_VLAN_TAG, ulProtocolId);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_VLAN_HEADER_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_VLAN_HEADER_INFO)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_VLAN_HEADER_INFO (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_VLAN_HEADER_INFO));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_VLAN_HEADER_INFO);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_VLAN_HEADER_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_VLAN_HEADER_INFO, ulNumVlanTag), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_VLAN_HEADER_INFO[0], tvb, offset,
			4, temp_data);
		if( ( (unsigned int)temp_data < 0) || ( (unsigned int)temp_data > cOCTVC1_VLAN_MAX_TAG ) )
		{
			proto_item_set_expert_flags(ti, PI_MALFORMED, PI_ERROR);
			proto_item_append_text(ti," [Out of range - (0..cOCTVC1_VLAN_MAX_TAG)]" );
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_VLAN_HEADER_INFO, ulNumVlanTag);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_VLAN_HEADER_INFO, aVlanTag), "aVlanTag:tOCTVC1_VLAN_TAG");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<4; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_VLAN_TAG), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_VLAN_TAG( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
			}
		}
	}


	return offset;

};

#include <octvc1_mac.h>

#include <octvc1_list.h>

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_LIST_HANDLE_OBJECT_GET;

void  register_tOCTVC1_LIST_HANDLE_OBJECT_GET(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET[0],
			{ "ulNumHandleObject", "octvc1.list.handle_object_get.ulnumhandleobject",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulNumHandleObject",HFILL }

		},
		{
			&ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET[1],
			{ "aHandleObject", "octvc1.list.handle_object_get.ahandleobject",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"aHandleObject",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_LIST_HANDLE_OBJECT_GET.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_LIST_NAME_OBJECT32_GET[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_LIST_NAME_OBJECT32_GET;

void  register_tOCTVC1_LIST_NAME_OBJECT32_GET(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_LIST_NAME_OBJECT32_GET[0],
			{ "ulNumObject", "octvc1.list.name_object32_get.ulnumobject",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulNumObject",HFILL }

		},
		{
			&ahf_tOCTVC1_LIST_NAME_OBJECT32_GET[1],
			{ "aNameObject32", "octvc1.list.name_object32_get.anameobject32",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"aNameObject32",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_LIST_NAME_OBJECT32_GET.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME;

void  register_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME[0],
			{ "hParent", "octvc1.list.handle_object_get_sub_object_id_name.hparent",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hParent",HFILL }

		},
		{
			&ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME[1],
			{ "ulNumObject", "octvc1.list.handle_object_get_sub_object_id_name.ulnumobject",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulNumObject",HFILL }

		},
		{
			&ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME[2],
			{ "aNameObject32", "octvc1.list.handle_object_get_sub_object_id_name.anameobject32",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"aNameObject32",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID;

void  register_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID[0],
			{ "hParent", "octvc1.list.handle_object_get_sub_object_id.hparent",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hParent",HFILL }

		},
		{
			&ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID[1],
			{ "ulNumSubObjectId", "octvc1.list.handle_object_get_sub_object_id.ulnumsubobjectid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulNumSubObjectId",HFILL }

		},
		{
			&ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID[2],
			{ "aSubObjectId", "octvc1.list.handle_object_get_sub_object_id.asubobjectid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"aSubObjectId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_LIST_INDEX_GET[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_LIST_INDEX_GET;

void  register_tOCTVC1_LIST_INDEX_GET(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_LIST_INDEX_GET[0],
			{ "ulNumIndex", "octvc1.list.index_get.ulnumindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulNumIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_LIST_INDEX_GET[1],
			{ "aIndex", "octvc1.list.index_get.aindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"aIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_LIST_INDEX_GET.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_LIST_INDEX_GET_SUB_INDEX[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_LIST_INDEX_GET_SUB_INDEX;

void  register_tOCTVC1_LIST_INDEX_GET_SUB_INDEX(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_LIST_INDEX_GET_SUB_INDEX[0],
			{ "ulParentIndex", "octvc1.list.index_get_sub_index.ulparentindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulParentIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_LIST_INDEX_GET_SUB_INDEX[1],
			{ "ulNumIndex", "octvc1.list.index_get_sub_index.ulnumindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulNumIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_LIST_INDEX_GET_SUB_INDEX[2],
			{ "aIndex", "octvc1.list.index_get_sub_index.aindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"aIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_LIST_INDEX_GET_SUB_INDEX.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_LIST_HANDLE_OBJECT_GET(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_LIST_HANDLE_OBJECT_GET)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_LIST_HANDLE_OBJECT_GET (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_LIST_HANDLE_OBJECT_GET));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_LIST_HANDLE_OBJECT_GET);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET, ulNumHandleObject), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET, ulNumHandleObject);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET, aHandleObject), "aHandleObject");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<100; i++ )
			{
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET, aHandleObject), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET[1], tvb, offset,
			4, temp_data, "[%d]: 0x%08x", i, temp_data );
		}
				offset+=4;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_LIST_NAME_OBJECT32_GET(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_LIST_NAME_OBJECT32_GET)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_LIST_NAME_OBJECT32_GET (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_LIST_NAME_OBJECT32_GET));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_LIST_NAME_OBJECT32_GET);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LIST_NAME_OBJECT32_GET[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_NAME_OBJECT32_GET, ulNumObject), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LIST_NAME_OBJECT32_GET[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LIST_NAME_OBJECT32_GET, ulNumObject);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_NAME_OBJECT32_GET, aNameObject32), "aNameObject32:tOCTVC1_OBJECT32_NAME");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<25; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_OBJECT32_NAME), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_OBJECT32_NAME( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME, hParent), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME, hParent);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME, ulNumObject), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME, ulNumObject);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME, aNameObject32), "aNameObject32:tOCTVC1_OBJECT32_NAME");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<25; i++ )
			{
		{
			proto_tree*	sub_tree2;
			ti = proto_tree_add_text(sub_tree, tvb, offset,
				sizeof(tOCTVC1_OBJECT32_NAME), "[%d]:", i);
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_OBJECT32_NAME( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID, hParent), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID, hParent);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID, ulNumSubObjectId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID, ulNumSubObjectId);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID, aSubObjectId), "aSubObjectId");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<100; i++ )
			{
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID, aSubObjectId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID[2], tvb, offset,
			4, temp_data, "[%d]: 0x%08x", i, temp_data );
		}
				offset+=4;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_LIST_INDEX_GET(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_LIST_INDEX_GET)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_LIST_INDEX_GET (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_LIST_INDEX_GET));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_LIST_INDEX_GET);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LIST_INDEX_GET[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_INDEX_GET, ulNumIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LIST_INDEX_GET[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LIST_INDEX_GET, ulNumIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_INDEX_GET, aIndex), "aIndex");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<100; i++ )
			{
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_LIST_INDEX_GET[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_INDEX_GET, aIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_LIST_INDEX_GET[1], tvb, offset,
			4, temp_data, "[%d]: 0x%08x", i, temp_data );
		}
				offset+=4;
			}
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_LIST_INDEX_GET_SUB_INDEX(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_LIST_INDEX_GET_SUB_INDEX)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_LIST_INDEX_GET_SUB_INDEX (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_LIST_INDEX_GET_SUB_INDEX));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_LIST_INDEX_GET_SUB_INDEX);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LIST_INDEX_GET_SUB_INDEX[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_INDEX_GET_SUB_INDEX, ulParentIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LIST_INDEX_GET_SUB_INDEX[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LIST_INDEX_GET_SUB_INDEX, ulParentIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LIST_INDEX_GET_SUB_INDEX[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_INDEX_GET_SUB_INDEX, ulNumIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LIST_INDEX_GET_SUB_INDEX[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LIST_INDEX_GET_SUB_INDEX, ulNumIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_INDEX_GET_SUB_INDEX, aIndex), "aIndex");
		{
			int i, ofs=offset;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i<100; i++ )
			{
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(sub_tree, ahf_tOCTVC1_LIST_INDEX_GET_SUB_INDEX[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LIST_INDEX_GET_SUB_INDEX, aIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint_format(sub_tree, ahf_tOCTVC1_LIST_INDEX_GET_SUB_INDEX[2], tvb, offset,
			4, temp_data, "[%d]: 0x%08x", i, temp_data );
		}
				offset+=4;
			}
		}
	}


	return offset;

};

#include <octvc1_msg.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_MSG_FLAGS_MASK[] = 
 {
	{ cOCTVC1_MSG_FLAGS_MASK_RETRANSMIT, "cOCTVC1_MSG_FLAGS_MASK_RETRANSMIT" },
	{ cOCTVC1_MSG_FLAGS_MASK_NO_RESPONSE, "cOCTVC1_MSG_FLAGS_MASK_NO_RESPONSE" },
	{ cOCTVC1_MSG_FLAGS_MASK_LONG_PROCESSING, "cOCTVC1_MSG_FLAGS_MASK_LONG_PROCESSING" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_MSG_HEADER[6];
tWS_ENTITY_HANDLE ws_htOCTVC1_MSG_HEADER;

void  register_tOCTVC1_MSG_HEADER(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MSG_HEADER[0],
			{ "ulLength", "octvc1.msg.header.ullength",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulLength",HFILL }

		},
		{
			&ahf_tOCTVC1_MSG_HEADER[1],
			{ "ulTransactionId", "octvc1.msg.header.ultransactionid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTransactionId",HFILL }

		},
		{
			&ahf_tOCTVC1_MSG_HEADER[2],
			{ "ul_Type_R_CmdId", "octvc1.msg.header.ul_type_r_cmdid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ul_Type_R_CmdId",HFILL }

		},
		{
			&ahf_tOCTVC1_MSG_HEADER[3],
			{ "ulSessionId", "octvc1.msg.header.ulsessionid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulSessionId",HFILL }

		},
		{
			&ahf_tOCTVC1_MSG_HEADER[4],
			{ "ulReturnCode", "octvc1.msg.header.ulreturncode",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulReturnCode",HFILL }

		},
		{
			&ahf_tOCTVC1_MSG_HEADER[5],
			{ "ulUserInfo", "octvc1.msg.header.uluserinfo",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulUserInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MSG_HEADER.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_EVENT_HEADER[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_EVENT_HEADER;

void  register_tOCTVC1_EVENT_HEADER(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_EVENT_HEADER[0],
			{ "ulLength", "octvc1.msg.toctvc1_event_header.ullength",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulLength",HFILL }

		},
		{
			&ahf_tOCTVC1_EVENT_HEADER[1],
			{ "ulEventId", "octvc1.msg.toctvc1_event_header.uleventid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulEventId",HFILL }

		},
		{
			&ahf_tOCTVC1_EVENT_HEADER[2],
			{ "ulUserEventId", "octvc1.msg.toctvc1_event_header.ulusereventid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulUserEventId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_EVENT_HEADER.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_MSG_HEADER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MSG_HEADER (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_MSG_HEADER);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MSG_HEADER[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ulLength), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MSG_HEADER[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ulLength);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MSG_HEADER[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ulTransactionId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MSG_HEADER[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ulTransactionId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MSG_HEADER[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ul_Type_R_CmdId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MSG_HEADER[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ul_Type_R_CmdId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MSG_HEADER[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ulSessionId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MSG_HEADER[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ulSessionId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MSG_HEADER[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ulReturnCode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MSG_HEADER[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ulReturnCode);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MSG_HEADER[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ulUserInfo), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MSG_HEADER[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MSG_HEADER, ulUserInfo);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_EVENT_HEADER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_EVENT_HEADER (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_EVENT_HEADER);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_EVENT_HEADER[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_EVENT_HEADER, ulLength), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_EVENT_HEADER[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_EVENT_HEADER, ulLength);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_EVENT_HEADER[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_EVENT_HEADER, ulEventId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_EVENT_HEADER[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_EVENT_HEADER, ulEventId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_EVENT_HEADER[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_EVENT_HEADER, ulUserEventId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_EVENT_HEADER[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_EVENT_HEADER, ulUserEventId);

	}


	return offset;

};

#include <octvc1_log.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_LOG_TYPE_ENUM[] = 
 {
	{ cOCTVC1_LOG_TYPE_ENUM_LOG, "cOCTVC1_LOG_TYPE_ENUM_LOG" },
	{ cOCTVC1_LOG_TYPE_ENUM_TRACE, "cOCTVC1_LOG_TYPE_ENUM_TRACE" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_LOG_LEVEL_ENUM[] = 
 {
	{ cOCTVC1_LOG_LEVEL_ENUM_EMERGENCY, "cOCTVC1_LOG_LEVEL_ENUM_EMERGENCY" },
	{ cOCTVC1_LOG_LEVEL_ENUM_ALERT, "cOCTVC1_LOG_LEVEL_ENUM_ALERT" },
	{ cOCTVC1_LOG_LEVEL_ENUM_CRITICAL, "cOCTVC1_LOG_LEVEL_ENUM_CRITICAL" },
	{ cOCTVC1_LOG_LEVEL_ENUM_ERROR, "cOCTVC1_LOG_LEVEL_ENUM_ERROR" },
	{ cOCTVC1_LOG_LEVEL_ENUM_WARNING, "cOCTVC1_LOG_LEVEL_ENUM_WARNING" },
	{ cOCTVC1_LOG_LEVEL_ENUM_NOTICE, "cOCTVC1_LOG_LEVEL_ENUM_NOTICE" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_LOG_PAYLOAD_TYPE_ENUM[] = 
 {
	{ cOCTVC1_LOG_PAYLOAD_TYPE_ENUM_DATA, "cOCTVC1_LOG_PAYLOAD_TYPE_ENUM_DATA" },
	{ cOCTVC1_LOG_PAYLOAD_TYPE_ENUM_STRING, "cOCTVC1_LOG_PAYLOAD_TYPE_ENUM_STRING" },
	{ cOCTVC1_LOG_PAYLOAD_TYPE_ENUM_RC, "cOCTVC1_LOG_PAYLOAD_TYPE_ENUM_RC" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_LOG_TRACE_MASK[] = 
 {
	{ cOCTVC1_LOG_TRACE_MASK_SYSTEM, "cOCTVC1_LOG_TRACE_MASK_SYSTEM" },
	{ cOCTVC1_LOG_TRACE_MASK_IPC_MESSAGE, "cOCTVC1_LOG_TRACE_MASK_IPC_MESSAGE" },
	{ cOCTVC1_LOG_TRACE_MASK_USER_0, "cOCTVC1_LOG_TRACE_MASK_USER_0" },
	{ cOCTVC1_LOG_TRACE_MASK_USER_1, "cOCTVC1_LOG_TRACE_MASK_USER_1" },
	{ cOCTVC1_LOG_TRACE_MASK_USER_2, "cOCTVC1_LOG_TRACE_MASK_USER_2" },
	{ cOCTVC1_LOG_TRACE_MASK_USER_3, "cOCTVC1_LOG_TRACE_MASK_USER_3" },
	{ cOCTVC1_LOG_TRACE_MASK_USER_4, "cOCTVC1_LOG_TRACE_MASK_USER_4" },
	{ cOCTVC1_LOG_TRACE_MASK_USER_5, "cOCTVC1_LOG_TRACE_MASK_USER_5" },
	{ cOCTVC1_LOG_TRACE_MASK_NONE, "cOCTVC1_LOG_TRACE_MASK_NONE" },
	{ cOCTVC1_LOG_TRACE_MASK_ALL, "cOCTVC1_LOG_TRACE_MASK_ALL" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_LOG_HEADER[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_LOG_HEADER;

void  register_tOCTVC1_LOG_HEADER(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_LOG_HEADER[0],
			{ "ulId", "octvc1.log.header.ulid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulId",HFILL }

		},
		{
			&ahf_tOCTVC1_LOG_HEADER[1],
			{ "ulTime", "octvc1.log.header.ultime",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTime",HFILL }

		},
		{
			&ahf_tOCTVC1_LOG_HEADER[2],
			{ "hProcess", "octvc1.log.header.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_LOG_HEADER[3],
			{ "ul_Type_Info_Length", "octvc1.log.header.ul_type_info_length",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ul_Type_Info_Length",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_LOG_HEADER.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_LOG_HEADER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_LOG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_LOG_HEADER (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_LOG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_LOG_HEADER);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LOG_HEADER[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LOG_HEADER, ulId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LOG_HEADER[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LOG_HEADER, ulId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LOG_HEADER[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LOG_HEADER, ulTime), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LOG_HEADER[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LOG_HEADER, ulTime);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LOG_HEADER[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LOG_HEADER, hProcess), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LOG_HEADER[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LOG_HEADER, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_LOG_HEADER[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_LOG_HEADER, ul_Type_Info_Length), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_LOG_HEADER[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_LOG_HEADER, ul_Type_Info_Length);

	}


	return offset;

};

#include <octvc1_file.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_FILE_TYPE_ENUM[] = 
 {
	{ cOCTVC1_FILE_TYPE_ENUM_NONE, "cOCTVC1_FILE_TYPE_ENUM_NONE" },
	{ cOCTVC1_FILE_TYPE_ENUM_TEXT, "cOCTVC1_FILE_TYPE_ENUM_TEXT" },
	{ cOCTVC1_FILE_TYPE_ENUM_BIN, "cOCTVC1_FILE_TYPE_ENUM_BIN" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_FILE_FORMAT_ENUM[] = 
 {
	{ cOCTVC1_FILE_FORMAT_ENUM_NONE, "cOCTVC1_FILE_FORMAT_ENUM_NONE" },
	{ cOCTVC1_FILE_FORMAT_ENUM_LOG, "cOCTVC1_FILE_FORMAT_ENUM_LOG" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_FILE_HEADER[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_FILE_HEADER;

void  register_tOCTVC1_FILE_HEADER(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_FILE_HEADER[0],
			{ "ulMagic", "octvc1.file.header.ulmagic",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulMagic",HFILL }

		},
		{
			&ahf_tOCTVC1_FILE_HEADER[1],
			{ "ul_Type_Ver", "octvc1.file.header.ul_type_ver",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ul_Type_Ver",HFILL }

		},
		{
			&ahf_tOCTVC1_FILE_HEADER[2],
			{ "ulTimeStamp", "octvc1.file.header.ultimestamp",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTimeStamp",HFILL }

		},
		{
			&ahf_tOCTVC1_FILE_HEADER[3],
			{ "ul_Align_HdrSize", "octvc1.file.header.ul_align_hdrsize",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ul_Align_HdrSize",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_FILE_HEADER.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_FILE_HEADER(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_FILE_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_FILE_HEADER (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_FILE_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_FILE_HEADER);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_FILE_ENDIAN_MAGIC_WORD)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_FILE_HEADER[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_FILE_HEADER, ulMagic),temp_data, "cOCTVC1_FILE_ENDIAN_MAGIC_WORD (0x%08x)",temp_data);
		}else {
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_FILE_HEADER[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_FILE_HEADER, ulMagic), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_FILE_HEADER[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_FILE_HEADER, ulMagic);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_FILE_HEADER[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_FILE_HEADER, ul_Type_Ver), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_FILE_HEADER[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_FILE_HEADER, ul_Type_Ver);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_FILE_HEADER[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_FILE_HEADER, ulTimeStamp), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_FILE_HEADER[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_FILE_HEADER, ulTimeStamp);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_FILE_HEADER[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_FILE_HEADER, ul_Align_HdrSize), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_FILE_HEADER[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_FILE_HEADER, ul_Align_HdrSize);

	}


	return offset;

};

#include <octvc1_module.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_MODULE_ID_ENUM[] = 
 {
	{ cOCTVC1_MODULE_ID_ENUM_GENERIC, "cOCTVC1_MODULE_ID_ENUM_GENERIC" },
	{ cOCTVC1_MODULE_ID_ENUM_MAIN, "cOCTVC1_MODULE_ID_ENUM_MAIN" },
	{ cOCTVC1_MODULE_ID_ENUM_PKT_API, "cOCTVC1_MODULE_ID_ENUM_PKT_API" },
	{ cOCTVC1_MODULE_ID_ENUM_CTRL, "cOCTVC1_MODULE_ID_ENUM_CTRL" },
	{ cOCTVC1_MODULE_ID_ENUM_LICENSING, "cOCTVC1_MODULE_ID_ENUM_LICENSING" },
	{ cOCTVC1_MODULE_ID_ENUM_TEST, "cOCTVC1_MODULE_ID_ENUM_TEST" },
	{ cOCTVC1_MODULE_ID_ENUM_HW, "cOCTVC1_MODULE_ID_ENUM_HW" },
	{ cOCTVC1_MODULE_ID_ENUM_IRSC, "cOCTVC1_MODULE_ID_ENUM_IRSC" },
	{ cOCTVC1_MODULE_ID_ENUM_OBM, "cOCTVC1_MODULE_ID_ENUM_OBM" },
	{ cOCTVC1_MODULE_ID_ENUM_SDR, "cOCTVC1_MODULE_ID_ENUM_SDR" },
	{ cOCTVC1_MODULE_ID_ENUM_GSM, "cOCTVC1_MODULE_ID_ENUM_GSM" },
	{ cOCTVC1_MODULE_ID_ENUM_LTE, "cOCTVC1_MODULE_ID_ENUM_LTE" },
	{ cOCTVC1_MODULE_ID_ENUM_UMTS, "cOCTVC1_MODULE_ID_ENUM_UMTS" },
	{ cOCTVC1_MODULE_ID_ENUM_RUS, "cOCTVC1_MODULE_ID_ENUM_RUS" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_MODULE_DATA[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_MODULE_DATA;

void  register_tOCTVC1_MODULE_DATA(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_MODULE_DATA[0],
			{ "ulModuleId", "octvc1.module.data.ulmoduleid",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_MODULE_ID_ENUM), 0x0,
			"ulModuleId",HFILL }

		},
		{
			&ahf_tOCTVC1_MODULE_DATA[1],
			{ "ulModuleDataId", "octvc1.module.data.ulmoduledataid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulModuleDataId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_MODULE_DATA.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_MODULE_DATA(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_MODULE_DATA)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_MODULE_DATA (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_MODULE_DATA));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_MODULE_DATA);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MODULE_DATA[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MODULE_DATA, ulModuleId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MODULE_DATA[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MODULE_DATA, ulModuleId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_MODULE_DATA[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_MODULE_DATA, ulModuleDataId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_MODULE_DATA[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_MODULE_DATA, ulModuleDataId);

	}


	return offset;

};

#include <octvc1_tap.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_TAP_DIRECTION_ENUM[] = 
 {
	{ cOCTVC1_TAP_DIRECTION_ENUM_TX, "cOCTVC1_TAP_DIRECTION_ENUM_TX" },
	{ cOCTVC1_TAP_DIRECTION_ENUM_RX, "cOCTVC1_TAP_DIRECTION_ENUM_RX" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_TAP_ID[] = 
 {
	{ cOCTVC1_TAP_ID_MASK_MODULE_ID_BIT, "cOCTVC1_TAP_ID_MASK_MODULE_ID_BIT" },
	{ cOCTVC1_TAP_ID_MASK_MODULE_ID_BIT_OFFSET, "cOCTVC1_TAP_ID_MASK_MODULE_ID_BIT_OFFSET" },
	{ cOCTVC1_TAP_ID_MASK_MODULE_ID, "cOCTVC1_TAP_ID_MASK_MODULE_ID" },
	{ cOCTVC1_TAP_ID_MASK_DIRECTION_BIT, "cOCTVC1_TAP_ID_MASK_DIRECTION_BIT" },
	{ cOCTVC1_TAP_ID_MASK_DIRECTION_BIT_OFFSET, "cOCTVC1_TAP_ID_MASK_DIRECTION_BIT_OFFSET" },
	{ cOCTVC1_TAP_ID_MASK_DIRECTION, "cOCTVC1_TAP_ID_MASK_DIRECTION" },
	{ cOCTVC1_TAP_ID_MASK_INDEX_BIT, "cOCTVC1_TAP_ID_MASK_INDEX_BIT" },
	{ cOCTVC1_TAP_ID_MASK_INDEX_BIT_OFFSET, "cOCTVC1_TAP_ID_MASK_INDEX_BIT_OFFSET" },
	{ cOCTVC1_TAP_ID_MASK_INDEX, "cOCTVC1_TAP_ID_MASK_INDEX" },
	{ 0,		NULL }
 };

#include <octvc1_radio.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_RADIO_STANDARD_ENUM[] = 
 {
	{ cOCTVC1_RADIO_STANDARD_ENUM_GSM, "cOCTVC1_RADIO_STANDARD_ENUM_GSM" },
	{ cOCTVC1_RADIO_STANDARD_ENUM_UMTS, "cOCTVC1_RADIO_STANDARD_ENUM_UMTS" },
	{ cOCTVC1_RADIO_STANDARD_ENUM_LTE, "cOCTVC1_RADIO_STANDARD_ENUM_LTE" },
	{ cOCTVC1_RADIO_STANDARD_ENUM_INVALID, "cOCTVC1_RADIO_STANDARD_ENUM_INVALID" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM[] = 
 {
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_380, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_380" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_410, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_410" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_450, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_450" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_480, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_480" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_710, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_710" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_750, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_750" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_810, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_810" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_850, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_850" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_E_900, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_E_900" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_P_900, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_P_900" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_R_900, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_R_900" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_DCS_1800, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_DCS_1800" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_PCS_1900, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_PCS_1900" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_LAST, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_LAST" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_INVALID, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_GSM_ENUM_INVALID" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM[] = 
 {
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_1, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_1" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_2, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_2" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_3, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_3" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_4, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_4" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_5, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_5" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_6, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_6" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_7, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_7" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_8, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_8" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_9, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_9" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_10, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_10" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_11, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_11" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_12, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_12" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_13, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_13" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_14, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_14" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_15, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_15" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_16, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_16" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_17, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_17" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_18, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_18" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_19, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_19" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_20, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_20" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_21, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_21" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_22, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_22" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_23, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_23" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_24, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_24" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_25, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_25" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_26, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_26" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_2, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_2" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_4, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_4" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_5, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_5" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_6, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_6" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_7, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_7" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_10, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_10" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_12, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_12" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_13, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_13" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_14, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_14" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_19, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_19" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_25, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_25" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_26, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_ADD_26" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_LAST, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_LAST" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_INVALID, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_UMTS_ENUM_INVALID" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM[] = 
 {
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_1, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_1" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_2, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_2" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_3, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_3" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_4, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_4" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_5, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_5" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_6, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_6" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_7, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_7" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_8, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_8" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_9, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_9" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_10, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_10" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_11, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_11" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_12, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_12" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_13, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_13" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_14, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_14" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_15, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_15" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_16, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_16" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_17, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_17" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_18, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_18" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_19, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_19" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_20, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_20" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_21, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_21" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_22, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_22" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_23, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_23" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_24, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_24" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_25, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_25" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_26, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_26" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_27, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_27" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_28, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_28" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_29, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_29" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_30, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_30" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_31, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_31" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_32, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_32" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_33, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_33" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_34, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_34" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_35, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_35" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_36, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_36" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_37, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_37" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_38, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_38" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_39, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_39" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_40, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_40" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_41, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_41" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_42, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_42" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_43, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_43" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_44, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_44" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_LAST, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_LAST" },
	{ cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_INVALID, "cOCTVC1_RADIO_STANDARD_FREQ_BAND_LTE_ENUM_INVALID" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_RADIO_CONFIG_SCRIPT_TYPE_ENUM[] = 
 {
	{ cOCTVC1_RADIO_CONFIG_SCRIPT_TYPE_ENUM_INVALID, "cOCTVC1_RADIO_CONFIG_SCRIPT_TYPE_ENUM_INVALID" },
	{ cOCTVC1_RADIO_CONFIG_SCRIPT_TYPE_ENUM_TEXT, "cOCTVC1_RADIO_CONFIG_SCRIPT_TYPE_ENUM_TEXT" },
	{ cOCTVC1_RADIO_CONFIG_SCRIPT_TYPE_ENUM_BIN, "cOCTVC1_RADIO_CONFIG_SCRIPT_TYPE_ENUM_BIN" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_RADIO_ID_DIRECTION_ENUM[] = 
 {
	{ cOCTVC1_RADIO_ID_DIRECTION_ENUM_INVALID, "cOCTVC1_RADIO_ID_DIRECTION_ENUM_INVALID" },
	{ cOCTVC1_RADIO_ID_DIRECTION_ENUM_DOWNLINK, "cOCTVC1_RADIO_ID_DIRECTION_ENUM_DOWNLINK" },
	{ cOCTVC1_RADIO_ID_DIRECTION_ENUM_UPLINK, "cOCTVC1_RADIO_ID_DIRECTION_ENUM_UPLINK" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM[] = 
 {
	{ cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_1P4MHZ, "cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_1P4MHZ" },
	{ cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_3MHZ, "cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_3MHZ" },
	{ cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_5MHZ, "cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_5MHZ" },
	{ cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_10MHZ, "cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_10MHZ" },
	{ cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_15MHZ, "cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_15MHZ" },
	{ cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_20MHZ, "cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_20MHZ" },
	{ cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_LAST, "cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_LAST" },
	{ cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_INVALID, "cOCTVC1_RADIO_STANDARD_BANDWIDTH_ENUM_INVALID" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_RADIO_RX_GAIN_CTRL_MODE_ENUM[] = 
 {
	{ cOCTVC1_RADIO_RX_GAIN_CTRL_MODE_ENUM_MGC, "cOCTVC1_RADIO_RX_GAIN_CTRL_MODE_ENUM_MGC" },
	{ cOCTVC1_RADIO_RX_GAIN_CTRL_MODE_ENUM_AGC_FAST_ATK, "cOCTVC1_RADIO_RX_GAIN_CTRL_MODE_ENUM_AGC_FAST_ATK" },
	{ cOCTVC1_RADIO_RX_GAIN_CTRL_MODE_ENUM_AGC_SLOW_ATK, "cOCTVC1_RADIO_RX_GAIN_CTRL_MODE_ENUM_AGC_SLOW_ATK" },
	{ 0,		NULL }
 };

#include <octvc1_api.h>

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_API_SESSION_INFO[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_API_SESSION_INFO;

void  register_tOCTVC1_API_SESSION_INFO(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_API_SESSION_INFO[0],
			{ "ulActiveFlag", "octvc1.api.session_info.ulactiveflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulActiveFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_API_SESSION_INFO[1],
			{ "ulSessionId", "octvc1.api.session_info.ulsessionid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulSessionId",HFILL }

		},
		{
			&ahf_tOCTVC1_API_SESSION_INFO[2],
			{ "ulTransportSessionIndex", "octvc1.api.session_info.ultransportsessionindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTransportSessionIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_API_SESSION_INFO[3],
			{ "ulTransportSessionId", "octvc1.api.session_info.ultransportsessionid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulTransportSessionId",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_API_SESSION_INFO.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_API_SESSION_EVT_INFO[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_API_SESSION_EVT_INFO;

void  register_tOCTVC1_API_SESSION_EVT_INFO(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_API_SESSION_EVT_INFO[0],
			{ "ulEvtActiveFlag", "octvc1.api.session_evt_info.ulevtactiveflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulEvtActiveFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_API_SESSION_EVT_INFO[1],
			{ "lEvtEnablerCnt", "octvc1.api.session_evt_info.levtenablercnt",
			FT_INT32,BASE_DEC, NULL, 0x0,
			"lEvtEnablerCnt",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_API_SESSION_EVT_INFO.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_API_SESSION_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_API_SESSION_INFO)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_API_SESSION_INFO (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_API_SESSION_INFO));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_API_SESSION_INFO);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_API_SESSION_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_API_SESSION_INFO, ulActiveFlag), temp_data, "%s (0x%x)", pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_API_SESSION_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_API_SESSION_INFO, ulActiveFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_API_SESSION_INFO, ulActiveFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_API_SESSION_INFO[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_API_SESSION_INFO, ulSessionId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_API_SESSION_INFO[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_API_SESSION_INFO, ulSessionId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_API_SESSION_INFO[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_API_SESSION_INFO, ulTransportSessionIndex), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_API_SESSION_INFO[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_API_SESSION_INFO, ulTransportSessionIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_API_SESSION_INFO[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_API_SESSION_INFO, ulTransportSessionId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_API_SESSION_INFO[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_API_SESSION_INFO, ulTransportSessionId);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_API_SESSION_EVT_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_API_SESSION_EVT_INFO)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_API_SESSION_EVT_INFO (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_API_SESSION_EVT_INFO));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_API_SESSION_EVT_INFO);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_API_SESSION_EVT_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_API_SESSION_EVT_INFO, ulEvtActiveFlag), temp_data, "%s (0x%x)", pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_API_SESSION_EVT_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_API_SESSION_EVT_INFO, ulEvtActiveFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_API_SESSION_EVT_INFO, ulEvtActiveFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_int_format_value(field_tree, ahf_tOCTVC1_API_SESSION_EVT_INFO[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_API_SESSION_EVT_INFO, lEvtEnablerCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_API_SESSION_EVT_INFO[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_API_SESSION_EVT_INFO, lEvtEnablerCnt);

	}


	return offset;

};

#include <octvc1_process.h>

/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_PROCESS_TYPE_ENUM[] = 
 {
	{ cOCTVC1_PROCESS_TYPE_ENUM_INVALID, "cOCTVC1_PROCESS_TYPE_ENUM_INVALID" },
	{ cOCTVC1_PROCESS_TYPE_ENUM_CONTROL, "cOCTVC1_PROCESS_TYPE_ENUM_CONTROL" },
	{ cOCTVC1_PROCESS_TYPE_ENUM_OCTADF_ROUTER, "cOCTVC1_PROCESS_TYPE_ENUM_OCTADF_ROUTER" },
	{ cOCTVC1_PROCESS_TYPE_ENUM_GSM_SCHED, "cOCTVC1_PROCESS_TYPE_ENUM_GSM_SCHED" },
	{ cOCTVC1_PROCESS_TYPE_ENUM_GSM_DL, "cOCTVC1_PROCESS_TYPE_ENUM_GSM_DL" },
	{ cOCTVC1_PROCESS_TYPE_ENUM_GSM_ULIM, "cOCTVC1_PROCESS_TYPE_ENUM_GSM_ULIM" },
	{ cOCTVC1_PROCESS_TYPE_ENUM_GSM_ULOM, "cOCTVC1_PROCESS_TYPE_ENUM_GSM_ULOM" },
	{ cOCTVC1_PROCESS_TYPE_ENUM_GSM_DECOMB, "cOCTVC1_PROCESS_TYPE_ENUM_GSM_DECOMB" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_PROCESS_STATE_ENUM[] = 
 {
	{ cOCTVC1_PROCESS_STATE_ENUM_INVALID, "cOCTVC1_PROCESS_STATE_ENUM_INVALID" },
	{ cOCTVC1_PROCESS_STATE_ENUM_RESET, "cOCTVC1_PROCESS_STATE_ENUM_RESET" },
	{ cOCTVC1_PROCESS_STATE_ENUM_CREATE, "cOCTVC1_PROCESS_STATE_ENUM_CREATE" },
	{ cOCTVC1_PROCESS_STATE_ENUM_WAITING, "cOCTVC1_PROCESS_STATE_ENUM_WAITING" },
	{ cOCTVC1_PROCESS_STATE_ENUM_READY, "cOCTVC1_PROCESS_STATE_ENUM_READY" },
	{ cOCTVC1_PROCESS_STATE_ENUM_RUNNING, "cOCTVC1_PROCESS_STATE_ENUM_RUNNING" },
	{ cOCTVC1_PROCESS_STATE_ENUM_ERROR, "cOCTVC1_PROCESS_STATE_ENUM_ERROR" },
	{ cOCTVC1_PROCESS_STATE_ENUM_HALT, "cOCTVC1_PROCESS_STATE_ENUM_HALT" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_PROCESS_TASK_STATE_ENUM[] = 
 {
	{ cOCTVC1_PROCESS_TASK_STATE_ENUM_INVALID, "cOCTVC1_PROCESS_TASK_STATE_ENUM_INVALID" },
	{ cOCTVC1_PROCESS_TASK_STATE_ENUM_ERROR, "cOCTVC1_PROCESS_TASK_STATE_ENUM_ERROR" },
	{ cOCTVC1_PROCESS_TASK_STATE_ENUM_IDLE, "cOCTVC1_PROCESS_TASK_STATE_ENUM_IDLE" },
	{ cOCTVC1_PROCESS_TASK_STATE_ENUM_PENDING, "cOCTVC1_PROCESS_TASK_STATE_ENUM_PENDING" },
	{ cOCTVC1_PROCESS_TASK_STATE_ENUM_RUNNING, "cOCTVC1_PROCESS_TASK_STATE_ENUM_RUNNING" },
	{ cOCTVC1_PROCESS_TASK_STATE_ENUM_WAIT_TIMER, "cOCTVC1_PROCESS_TASK_STATE_ENUM_WAIT_TIMER" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_PROCESS_ERROR[6];
tWS_ENTITY_HANDLE ws_htOCTVC1_PROCESS_ERROR;

void  register_tOCTVC1_PROCESS_ERROR(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_PROCESS_ERROR[0],
			{ "ulInputOverflowCnt", "octvc1.process.error.ulinputoverflowcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulInputOverflowCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_ERROR[1],
			{ "ulInputUnderflowCnt", "octvc1.process.error.ulinputunderflowcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulInputUnderflowCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_ERROR[2],
			{ "ulOutputOverflowCnt", "octvc1.process.error.uloutputoverflowcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulOutputOverflowCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_ERROR[3],
			{ "ulOutputUnderflowCnt", "octvc1.process.error.uloutputunderflowcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulOutputUnderflowCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_ERROR[4],
			{ "ulIpcMsgMissCnt", "octvc1.process.error.ulipcmsgmisscnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulIpcMsgMissCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_ERROR[5],
			{ "ulLastErrorRc", "octvc1.process.error.ullasterrorrc",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulLastErrorRc",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_PROCESS_ERROR.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_PROCESS_TASK_STATS[10];
tWS_ENTITY_HANDLE ws_htOCTVC1_PROCESS_TASK_STATS;

void  register_tOCTVC1_PROCESS_TASK_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_PROCESS_TASK_STATS[0],
			{ "ulUserData", "octvc1.process.task_stats.uluserdata",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulUserData",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_TASK_STATS[1],
			{ "ulMaxExecTimeUs", "octvc1.process.task_stats.ulmaxexectimeus",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMaxExecTimeUs",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_TASK_STATS[2],
			{ "ulAvgExecTimeUs", "octvc1.process.task_stats.ulavgexectimeus",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulAvgExecTimeUs",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_TASK_STATS[3],
			{ "ulLastExecTimeUs", "octvc1.process.task_stats.ullastexectimeus",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulLastExecTimeUs",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_TASK_STATS[4],
			{ "ulMaxExecCacheMissCnt", "octvc1.process.task_stats.ulmaxexeccachemisscnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulMaxExecCacheMissCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_TASK_STATS[5],
			{ "ulLastExecCacheMissCnt", "octvc1.process.task_stats.ullastexeccachemisscnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulLastExecCacheMissCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_TASK_STATS[6],
			{ "ulExecCnt", "octvc1.process.task_stats.ulexeccnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulExecCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_TASK_STATS[7],
			{ "ulDoneCnt", "octvc1.process.task_stats.uldonecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulDoneCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_TASK_STATS[8],
			{ "ulErrorCnt", "octvc1.process.task_stats.ulerrorcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulErrorCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_TASK_STATS[9],
			{ "State", "octvc1.process.task_stats.state",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_PROCESS_TASK_STATE_ENUM), 0x0,
			"State",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_PROCESS_TASK_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_PROCESS_CPU_USAGE_STATS[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_PROCESS_CPU_USAGE_STATS;

void  register_tOCTVC1_PROCESS_CPU_USAGE_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_PROCESS_CPU_USAGE_STATS[0],
			{ "ulProcessCpuUsagePercent", "octvc1.process.cpu_usage_stats.ulprocesscpuusagepercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulProcessCpuUsagePercent",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_CPU_USAGE_STATS[1],
			{ "ulProcessCpuUsageMinPercent", "octvc1.process.cpu_usage_stats.ulprocesscpuusageminpercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulProcessCpuUsageMinPercent",HFILL }

		},
		{
			&ahf_tOCTVC1_PROCESS_CPU_USAGE_STATS[2],
			{ "ulProcessCpuUsageMaxPercent", "octvc1.process.cpu_usage_stats.ulprocesscpuusagemaxpercent",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulProcessCpuUsageMaxPercent",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_PROCESS_CPU_USAGE_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_PROCESS_ERROR(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_PROCESS_ERROR)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_PROCESS_ERROR (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_PROCESS_ERROR));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_PROCESS_ERROR);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_ERROR[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulInputOverflowCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_ERROR[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulInputOverflowCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_ERROR[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulInputUnderflowCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_ERROR[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulInputUnderflowCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_ERROR[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulOutputOverflowCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_ERROR[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulOutputOverflowCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_ERROR[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulOutputUnderflowCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_ERROR[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulOutputUnderflowCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_ERROR[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulIpcMsgMissCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_ERROR[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulIpcMsgMissCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_ERROR[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulLastErrorRc), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_ERROR[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_ERROR, ulLastErrorRc);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_PROCESS_TASK_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_PROCESS_TASK_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_PROCESS_TASK_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_PROCESS_TASK_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_PROCESS_TASK_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulUserData), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulUserData);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulMaxExecTimeUs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulMaxExecTimeUs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulAvgExecTimeUs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulAvgExecTimeUs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulLastExecTimeUs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulLastExecTimeUs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulMaxExecCacheMissCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulMaxExecCacheMissCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulLastExecCacheMissCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulLastExecCacheMissCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulExecCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[6], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulExecCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[7], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulDoneCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[7], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulDoneCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[8], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulErrorCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[8], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, ulErrorCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[9], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, State), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_TASK_STATS[9], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_TASK_STATS, State);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_PROCESS_CPU_USAGE_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_PROCESS_CPU_USAGE_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_PROCESS_CPU_USAGE_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_PROCESS_CPU_USAGE_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_PROCESS_CPU_USAGE_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_CPU_USAGE_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_CPU_USAGE_STATS, ulProcessCpuUsagePercent), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_CPU_USAGE_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_CPU_USAGE_STATS, ulProcessCpuUsagePercent);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_CPU_USAGE_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_CPU_USAGE_STATS, ulProcessCpuUsageMinPercent), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_CPU_USAGE_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_CPU_USAGE_STATS, ulProcessCpuUsageMinPercent);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_PROCESS_CPU_USAGE_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_PROCESS_CPU_USAGE_STATS, ulProcessCpuUsageMaxPercent), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_PROCESS_CPU_USAGE_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_PROCESS_CPU_USAGE_STATS, ulProcessCpuUsageMaxPercent);

	}


	return offset;

};
/****************************************************************************
	MODULE REGISTERED EXPORTED FUNCTION
 ****************************************************************************/

void ws_register_OCTVC1_common(void)
{
	register_tOCTVC1_CURSOR_HANDLE_OBJECT_GET(); 
	register_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_HANDLE_OBJECT(); 
	register_tOCTVC1_CURSOR_HANDLE_OBJECT_GET_SUB_OBJECT_ID(); 
	register_tOCTVC1_CURSOR_INDEX_GET(); 
	register_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX(); 
	register_tOCTVC1_OBJECT32_NAME(); 
	register_tOCTVC1_VLAN_TAG(); 
	register_tOCTVC1_VLAN_HEADER_INFO(); 
	register_tOCTVC1_LIST_HANDLE_OBJECT_GET(); 
	register_tOCTVC1_LIST_NAME_OBJECT32_GET(); 
	register_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID_NAME(); 
	register_tOCTVC1_LIST_HANDLE_OBJECT_GET_SUB_OBJECT_ID(); 
	register_tOCTVC1_LIST_INDEX_GET(); 
	register_tOCTVC1_LIST_INDEX_GET_SUB_INDEX(); 
	register_tOCTVC1_MSG_HEADER(); 
	register_tOCTVC1_EVENT_HEADER(); 
	register_tOCTVC1_LOG_HEADER(); 
	register_tOCTVC1_FILE_HEADER(); 
	register_tOCTVC1_MODULE_DATA(); 
	register_tOCTVC1_API_SESSION_INFO(); 
	register_tOCTVC1_API_SESSION_EVT_INFO(); 
	register_tOCTVC1_PROCESS_ERROR(); 
	register_tOCTVC1_PROCESS_TASK_STATS(); 
	register_tOCTVC1_PROCESS_CPU_USAGE_STATS(); 

}


