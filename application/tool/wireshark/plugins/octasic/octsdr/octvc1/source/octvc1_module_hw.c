/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: octvc1_module_hw.c

      
Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark module dissector related functions

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/

#include <config.h>
#include <epan/packet.h>
#include "../../../include/oct_ws_macro.h"
#include "../include/module.h"
#include "../include/octvc1_common.h"

#include <hw/octvc1_hw_evt_priv.h>


/****************************************************************************
	MODULE API ENUMERATION STRING VALUES
 ****************************************************************************/

const value_string  vals_tOCTVC1_HW_PCB_INFO_SOURCE_ENUM[] = 
 {
	{ cOCTVC1_HW_PCB_INFO_SOURCE_ENUM_USER_HW_CONFIG, "cOCTVC1_HW_PCB_INFO_SOURCE_ENUM_USER_HW_CONFIG" },
	{ cOCTVC1_HW_PCB_INFO_SOURCE_ENUM_DATA_SECTION, "cOCTVC1_HW_PCB_INFO_SOURCE_ENUM_DATA_SECTION" },
	{ cOCTVC1_HW_PCB_INFO_SOURCE_ENUM_EEPROM, "cOCTVC1_HW_PCB_INFO_SOURCE_ENUM_EEPROM" },
	{ cOCTVC1_HW_PCB_INFO_SOURCE_ENUM_INI_FILE, "cOCTVC1_HW_PCB_INFO_SOURCE_ENUM_INI_FILE" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_PCB_INFO_STATE_ENUM[] = 
 {
	{ cOCTVC1_HW_PCB_INFO_STATE_ENUM_PARSED_ERROR, "cOCTVC1_HW_PCB_INFO_STATE_ENUM_PARSED_ERROR" },
	{ cOCTVC1_HW_PCB_INFO_STATE_ENUM_PARSED_OK, "cOCTVC1_HW_PCB_INFO_STATE_ENUM_PARSED_OK" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_CPU_CORE_MASK[] = 
 {
	{ cOCTVC1_HW_CPU_CORE_MASK_1, "cOCTVC1_HW_CPU_CORE_MASK_1" },
	{ cOCTVC1_HW_CPU_CORE_MASK_2, "cOCTVC1_HW_CPU_CORE_MASK_2" },
	{ cOCTVC1_HW_CPU_CORE_MASK_3, "cOCTVC1_HW_CPU_CORE_MASK_3" },
	{ cOCTVC1_HW_CPU_CORE_MASK_4, "cOCTVC1_HW_CPU_CORE_MASK_4" },
	{ cOCTVC1_HW_CPU_CORE_MASK_5, "cOCTVC1_HW_CPU_CORE_MASK_5" },
	{ cOCTVC1_HW_CPU_CORE_MASK_6, "cOCTVC1_HW_CPU_CORE_MASK_6" },
	{ cOCTVC1_HW_CPU_CORE_MASK_7, "cOCTVC1_HW_CPU_CORE_MASK_7" },
	{ cOCTVC1_HW_CPU_CORE_MASK_8, "cOCTVC1_HW_CPU_CORE_MASK_8" },
	{ cOCTVC1_HW_CPU_CORE_MASK_9, "cOCTVC1_HW_CPU_CORE_MASK_9" },
	{ cOCTVC1_HW_CPU_CORE_MASK_10, "cOCTVC1_HW_CPU_CORE_MASK_10" },
	{ cOCTVC1_HW_CPU_CORE_MASK_11, "cOCTVC1_HW_CPU_CORE_MASK_11" },
	{ cOCTVC1_HW_CPU_CORE_MASK_12, "cOCTVC1_HW_CPU_CORE_MASK_12" },
	{ cOCTVC1_HW_CPU_CORE_MASK_13, "cOCTVC1_HW_CPU_CORE_MASK_13" },
	{ cOCTVC1_HW_CPU_CORE_MASK_14, "cOCTVC1_HW_CPU_CORE_MASK_14" },
	{ cOCTVC1_HW_CPU_CORE_MASK_15, "cOCTVC1_HW_CPU_CORE_MASK_15" },
	{ cOCTVC1_HW_CPU_CORE_MASK_16, "cOCTVC1_HW_CPU_CORE_MASK_16" },
	{ cOCTVC1_HW_CPU_CORE_MASK_17, "cOCTVC1_HW_CPU_CORE_MASK_17" },
	{ cOCTVC1_HW_CPU_CORE_MASK_18, "cOCTVC1_HW_CPU_CORE_MASK_18" },
	{ cOCTVC1_HW_CPU_CORE_MASK_19, "cOCTVC1_HW_CPU_CORE_MASK_19" },
	{ cOCTVC1_HW_CPU_CORE_MASK_20, "cOCTVC1_HW_CPU_CORE_MASK_20" },
	{ cOCTVC1_HW_CPU_CORE_MASK_21, "cOCTVC1_HW_CPU_CORE_MASK_21" },
	{ cOCTVC1_HW_CPU_CORE_MASK_22, "cOCTVC1_HW_CPU_CORE_MASK_22" },
	{ cOCTVC1_HW_CPU_CORE_MASK_23, "cOCTVC1_HW_CPU_CORE_MASK_23" },
	{ cOCTVC1_HW_CPU_CORE_MASK_24, "cOCTVC1_HW_CPU_CORE_MASK_24" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_CPU_CORE_STATUS_ENUM[] = 
 {
	{ cOCTVC1_HW_CPU_CORE_STATUS_ENUM_RESET, "cOCTVC1_HW_CPU_CORE_STATUS_ENUM_RESET" },
	{ cOCTVC1_HW_CPU_CORE_STATUS_ENUM_RUNNING, "cOCTVC1_HW_CPU_CORE_STATUS_ENUM_RUNNING" },
	{ cOCTVC1_HW_CPU_CORE_STATUS_ENUM_HALT, "cOCTVC1_HW_CPU_CORE_STATUS_ENUM_HALT" },
	{ cOCTVC1_HW_CPU_CORE_STATUS_ENUM_TRAP, "cOCTVC1_HW_CPU_CORE_STATUS_ENUM_TRAP" },
	{ cOCTVC1_HW_CPU_CORE_STATUS_ENUM_ACCESS_VIOLATION, "cOCTVC1_HW_CPU_CORE_STATUS_ENUM_ACCESS_VIOLATION" },
	{ cOCTVC1_HW_CPU_CORE_STATUS_ENUM_NOT_PRESENT, "cOCTVC1_HW_CPU_CORE_STATUS_ENUM_NOT_PRESENT" },
	{ cOCTVC1_HW_CPU_CORE_STATUS_ENUM_FAILURE_DETECTED, "cOCTVC1_HW_CPU_CORE_STATUS_ENUM_FAILURE_DETECTED" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_CPU_CORE_FAILURE_MASK[] = 
 {
	{ cOCTVC1_HW_CPU_CORE_FAILURE_MASK_NONE, "cOCTVC1_HW_CPU_CORE_FAILURE_MASK_NONE" },
	{ cOCTVC1_HW_CPU_CORE_FAILURE_MASK_MEMORY, "cOCTVC1_HW_CPU_CORE_FAILURE_MASK_MEMORY" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_ETH_MODE_ENUM[] = 
 {
	{ cOCTVC1_HW_ETH_MODE_ENUM_INVALID, "cOCTVC1_HW_ETH_MODE_ENUM_INVALID" },
	{ cOCTVC1_HW_ETH_MODE_ENUM_MII, "cOCTVC1_HW_ETH_MODE_ENUM_MII" },
	{ cOCTVC1_HW_ETH_MODE_ENUM_RMII, "cOCTVC1_HW_ETH_MODE_ENUM_RMII" },
	{ cOCTVC1_HW_ETH_MODE_ENUM_GMII, "cOCTVC1_HW_ETH_MODE_ENUM_GMII" },
	{ cOCTVC1_HW_ETH_MODE_ENUM_RGMII, "cOCTVC1_HW_ETH_MODE_ENUM_RGMII" },
	{ cOCTVC1_HW_ETH_MODE_ENUM_SGMII, "cOCTVC1_HW_ETH_MODE_ENUM_SGMII" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_ETH_LINK_SPEED_ENUM[] = 
 {
	{ cOCTVC1_HW_ETH_LINK_SPEED_ENUM_INVALID, "cOCTVC1_HW_ETH_LINK_SPEED_ENUM_INVALID" },
	{ cOCTVC1_HW_ETH_LINK_SPEED_ENUM_10, "cOCTVC1_HW_ETH_LINK_SPEED_ENUM_10" },
	{ cOCTVC1_HW_ETH_LINK_SPEED_ENUM_100, "cOCTVC1_HW_ETH_LINK_SPEED_ENUM_100" },
	{ cOCTVC1_HW_ETH_LINK_SPEED_ENUM_1000, "cOCTVC1_HW_ETH_LINK_SPEED_ENUM_1000" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_ETH_DUPLEX_MODE_ENUM[] = 
 {
	{ cOCTVC1_HW_ETH_DUPLEX_MODE_ENUM_INVALID, "cOCTVC1_HW_ETH_DUPLEX_MODE_ENUM_INVALID" },
	{ cOCTVC1_HW_ETH_DUPLEX_MODE_ENUM_FULL, "cOCTVC1_HW_ETH_DUPLEX_MODE_ENUM_FULL" },
	{ cOCTVC1_HW_ETH_DUPLEX_MODE_ENUM_HALF, "cOCTVC1_HW_ETH_DUPLEX_MODE_ENUM_HALF" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM[] = 
 {
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM_FREQ_1HZ, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM_FREQ_1HZ" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM_FREQ_10MHZ, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM_FREQ_10MHZ" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM_FREQ_30_72MHZ, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM_FREQ_30_72MHZ" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM_FREQ_1HZ_EXT, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM_FREQ_1HZ_EXT" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM_NONE, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM_NONE" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_SELECTION_ENUM[] = 
 {
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_SELECTION_ENUM_AUTOSELECT, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_SELECTION_ENUM_AUTOSELECT" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_SELECTION_ENUM_CONFIG_FILE, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_SELECTION_ENUM_CONFIG_FILE" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_SELECTION_ENUM_HOST_APPLICATION, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_SELECTION_ENUM_HOST_APPLICATION" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_STATE_ENUM[] = 
 {
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_STATE_ENUM_INVALID, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_STATE_ENUM_INVALID" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_STATE_ENUM_VALID, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_STATE_ENUM_VALID" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_STATE_ENUM_UNSPECIFIED, "cOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_STATE_ENUM_UNSPECIFIED" },
	{ 0,		NULL }
 };

const value_string  vals_tOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM[] = 
 {
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_UNINITIALIZE, "cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_UNINITIALIZE" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_IDLE, "cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_IDLE" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_NO_EXT_CLOCK, "cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_NO_EXT_CLOCK" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_LOCKED, "cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_LOCKED" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_UNLOCKED, "cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_UNLOCKED" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_ERROR, "cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_ERROR" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_DISABLE, "cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_DISABLE" },
	{ cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_LOSS_EXT_CLOCK, "cOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM_LOSS_EXT_CLOCK" },
	{ 0,		NULL }
 };

/****************************************************************************
	COMMON Registered 
 ****************************************************************************/

int ahf_tOCTVC1_HW_CPU_CORE_STATS[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_CPU_CORE_STATS;

void  register_tOCTVC1_HW_CPU_CORE_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_CPU_CORE_STATS[0],
			{ "ulCoreStatus", "octvc1.hw.cpu_core_stats.ulcorestatus",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CPU_CORE_STATUS_ENUM), 0x0,
			"ulCoreStatus",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_CPU_CORE_STATS[1],
			{ "ulProgramCounter", "octvc1.hw.cpu_core_stats.ulprogramcounter",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulProgramCounter",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_CPU_CORE_STATS[2],
			{ "ulFailureMask", "octvc1.hw.cpu_core_stats.ulfailuremask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulFailureMask",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_CPU_CORE_STATS[3],
			{ "ulAccessViolationAddress", "octvc1.hw.cpu_core_stats.ulaccessviolationaddress",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulAccessViolationAddress",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_CPU_CORE_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_CPU_CORE_INFO[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_CPU_CORE_INFO;

void  register_tOCTVC1_HW_CPU_CORE_INFO(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_CPU_CORE_INFO[0],
			{ "hProcess", "octvc1.hw.cpu_core_info.hprocess",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hProcess",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_CPU_CORE_INFO[1],
			{ "ulPhysicalCoreId", "octvc1.hw.cpu_core_info.ulphysicalcoreid",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPhysicalCoreId",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_CPU_CORE_INFO[2],
			{ "ulProcessImageType", "octvc1.hw.cpu_core_info.ulprocessimagetype",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_PROCESS_TYPE_ENUM), 0x0,
			"ulProcessImageType",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_CPU_CORE_INFO.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_ETH_PORT_CONFIG[7];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_ETH_PORT_CONFIG;

void  register_tOCTVC1_HW_ETH_PORT_CONFIG(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_ETH_PORT_CONFIG[0],
			{ "MacAddress", "octvc1.hw.eth_port_config.macaddress",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"MacAddress",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_CONFIG[1],
			{ "ulPromiscuousModeFlag", "octvc1.hw.eth_port_config.ulpromiscuousmodeflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulPromiscuousModeFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_CONFIG[2],
			{ "ulAcceptMulticastFlag", "octvc1.hw.eth_port_config.ulacceptmulticastflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulAcceptMulticastFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_CONFIG[3],
			{ "ulAcceptJumboFrameFlag", "octvc1.hw.eth_port_config.ulacceptjumboframeflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulAcceptJumboFrameFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_CONFIG[4],
			{ "ulSgmiiAutoNegotationFlag", "octvc1.hw.eth_port_config.ulsgmiiautonegotationflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulSgmiiAutoNegotationFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_CONFIG[5],
			{ "ulLinkSpeed", "octvc1.hw.eth_port_config.ullinkspeed",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_ETH_LINK_SPEED_ENUM), 0x0,
			"ulLinkSpeed",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_CONFIG[6],
			{ "ulDuplexMode", "octvc1.hw.eth_port_config.ulduplexmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_ETH_DUPLEX_MODE_ENUM), 0x0,
			"ulDuplexMode",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_ETH_PORT_CONFIG.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_ETH_PORT_TX_ERROR_STATS;

void  register_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[0],
			{ "ulTxUnderflowCnt", "octvc1.hw.eth_port_tx_error_stats.ultxunderflowcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxUnderflowCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[1],
			{ "ulTxLateCollisionCnt", "octvc1.hw.eth_port_tx_error_stats.ultxlatecollisioncnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxLateCollisionCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[2],
			{ "ulTxExcessCollisionCnt", "octvc1.hw.eth_port_tx_error_stats.ultxexcesscollisioncnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxExcessCollisionCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[3],
			{ "ulTxExcessDeferralCnt", "octvc1.hw.eth_port_tx_error_stats.ultxexcessdeferralcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxExcessDeferralCnt",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_ETH_PORT_TX_ERROR_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[6];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_ETH_PORT_TX_STATS;

void  register_tOCTVC1_HW_ETH_PORT_TX_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[0],
			{ "ulTxFrameCnt", "octvc1.hw.eth_port_tx_stats.ultxframecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxFrameCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[1],
			{ "ulTxByteCnt", "octvc1.hw.eth_port_tx_stats.ultxbytecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxByteCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[2],
			{ "ulTxPauseFrameCnt", "octvc1.hw.eth_port_tx_stats.ultxpauseframecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxPauseFrameCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[3],
			{ "ulTxVlanFrameCnt", "octvc1.hw.eth_port_tx_stats.ultxvlanframecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxVlanFrameCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[4],
			{ "ulTxJumboFrameCnt", "octvc1.hw.eth_port_tx_stats.ultxjumboframecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxJumboFrameCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[5],
			{ "TxErrorStat", "octvc1.hw.eth_port_tx_stats.txerrorstat",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TxErrorStat",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_ETH_PORT_TX_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[9];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_ETH_PORT_RX_ERROR_STATS;

void  register_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[0],
			{ "ulRxCrcErrorCnt", "octvc1.hw.eth_port_rx_error_stats.ulrxcrcerrorcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxCrcErrorCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[1],
			{ "ulRxAlignmentErrorCnt", "octvc1.hw.eth_port_rx_error_stats.ulrxalignmenterrorcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxAlignmentErrorCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[2],
			{ "ulRxJabberErrorCnt", "octvc1.hw.eth_port_rx_error_stats.ulrxjabbererrorcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxJabberErrorCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[3],
			{ "ulRxUndersizeCnt", "octvc1.hw.eth_port_rx_error_stats.ulrxundersizecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxUndersizeCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[4],
			{ "ulRxOversizeCnt", "octvc1.hw.eth_port_rx_error_stats.ulrxoversizecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxOversizeCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[5],
			{ "ulRxLengthCnt", "octvc1.hw.eth_port_rx_error_stats.ulrxlengthcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxLengthCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[6],
			{ "ulRxOutOfRangeCnt", "octvc1.hw.eth_port_rx_error_stats.ulrxoutofrangecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxOutOfRangeCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[7],
			{ "ulRxFifoOverflowCnt", "octvc1.hw.eth_port_rx_error_stats.ulrxfifooverflowcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxFifoOverflowCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[8],
			{ "ulRxWatchdogCnt", "octvc1.hw.eth_port_rx_error_stats.ulrxwatchdogcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxWatchdogCnt",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_ETH_PORT_RX_ERROR_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[6];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_ETH_PORT_RX_STATS;

void  register_tOCTVC1_HW_ETH_PORT_RX_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[0],
			{ "ulRxFrameCnt", "octvc1.hw.eth_port_rx_stats.ulrxframecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxFrameCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[1],
			{ "ulRxByteCnt", "octvc1.hw.eth_port_rx_stats.ulrxbytecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxByteCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[2],
			{ "ulRxJumboFrameCnt", "octvc1.hw.eth_port_rx_stats.ulrxjumboframecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxJumboFrameCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[3],
			{ "ulRxPauseFrameCnt", "octvc1.hw.eth_port_rx_stats.ulrxpauseframecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxPauseFrameCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[4],
			{ "ulRxVlanFrameCnt", "octvc1.hw.eth_port_rx_stats.ulrxvlanframecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxVlanFrameCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[5],
			{ "RxErrorStat", "octvc1.hw.eth_port_rx_stats.rxerrorstat",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"RxErrorStat",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_ETH_PORT_RX_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_RF_PORT_RX_STATS[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_RF_PORT_RX_STATS;

void  register_tOCTVC1_HW_RF_PORT_RX_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_RF_PORT_RX_STATS[0],
			{ "ulRxByteCnt", "octvc1.hw.rf_port_rx_stats.ulrxbytecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxByteCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_RF_PORT_RX_STATS[1],
			{ "ulRxOverflowCnt", "octvc1.hw.rf_port_rx_stats.ulrxoverflowcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxOverflowCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_RF_PORT_RX_STATS[2],
			{ "ulRxAverageBytePerSecond", "octvc1.hw.rf_port_rx_stats.ulrxaveragebytepersecond",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxAverageBytePerSecond",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_RF_PORT_RX_STATS[3],
			{ "ulRxAveragePeriodUs", "octvc1.hw.rf_port_rx_stats.ulrxaverageperiodus",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxAveragePeriodUs",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_RF_PORT_RX_STATS[4],
			{ "ulFrequencyKhz", "octvc1.hw.rf_port_rx_stats.ulfrequencykhz",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulFrequencyKhz",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_RF_PORT_RX_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_RF_PORT_TX_STATS[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_RF_PORT_TX_STATS;

void  register_tOCTVC1_HW_RF_PORT_TX_STATS(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_RF_PORT_TX_STATS[0],
			{ "ulTxByteCnt", "octvc1.hw.rf_port_tx_stats.ultxbytecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxByteCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_RF_PORT_TX_STATS[1],
			{ "ulTxUnderflowCnt", "octvc1.hw.rf_port_tx_stats.ultxunderflowcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxUnderflowCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_RF_PORT_TX_STATS[2],
			{ "ulTxAverageBytePerSecond", "octvc1.hw.rf_port_tx_stats.ultxaveragebytepersecond",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxAverageBytePerSecond",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_RF_PORT_TX_STATS[3],
			{ "ulTxAveragePeriodUs", "octvc1.hw.rf_port_tx_stats.ultxaverageperiodus",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxAveragePeriodUs",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_RF_PORT_TX_STATS[4],
			{ "ulFrequencyKhz", "octvc1.hw.rf_port_tx_stats.ulfrequencykhz",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulFrequencyKhz",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_RF_PORT_TX_STATS.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	Event Registered 
 ****************************************************************************/

int ahf_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT;

void  register_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT[0],
			{ "ulCoreIndex", "octvc1.hw.cpu_core.exec_report.ulcoreindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulCoreIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT[1],
			{ "ulCoreUseMask", "octvc1.hw.cpu_core.exec_report.ulcoreusemask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulCoreUseMask",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT[2],
			{ "ulCoreHaltMask", "octvc1.hw.cpu_core.exec_report.ulcorehaltmask",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"ulCoreHaltMask",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT;

void  register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT[0],
			{ "ulState", "octvc1.hw.clock_sync_mgr.status_change.ulstate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM), 0x0,
			"ulState",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT[1],
			{ "ulPreviousState", "octvc1.hw.clock_sync_mgr.status_change.ulpreviousstate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM), 0x0,
			"ulPreviousState",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	CMD/RSP Registered 
 ****************************************************************************/

int ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[9];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_PCB_INFO_RSP;

void  register_tOCTVC1_HW_MSG_PCB_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[0],
			{ "szName", "octvc1.hw.pcb.info.szname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szName",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[1],
			{ "ulDeviceId", "octvc1.hw.pcb.info.uldeviceid",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulDeviceId",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[2],
			{ "szSerial", "octvc1.hw.pcb.info.szserial",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szSerial",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[3],
			{ "szFilename", "octvc1.hw.pcb.info.szfilename",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szFilename",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[4],
			{ "ulInfoSource", "octvc1.hw.pcb.info.ulinfosource",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_PCB_INFO_SOURCE_ENUM), 0x0,
			"ulInfoSource",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[5],
			{ "ulInfoState", "octvc1.hw.pcb.info.ulinfostate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_PCB_INFO_STATE_ENUM), 0x0,
			"ulInfoState",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[6],
			{ "szGpsName", "octvc1.hw.pcb.info.szgpsname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szGpsName",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[7],
			{ "szWifiName", "octvc1.hw.pcb.info.szwifiname",
			FT_STRING, BASE_NONE, NULL, 0x0,
			"szWifiName",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_PCB_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CPU_CORE_STATS_CMD;

void  register_tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD[0],
			{ "ulCoreIndex", "octvc1.hw.cpu_core.stats.ulcoreindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulCoreIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CPU_CORE_STATS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CPU_CORE_STATS_RSP;

void  register_tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP[0],
			{ "CoreStats", "octvc1.hw.cpu_core.stats.corestats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"CoreStats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CPU_CORE_STATS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CPU_CORE_INFO_CMD;

void  register_tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD[0],
			{ "ulCoreIndex", "octvc1.hw.cpu_core.info.ulcoreindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulCoreIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CPU_CORE_INFO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CPU_CORE_INFO_RSP;

void  register_tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP[0],
			{ "CoreInfo", "octvc1.hw.cpu_core.info.coreinfo",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"CoreInfo",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CPU_CORE_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CPU_CORE_LIST_CMD;

void  register_tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD[0],
			{ "IndexGet", "octvc1.hw.cpu_core.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CPU_CORE_LIST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CPU_CORE_LIST_RSP;

void  register_tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP[0],
			{ "IndexGet", "octvc1.hw.cpu_core.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP[1],
			{ "IndexList", "octvc1.hw.cpu_core.list.indexlist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CPU_CORE_LIST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_ETH_PORT_INFO_CMD;

void  register_tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD[0],
			{ "ulPortIndex", "octvc1.hw.eth_port.info.ulportindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_ETH_PORT_INFO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[9];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_ETH_PORT_INFO_RSP;

void  register_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[0],
			{ "ulPortIndex", "octvc1.hw.eth_port.info.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[1],
			{ "ulInterfaceId", "octvc1.hw.eth_port.info.ulinterfaceid",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulInterfaceId",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[2],
			{ "ulMode", "octvc1.hw.eth_port.info.ulmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_ETH_MODE_ENUM), 0x0,
			"ulMode",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[3],
			{ "ulTxPktQueuesByteSize", "octvc1.hw.eth_port.info.ultxpktqueuesbytesize",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulTxPktQueuesByteSize",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[4],
			{ "ulRxPktQueuesByteSize", "octvc1.hw.eth_port.info.ulrxpktqueuesbytesize",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulRxPktQueuesByteSize",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[5],
			{ "ulRestrictedApiFlag", "octvc1.hw.eth_port.info.ulrestrictedapiflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulRestrictedApiFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[6],
			{ "ulEnableFlag", "octvc1.hw.eth_port.info.ulenableflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulEnableFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[7],
			{ "Config", "octvc1.hw.eth_port.info.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_ETH_PORT_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_ETH_PORT_LIST_CMD;

void  register_tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD[0],
			{ "IndexGet", "octvc1.hw.eth_port.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_ETH_PORT_LIST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_ETH_PORT_LIST_RSP;

void  register_tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP[0],
			{ "IndexGet", "octvc1.hw.eth_port.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP[1],
			{ "IndexList", "octvc1.hw.eth_port.list.indexlist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_ETH_PORT_LIST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_ETH_PORT_STATS_CMD;

void  register_tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD[0],
			{ "ulPortIndex", "octvc1.hw.eth_port.stats.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD[1],
			{ "ulResetStatsFlag", "octvc1.hw.eth_port.stats.ulresetstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetStatsFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_ETH_PORT_STATS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_ETH_PORT_STATS_RSP;

void  register_tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP[0],
			{ "ulPortIndex", "octvc1.hw.eth_port.stats.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP[1],
			{ "RxStats", "octvc1.hw.eth_port.stats.rxstats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"RxStats",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP[2],
			{ "TxStats", "octvc1.hw.eth_port.stats.txstats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TxStats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_ETH_PORT_STATS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD;

void  register_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD[0],
			{ "ulPortIndex", "octvc1.hw.eth_port.restricted_unblock.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD[1],
			{ "ulPassword", "octvc1.hw.eth_port.restricted_unblock.ulpassword",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPassword",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP;

void  register_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP[0],
			{ "ulPortIndex", "octvc1.hw.eth_port.restricted_unblock.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD;

void  register_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD[0],
			{ "ulPortIndex", "octvc1.hw.eth_port.modify.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD[1],
			{ "Config", "octvc1.hw.eth_port.modify.config",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"Config",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP;

void  register_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP[0],
			{ "ulPortIndex", "octvc1.hw.eth_port.modify.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_INFO_CMD;

void  register_tOCTVC1_HW_MSG_RF_PORT_INFO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_CMD[0],
			{ "ulPortIndex", "octvc1.hw.rf_port.info.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_INFO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[7];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_INFO_RSP;

void  register_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[0],
			{ "ulPortIndex", "octvc1.hw.rf_port.info.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[1],
			{ "ulInService", "octvc1.hw.rf_port.info.ulinservice",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulInService",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[2],
			{ "hOwner", "octvc1.hw.rf_port.info.howner",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"hOwner",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[3],
			{ "ulPortInterfaceId", "octvc1.hw.rf_port.info.ulportinterfaceid",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortInterfaceId",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[4],
			{ "ulFrequencyMinKhz", "octvc1.hw.rf_port.info.ulfrequencyminkhz",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulFrequencyMinKhz",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[5],
			{ "ulFrequencyMaxKhz", "octvc1.hw.rf_port.info.ulfrequencymaxkhz",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulFrequencyMaxKhz",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_STATS_CMD;

void  register_tOCTVC1_HW_MSG_RF_PORT_STATS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_CMD[0],
			{ "ulPortIndex", "octvc1.hw.rf_port.stats.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_CMD[1],
			{ "ulResetStatsFlag", "octvc1.hw.rf_port.stats.ulresetstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetStatsFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_STATS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_STATS_RSP;

void  register_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP[0],
			{ "ulPortIndex", "octvc1.hw.rf_port.stats.ulportindex",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP[1],
			{ "ulRadioStandard", "octvc1.hw.rf_port.stats.ulradiostandard",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_RADIO_STANDARD_ENUM), 0x0,
			"ulRadioStandard",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP[2],
			{ "RxStats", "octvc1.hw.rf_port.stats.rxstats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"RxStats",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP[3],
			{ "TxStats", "octvc1.hw.rf_port.stats.txstats",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"TxStats",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_STATS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_LIST_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_LIST_CMD;

void  register_tOCTVC1_HW_MSG_RF_PORT_LIST_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_LIST_CMD[0],
			{ "IndexGet", "octvc1.hw.rf_port.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_LIST_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_LIST_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_LIST_RSP;

void  register_tOCTVC1_HW_MSG_RF_PORT_LIST_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_LIST_RSP[0],
			{ "IndexGet", "octvc1.hw.rf_port.list.indexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexGet",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_LIST_RSP[1],
			{ "IndexList", "octvc1.hw.rf_port.list.indexlist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"IndexList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_LIST_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD;

void  register_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD[0],
			{ "SubIndexGet", "octvc1.hw.rf_port.antenna.list.subindexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SubIndexGet",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP;

void  register_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP[0],
			{ "SubIndexGet", "octvc1.hw.rf_port.antenna.list.subindexget",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SubIndexGet",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP[1],
			{ "SubIndexList", "octvc1.hw.rf_port.antenna.list.subindexlist",
			FT_NONE, BASE_NONE, NULL, 0x0,
			"SubIndexList",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD;

void  register_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD[0],
			{ "ulPortIndex", "octvc1.hw.rf_port.antenna.rx_config.info.ulportindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD[1],
			{ "ulAntennaIndex", "octvc1.hw.rf_port.antenna.rx_config.info.ulantennaindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulAntennaIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[6];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP;

void  register_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[0],
			{ "ulPortIndex", "octvc1.hw.rf_port.antenna.rx_config.info.ulportindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[1],
			{ "ulAntennaIndex", "octvc1.hw.rf_port.antenna.rx_config.info.ulantennaindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulAntennaIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[2],
			{ "ulEnableFlag", "octvc1.hw.rf_port.antenna.rx_config.info.ulenableflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulEnableFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[3],
			{ "lRxGaindB", "octvc1.hw.rf_port.antenna.rx_config.info.lrxgaindb",
			FT_INT32,BASE_DEC, NULL, 0x0,
			"lRxGaindB",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[4],
			{ "ulRxGainMode", "octvc1.hw.rf_port.antenna.rx_config.info.ulrxgainmode",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_RADIO_RX_GAIN_CTRL_MODE_ENUM), 0x0,
			"ulRxGainMode",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD;

void  register_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD[0],
			{ "ulPortIndex", "octvc1.hw.rf_port.antenna.tx_config.info.ulportindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD[1],
			{ "ulAntennaIndex", "octvc1.hw.rf_port.antenna.tx_config.info.ulantennaindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulAntennaIndex",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP[5];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP;

void  register_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP[0],
			{ "ulPortIndex", "octvc1.hw.rf_port.antenna.tx_config.info.ulportindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulPortIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP[1],
			{ "ulAntennaIndex", "octvc1.hw.rf_port.antenna.tx_config.info.ulantennaindex",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulAntennaIndex",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP[2],
			{ "ulEnableFlag", "octvc1.hw.rf_port.antenna.tx_config.info.ulenableflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulEnableFlag",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP[3],
			{ "lTxGaindB", "octvc1.hw.rf_port.antenna.tx_config.info.ltxgaindb",
			FT_INT32,BASE_DEC, NULL, 0x0,
			"lTxGaindB",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP;

void  register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP[0],
			{ "ulClkSourceRef", "octvc1.hw.clock_sync_mgr.info.ulclksourceref",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM), 0x0,
			"ulClkSourceRef",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP[1],
			{ "ulClkSourceSelection", "octvc1.hw.clock_sync_mgr.info.ulclksourceselection",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_SELECTION_ENUM), 0x0,
			"ulClkSourceSelection",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD[2];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD;

void  register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD[0],
			{ "ulResetStatsFlag", "octvc1.hw.clock_sync_mgr.stats.ulresetstatsflag",
			FT_BOOLEAN, BASE_NONE, NULL, 0x0,
			"ulResetStatsFlag",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[10];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP;

void  register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[0],
			{ "ulState", "octvc1.hw.clock_sync_mgr.stats.ulstate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CLOCK_SYNC_MGR_STATE_ENUM), 0x0,
			"ulState",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[1],
			{ "lClockError", "octvc1.hw.clock_sync_mgr.stats.lclockerror",
			FT_INT32,BASE_DEC, NULL, 0x0,
			"lClockError",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[2],
			{ "lDroppedCycles", "octvc1.hw.clock_sync_mgr.stats.ldroppedcycles",
			FT_INT32,BASE_DEC, NULL, 0x0,
			"lDroppedCycles",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[3],
			{ "ulPllFreqHz", "octvc1.hw.clock_sync_mgr.stats.ulpllfreqhz",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPllFreqHz",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[4],
			{ "ulPllFractionalFreqHz", "octvc1.hw.clock_sync_mgr.stats.ulpllfractionalfreqhz",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulPllFractionalFreqHz",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[5],
			{ "ulSlipCnt", "octvc1.hw.clock_sync_mgr.stats.ulslipcnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSlipCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[6],
			{ "ulSyncLosseCnt", "octvc1.hw.clock_sync_mgr.stats.ulsynclossecnt",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSyncLosseCnt",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[7],
			{ "ulSourceState", "octvc1.hw.clock_sync_mgr.stats.ulsourcestate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_STATE_ENUM), 0x0,
			"ulSourceState",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[8],
			{ "ulDacValue", "octvc1.hw.clock_sync_mgr.stats.uldacvalue",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulDacValue",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP;

void  register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP[0],
			{ "ulClkSourceRef", "octvc1.hw.clock_sync_mgr.source.info.ulclksourceref",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM), 0x0,
			"ulClkSourceRef",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP[1],
			{ "ulSourceState", "octvc1.hw.clock_sync_mgr.source.info.ulsourcestate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_STATE_ENUM), 0x0,
			"ulSourceState",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD[3];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD;

void  register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD[0],
			{ "ulClkSourceRef", "octvc1.hw.clock_sync_mgr.source.modify.ulclksourceref",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_ENUM), 0x0,
			"ulClkSourceRef",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD[1],
			{ "ulSourceState", "octvc1.hw.clock_sync_mgr.source.modify.ulsourcestate",
			FT_UINT32,BASE_HEX, VALS(vals_tOCTVC1_HW_CLOCK_SYNC_MGR_SOURCE_STATE_ENUM), 0x0,
			"ulSourceState",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

int ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD[4];
tWS_ENTITY_HANDLE ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD;

void  register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD(void)
{
	static hf_register_info hf[] =
	{
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD[0],
			{ "ulDacInitValue", "octvc1.hw.clock_sync_mgr.synchro.start.uldacinitvalue",
			FT_UINT32,BASE_HEX, NULL, 0x0,
			"ulDacInitValue",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD[1],
			{ "ulSyncWindowSize", "octvc1.hw.clock_sync_mgr.synchro.start.ulsyncwindowsize",
			FT_UINT32,BASE_DEC, NULL, 0x0,
			"ulSyncWindowSize",HFILL }

		},
		{
			&ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD[2],
			{ "lSyncThreshold", "octvc1.hw.clock_sync_mgr.synchro.start.lsyncthreshold",
			FT_INT32,BASE_DEC, NULL, 0x0,
			"lSyncThreshold",HFILL }

		},
	};

	static gint* ett[] =
	{
		&ws_htOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD.ett
	};

	proto_register_field_array( proto_octvc1_ctrl, hf, array_length(hf) );

};

/****************************************************************************
	COMMON dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_HW_CPU_CORE_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_CPU_CORE_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_CPU_CORE_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_CPU_CORE_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_HW_CPU_CORE_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_CPU_CORE_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_STATS, ulCoreStatus), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_CPU_CORE_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_STATS, ulCoreStatus);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_CPU_CORE_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_STATS, ulProgramCounter), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_CPU_CORE_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_STATS, ulProgramCounter);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_HW_CPU_CORE_STATS[2], tvb, offset,
			4,"ulFailureMask:%s (0x%08x)", pExtValue->pszValue, temp_data);
		}else{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_HW_CPU_CORE_STATS[2], tvb, offset,
			4,"ulFailureMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_HW_CPU_CORE_FAILURE_MASK); i++ )
			{
				if( ( vals_tOCTVC1_HW_CPU_CORE_FAILURE_MASK[i].value && 
					( vals_tOCTVC1_HW_CPU_CORE_FAILURE_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_HW_CPU_CORE_FAILURE_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_HW_CPU_CORE_FAILURE_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_HW_CPU_CORE_FAILURE_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_STATS, ulFailureMask);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_CPU_CORE_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_STATS, ulAccessViolationAddress), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_CPU_CORE_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_STATS, ulAccessViolationAddress);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_HW_CPU_CORE_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_CPU_CORE_INFO)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_CPU_CORE_INFO (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_CPU_CORE_INFO));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_HW_CPU_CORE_INFO);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HANDLE_INVALID)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_CPU_CORE_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_INFO, hProcess),temp_data, "cOCTVC1_HANDLE_INVALID (0x%08x)",temp_data);
		}else {
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_CPU_CORE_INFO[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_INFO, hProcess), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_CPU_CORE_INFO[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_INFO, hProcess);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_CPU_CORE_INFO[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_INFO, ulPhysicalCoreId), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_CPU_CORE_INFO[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_INFO, ulPhysicalCoreId);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_CPU_CORE_INFO[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_INFO, ulProcessImageType), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_CPU_CORE_INFO[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_CPU_CORE_INFO, ulProcessImageType);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_HW_ETH_PORT_CONFIG(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_ETH_PORT_CONFIG)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_ETH_PORT_CONFIG (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_ETH_PORT_CONFIG));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_HW_ETH_PORT_CONFIG);
		{
		int ulMac0, ulMac1;
			unsigned int	offs = offset;
			ulMac0 = tvb_get_ntohl( tvb, offs );
			offs += 4;
			ulMac1 = tvb_get_ntohl( tvb, offs );
		ti = proto_tree_add_string_format(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[0], tvb, offset,
			sizeof(tOCTVC1_MAC_ADDRESS), "","MacAddress: %02x:%02x:%02x:%02x:%02x:%02x", 
			(( ulMac0 & 0x0000FF00 ) >> 8 ),
			( ulMac0 & 0x000000FF ),
			(( ulMac1 & 0xFF000000 ) >> 24 ),
			(( ulMac1 & 0x00FF0000 ) >> 16 ),
			(( ulMac1 & 0x0000FF00 ) >> 8 ),
			( ulMac1 & 0x000000FF ) );
		}
		offset += sizeof(tOCTVC1_MAC_ADDRESS);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulPromiscuousModeFlag), temp_data, "%s (0x%x)", pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulPromiscuousModeFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulPromiscuousModeFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulAcceptMulticastFlag), temp_data, "%s (0x%x)", pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulAcceptMulticastFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulAcceptMulticastFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulAcceptJumboFrameFlag), temp_data, "%s (0x%x)", pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulAcceptJumboFrameFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulAcceptJumboFrameFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_boolean_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulSgmiiAutoNegotationFlag), temp_data, "%s (0x%x)", pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulSgmiiAutoNegotationFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulSgmiiAutoNegotationFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulLinkSpeed), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulLinkSpeed);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulDuplexMode), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_CONFIG[6], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_CONFIG, ulDuplexMode);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS, ulTxUnderflowCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS, ulTxUnderflowCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS, ulTxLateCollisionCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS, ulTxLateCollisionCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS, ulTxExcessCollisionCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS, ulTxExcessCollisionCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS, ulTxExcessDeferralCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS, ulTxExcessDeferralCnt);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_HW_ETH_PORT_TX_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_ETH_PORT_TX_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_ETH_PORT_TX_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_ETH_PORT_TX_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_HW_ETH_PORT_TX_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, ulTxFrameCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, ulTxFrameCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, ulTxByteCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, ulTxByteCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, ulTxPauseFrameCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, ulTxPauseFrameCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, ulTxVlanFrameCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, ulTxVlanFrameCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, ulTxJumboFrameCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_TX_STATS[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, ulTxJumboFrameCnt);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_TX_STATS, TxErrorStat), "TxErrorStat:tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxCrcErrorCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxCrcErrorCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxAlignmentErrorCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxAlignmentErrorCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxJabberErrorCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxJabberErrorCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxUndersizeCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxUndersizeCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxOversizeCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxOversizeCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxLengthCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxLengthCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxOutOfRangeCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[6], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxOutOfRangeCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[7], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxFifoOverflowCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[7], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxFifoOverflowCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[8], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxWatchdogCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS[8], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS, ulRxWatchdogCnt);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_HW_ETH_PORT_RX_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_ETH_PORT_RX_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_ETH_PORT_RX_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_ETH_PORT_RX_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_HW_ETH_PORT_RX_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, ulRxFrameCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, ulRxFrameCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, ulRxByteCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, ulRxByteCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, ulRxJumboFrameCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, ulRxJumboFrameCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, ulRxPauseFrameCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, ulRxPauseFrameCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, ulRxVlanFrameCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_ETH_PORT_RX_STATS[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, ulRxVlanFrameCnt);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_ETH_PORT_RX_STATS, RxErrorStat), "RxErrorStat:tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS( tvb, pinfo, sub_tree2, offset, pExtValue );
		}
	}


	return offset;

};


unsigned int  dissect_tOCTVC1_HW_RF_PORT_RX_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_RF_PORT_RX_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_RF_PORT_RX_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_RF_PORT_RX_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_HW_RF_PORT_RX_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_RF_PORT_RX_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_RX_STATS, ulRxByteCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_RF_PORT_RX_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_RX_STATS, ulRxByteCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_RF_PORT_RX_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_RX_STATS, ulRxOverflowCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_RF_PORT_RX_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_RX_STATS, ulRxOverflowCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_RF_PORT_RX_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_RX_STATS, ulRxAverageBytePerSecond), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_RF_PORT_RX_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_RX_STATS, ulRxAverageBytePerSecond);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_RF_PORT_RX_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_RX_STATS, ulRxAveragePeriodUs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_RF_PORT_RX_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_RX_STATS, ulRxAveragePeriodUs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_RF_PORT_RX_STATS[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_RX_STATS, ulFrequencyKhz), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_RF_PORT_RX_STATS[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_RX_STATS, ulFrequencyKhz);

	}


	return offset;

};


unsigned int  dissect_tOCTVC1_HW_RF_PORT_TX_STATS(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset, tWS_EXTRA_VALUE *pExtValue )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_RF_PORT_TX_STATS)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_RF_PORT_TX_STATS (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_RF_PORT_TX_STATS));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		bytes = sizeof(tOCTVC1_HW_RF_PORT_TX_STATS);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_RF_PORT_TX_STATS[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_TX_STATS, ulTxByteCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_RF_PORT_TX_STATS[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_TX_STATS, ulTxByteCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_RF_PORT_TX_STATS[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_TX_STATS, ulTxUnderflowCnt), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_RF_PORT_TX_STATS[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_TX_STATS, ulTxUnderflowCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_RF_PORT_TX_STATS[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_TX_STATS, ulTxAverageBytePerSecond), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_RF_PORT_TX_STATS[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_TX_STATS, ulTxAverageBytePerSecond);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_RF_PORT_TX_STATS[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_TX_STATS, ulTxAveragePeriodUs), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_RF_PORT_TX_STATS[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_TX_STATS, ulTxAveragePeriodUs);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( pExtValue && (pExtValue->lValue == temp_data ) )
		{
		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_RF_PORT_TX_STATS[4], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_TX_STATS, ulFrequencyKhz), temp_data, "%s (0x%x)",pExtValue->pszValue, temp_data );
		}else
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_RF_PORT_TX_STATS[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_RF_PORT_TX_STATS, ulFrequencyKhz);

	}


	return offset;

};

/****************************************************************************
	Event dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT, ulCoreIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT[1], tvb, offset,
			4,"ulCoreUseMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_HW_CPU_CORE_MASK); i++ )
			{
				if( ( vals_tOCTVC1_HW_CPU_CORE_MASK[i].value && 
					( vals_tOCTVC1_HW_CPU_CORE_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_HW_CPU_CORE_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_HW_CPU_CORE_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_HW_CPU_CORE_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT, ulCoreUseMask);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_none_format(field_tree, ahf_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT[2], tvb, offset,
			4,"ulCoreHaltMask: (0x%08x)", temp_data);
		if(temp_data)
		{
			int i;
			proto_tree*	sub_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			for( i=0; i< mWS_COUNTOF(vals_tOCTVC1_HW_CPU_CORE_MASK); i++ )
			{
				if( ( vals_tOCTVC1_HW_CPU_CORE_MASK[i].value && 
					( vals_tOCTVC1_HW_CPU_CORE_MASK[i].value & temp_data ) == 
					 vals_tOCTVC1_HW_CPU_CORE_MASK[i].value))
					proto_tree_add_text(sub_tree, tvb, offset,4,"%s",
					decode_enumerated_bitfield(vals_tOCTVC1_HW_CPU_CORE_MASK[i].value, 0xFFFFFFFF, (4*8),
				VALS(vals_tOCTVC1_HW_CPU_CORE_MASK),"%s"));
			}
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT, ulCoreHaltMask);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT)-sizeof(tOCTVC1_EVENT_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT)-sizeof(tOCTVC1_EVENT_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT, ulState);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT, ulPreviousState);

	}


	return 0;

};

/****************************************************************************
	CMD/RSP dissectors 
 ****************************************************************************/


unsigned int  dissect_tOCTVC1_HW_MSG_PCB_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_PCB_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_PCB_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_PCB_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_PCB_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, szName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, szName);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, ulDeviceId);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, szSerial), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, szSerial);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[3], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, szFilename), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, szFilename);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, ulInfoSource);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, ulInfoState);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, szGpsName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, szGpsName);

		ti = proto_tree_add_item(field_tree, ahf_tOCTVC1_HW_MSG_PCB_INFO_RSP[7], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, szWifiName), FALSE);

		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_PCB_INFO_RSP, szWifiName);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD, ulCoreIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP, CoreStats), "CoreStats:tOCTVC1_HW_CPU_CORE_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_HW_CPU_CORE_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD, ulCoreIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP, CoreInfo), "CoreInfo:tOCTVC1_HW_CPU_CORE_INFO");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_HW_CPU_CORE_INFO( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP, IndexList), "IndexList:tOCTVC1_LIST_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD, ulPortIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP, ulPortIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP, ulInterfaceId);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP, ulMode);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP, ulTxPktQueuesByteSize);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP, ulRxPktQueuesByteSize);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[5], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP, ulRestrictedApiFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP, ulRestrictedApiFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP[6], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP, ulEnableFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP, ulEnableFlag);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP, Config), "Config:tOCTVC1_HW_ETH_PORT_CONFIG");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_HW_ETH_PORT_CONFIG( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP, IndexList), "IndexList:tOCTVC1_LIST_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD, ulPortIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD, ulResetStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD, ulResetStatsFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP, ulPortIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP, RxStats), "RxStats:tOCTVC1_HW_ETH_PORT_RX_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_HW_ETH_PORT_RX_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP, TxStats), "TxStats:tOCTVC1_HW_ETH_PORT_TX_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_HW_ETH_PORT_TX_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD, ulPortIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_HW_PRIVATE_API_CMD_PASSWORD)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD, ulPassword),temp_data, "cOCTVC1_HW_PRIVATE_API_CMD_PASSWORD (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD[1], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD, ulPassword);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP, ulPortIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD, ulPortIndex);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD, Config), "Config:tOCTVC1_HW_ETH_PORT_CONFIG");
		{
			proto_tree*	sub_tree2;
			tWS_EXTRA_VALUE ExtraVal;
		ExtraVal.lValue = cOCTVC1_DO_NOT_MODIFY;
		ExtraVal.pszValue = "cOCTVC1_DO_NOT_MODIFY";
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_HW_ETH_PORT_CONFIG( tvb, pinfo, sub_tree2, offset, &ExtraVal );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP, ulPortIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_CMD, ulPortIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_RSP, ulPortIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_RSP, ulInService), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_RSP, ulInService);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_RSP, hOwner);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_RSP, ulPortInterfaceId);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_RSP, ulFrequencyMinKhz);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_RSP, ulFrequencyMaxKhz);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_STATS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_STATS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_STATS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_STATS_CMD, ulPortIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_STATS_CMD, ulResetStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_STATS_CMD, ulResetStatsFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_STATS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_STATS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_STATS_RSP, ulPortIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_STATS_RSP, ulRadioStandard);

		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_STATS_RSP, RxStats), "RxStats:tOCTVC1_HW_RF_PORT_RX_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_HW_RF_PORT_RX_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_STATS_RSP, TxStats), "TxStats:tOCTVC1_HW_RF_PORT_TX_STATS");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_HW_RF_PORT_TX_STATS( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_LIST_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_LIST_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_LIST_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_LIST_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_LIST_CMD, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_LIST_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_LIST_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_LIST_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_LIST_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_LIST_RSP, IndexGet), "IndexGet:tOCTVC1_CURSOR_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_LIST_RSP, IndexList), "IndexList:tOCTVC1_LIST_INDEX_GET");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_INDEX_GET( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD, SubIndexGet), "SubIndexGet:tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP, SubIndexGet), "SubIndexGet:tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_CURSOR_INDEX_GET_SUB_INDEX( tvb, pinfo, sub_tree2, offset, NULL );
		}
		ti = proto_tree_add_text(field_tree, tvb, offset, 
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP, SubIndexList), "SubIndexList:tOCTVC1_LIST_INDEX_GET_SUB_INDEX");
		{
			proto_tree*	sub_tree2;
			sub_tree2 = proto_item_add_subtree(ti, ett_octvc1_msg_type);
			offset = dissect_tOCTVC1_LIST_INDEX_GET_SUB_INDEX( tvb, pinfo, sub_tree2, offset, NULL );
		}
	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD, ulPortIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD, ulAntennaIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP, ulPortIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP, ulAntennaIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP, ulEnableFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP, ulEnableFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP, lRxGaindB);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP, ulRxGainMode);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD, ulPortIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD, ulAntennaIndex);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP, ulPortIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP, ulAntennaIndex);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP[2], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP, ulEnableFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP, ulEnableFlag);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP, lTxGaindB);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP, ulClkSourceRef);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP, ulClkSourceSelection);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_boolean(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD, ulResetStatsFlag), temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD, ulResetStatsFlag);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP, ulState);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP, lClockError);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP, lDroppedCycles);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[3], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP, ulPllFreqHz);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[4], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP, ulPllFractionalFreqHz);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[5], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP, ulSlipCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[6], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP, ulSyncLosseCnt);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[7], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP, ulSourceState);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP[8], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP, ulDacValue);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP, ulClkSourceRef);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP, ulSourceState);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_DO_NOT_MODIFY)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD[0], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD, ulClkSourceRef),temp_data, "cOCTVC1_DO_NOT_MODIFY (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD[0], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD, ulClkSourceRef);

		temp_data = tvb_get_ntohl( tvb, offset );
		if( temp_data == cOCTVC1_DO_NOT_MODIFY)
		{		ti = proto_tree_add_uint_format_value(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD[1], tvb, offset,
			mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD, ulSourceState),temp_data, "cOCTVC1_DO_NOT_MODIFY (0x%08x)",temp_data);
		}else {
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD[1], tvb, offset,
			4, temp_data);
		}
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD, ulSourceState);

	}


	return 0;

};


unsigned int  dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, unsigned int offset )
{
	proto_tree *field_tree = tree;
	unsigned int offset_0 = offset;
	gint bytes = tvb_length_remaining(tvb, offset);
	if (bytes < (gint)(sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD)-sizeof(tOCTVC1_MSG_HEADER)))
	{
		proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD (%d byte%s) - Malformated packet...expected(%ld)",
			bytes, plurality(bytes, "", "s"),
			sizeof(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD)-sizeof(tOCTVC1_MSG_HEADER));
		return 1;
	}
	else
	{
		int  temp_data = 0;
		proto_item *ti;
		ti = proto_tree_add_text(tree, tvb, offset, bytes,
			"tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD (%d byte%s)", bytes, plurality(bytes, "", "s"));

		field_tree = proto_item_add_subtree(ti, ett_octvc1_msg_type);
		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD[0], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD, ulDacInitValue);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_uint(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD[1], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD, ulSyncWindowSize);

		temp_data = tvb_get_ntohl( tvb, offset );
		{
		ti = proto_tree_add_int(field_tree, ahf_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD[2], tvb, offset,
			4, temp_data);
		}
		offset += mWS_FIELDSIZE(tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD, lSyncThreshold);

	}


	return 0;

};
/****************************************************************************
	MODULE REGISTERED EXPORTED FUNCTION
 ****************************************************************************/

void ws_register_OCTVC1_HW(void)
{
	/****************************************************************************
		Register Common struct
	****************************************************************************/
	register_tOCTVC1_HW_CPU_CORE_STATS(); 
	register_tOCTVC1_HW_CPU_CORE_INFO(); 
	register_tOCTVC1_HW_ETH_PORT_CONFIG(); 
	register_tOCTVC1_HW_ETH_PORT_TX_ERROR_STATS(); 
	register_tOCTVC1_HW_ETH_PORT_TX_STATS(); 
	register_tOCTVC1_HW_ETH_PORT_RX_ERROR_STATS(); 
	register_tOCTVC1_HW_ETH_PORT_RX_STATS(); 
	register_tOCTVC1_HW_RF_PORT_RX_STATS(); 
	register_tOCTVC1_HW_RF_PORT_TX_STATS(); 

	/****************************************************************************
		CMD/RSP Registered 
	****************************************************************************/
	register_tOCTVC1_HW_MSG_PCB_INFO_RSP(); 
	register_tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD(); 
	register_tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP(); 
	register_tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD(); 
	register_tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP(); 
	register_tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD(); 
	register_tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP(); 
	register_tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD(); 
	register_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP(); 
	register_tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD(); 
	register_tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP(); 
	register_tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD(); 
	register_tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP(); 
	register_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD(); 
	register_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP(); 
	register_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD(); 
	register_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP(); 
	register_tOCTVC1_HW_MSG_RF_PORT_INFO_CMD(); 
	register_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP(); 
	register_tOCTVC1_HW_MSG_RF_PORT_STATS_CMD(); 
	register_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP(); 
	register_tOCTVC1_HW_MSG_RF_PORT_LIST_CMD(); 
	register_tOCTVC1_HW_MSG_RF_PORT_LIST_RSP(); 
	register_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD(); 
	register_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP(); 
	register_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD(); 
	register_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP(); 
	register_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD(); 
	register_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP(); 
	register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP(); 
	register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD(); 
	register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP(); 
	register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP(); 
	register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD(); 
	register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD(); 

	/****************************************************************************
		Event Registered 
	****************************************************************************/
	register_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT(); 
	register_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT(); 

}

/****************************************************************************
	MODULE DISSECTOR FUNCTIONS
 ****************************************************************************/
int ws_dissect_OCTVC1_HW_CMD( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_HW_MSG_PCB_INFO_CID: return 0; break;
			case cOCTVC1_HW_MSG_CPU_CORE_STATS_CID: return dissect_tOCTVC1_HW_MSG_CPU_CORE_STATS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CPU_CORE_INFO_CID: return dissect_tOCTVC1_HW_MSG_CPU_CORE_INFO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CPU_CORE_LIST_CID: return dissect_tOCTVC1_HW_MSG_CPU_CORE_LIST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_ETH_PORT_INFO_CID: return dissect_tOCTVC1_HW_MSG_ETH_PORT_INFO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_ETH_PORT_LIST_CID: return dissect_tOCTVC1_HW_MSG_ETH_PORT_LIST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_ETH_PORT_STATS_CID: return dissect_tOCTVC1_HW_MSG_ETH_PORT_STATS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CID: return dissect_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_ETH_PORT_MODIFY_CID: return dissect_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_INFO_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_STATS_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_STATS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_LIST_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_LIST_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_CID: return 0; break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CID: return dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_CID: return 0; break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CID: return dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CID: return dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CMD( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STOP_SYNCHRO_CID: return 0; break;
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_HW_RSP( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_HW_MSG_PCB_INFO_CID: return dissect_tOCTVC1_HW_MSG_PCB_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CPU_CORE_STATS_CID: return dissect_tOCTVC1_HW_MSG_CPU_CORE_STATS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CPU_CORE_INFO_CID: return dissect_tOCTVC1_HW_MSG_CPU_CORE_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CPU_CORE_LIST_CID: return dissect_tOCTVC1_HW_MSG_CPU_CORE_LIST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_ETH_PORT_INFO_CID: return dissect_tOCTVC1_HW_MSG_ETH_PORT_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_ETH_PORT_LIST_CID: return dissect_tOCTVC1_HW_MSG_ETH_PORT_LIST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_ETH_PORT_STATS_CID: return dissect_tOCTVC1_HW_MSG_ETH_PORT_STATS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_CID: return dissect_tOCTVC1_HW_MSG_ETH_PORT_RESTRICTED_UNBLOCK_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_ETH_PORT_MODIFY_CID: return dissect_tOCTVC1_HW_MSG_ETH_PORT_MODIFY_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_INFO_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_STATS_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_STATS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_LIST_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_LIST_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_LIST_ANTENNA_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_RX_CONFIG_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_CID: return dissect_tOCTVC1_HW_MSG_RF_PORT_INFO_ANTENNA_TX_CONFIG_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_CID: return dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_CID: return dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATS_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_CID: return dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_INFO_SOURCE_RSP( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_MODIFY_SOURCE_CID: return 0; break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_START_SYNCHRO_CID: return 0; break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STOP_SYNCHRO_CID: return 0; break;
			default: return 1;
		}
	}
	return 0;

}
int ws_dissect_OCTVC1_HW_EVT( guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if( tree )
	{
		switch( CID ) {
			case cOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EID: return dissect_tOCTVC1_HW_MSG_CPU_CORE_EXEC_REPORT_EVT( tvb, pinfo, tree, 0 ); break;
			case cOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EID: return dissect_tOCTVC1_HW_MSG_CLOCK_SYNC_MGR_STATUS_CHANGE_EVT( tvb, pinfo, tree, 0 ); break;
			default: return 1;
		}
	}
	return 0;

}

/****************************************************************************
	MODULE DISSECTOR EXPORTED FUNCTION
 ****************************************************************************/

int ws_dissect_OCTVC1_HW( guint8 message_type, guint32 CID, tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree )
{
	if (message_type == cOCTVC1_MSG_TYPE_RESPONSE)
		return ws_dissect_OCTVC1_HW_RSP( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_COMMAND)
		return ws_dissect_OCTVC1_HW_CMD( CID, tvb, pinfo, tree);
	if (message_type == cOCTVC1_MSG_TYPE_NOTIFICATION )
		return ws_dissect_OCTVC1_HW_EVT( CID, tvb, pinfo, tree);
	else
		return 1;

}

