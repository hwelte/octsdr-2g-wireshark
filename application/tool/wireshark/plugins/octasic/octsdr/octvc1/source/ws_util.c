
/*****************************  INCLUDE FILES  *******************************/
#include <config.h>
#include <epan/packet.h>
#include <octvc1_rc2string.h>
#include "../../../include/oct_ws_macro.h"
/*****************************  DEFINES **************************************/

void ws_dissector_not_registered(void)
{

}

/*************************************************************************
*
*	Return 0 on success
**************************************************************************/
int ws_dissector_notimplemented( tvbuff_t * UNUSED(tvb), packet_info * UNUSED(pinfo), proto_tree * UNUSED(tree), unsigned int UNUSED(offset) )
{
	return 1;
}

/*************************************************************************
*
*	Return 0 on success
**************************************************************************/
int ws_dissector_module_notimplemented( guint8 UNUSED( message_type ), guint32 UNUSED(CID), tvbuff_t * UNUSED(tvb), packet_info * UNUSED(pinfo), 
		proto_tree * UNUSED(tree) )
{
	return 1;
}

unsigned int  dissect_tOCTVC1_MAIN_FILE_HANDLE(tvbuff_t * UNUSED(tvb), packet_info * UNUSED(pinfo), proto_tree * UNUSED(tree), unsigned int UNUSED(offset))
{
	return 0;
}

const char *  dissect_OCTVC1_Return_Code( guint32 ulRc )
{
	return octvc1_rc2string( ulRc );
}

#if defined(VERSION_MAJOR) && (VERSION_MAJOR > 1 || (VERSION_MAJOR == 1 && VERSION_MINOR > 8))
/*************************************************************************
*
*	
**************************************************************************/

/* Generate a string describing an enumerated bitfield (an N-bit field   with various specific values having particular names). */
const char *decode_enumerated_bitfield(const guint32 val, const guint32 mask, const int width,  const value_string *tab, const char *fmt)
{
  static char buf[1025];
  char *p;

  p = decode_bitfield_value(buf, val, mask, width);
  g_snprintf(p, (gulong) (1024-(p-buf)), fmt, val_to_str(val & mask, tab, "Unknown"));
  return buf;
}
#endif
