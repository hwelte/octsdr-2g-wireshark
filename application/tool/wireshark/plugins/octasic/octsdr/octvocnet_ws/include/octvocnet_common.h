/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: octvocnet_common.h

      
Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contain the wireshark module dissector related functions

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/*****************************  INCLUDE FILES  *******************************/
#ifndef _OCTVOCNET_COMMON_H__
#define _OCTVOCNET_COMMON_H__

#include <epan/packet.h>

/****************************************************************************
	COMMON API ENUMERATION STRING VALUES
 ****************************************************************************/
extern const value_string  vals_tOCTVOCNET_PKT_CONTROL_PROTOCOL_TYPE_ENUM[]; 

#endif /* _OCTVOCNET_COMMON_H__ */

