// Can't create link because WS makefile use relative path 
cd /pub/dev.wireshark/1.2.6/wireshark-1.2.6/plugins

// Copy GOOSE plugins
cp /mnt/goose/application/tool/wireshark/plugins/octasic octasic

****************************
cd /pub/dev.wireshark/1.2.6/wireshark-1.2.6

Modify file: configure.in
	to allow plugin version to related OCTVC1 build version.
	- Added

		#PPMODIF
		#
		AC_MSG_CHECKING([whether a TGT_VERSION was specified])
		AC_ARG_WITH([version],
			AS_HELP_STRING([--with-version=@<:@*@:>@],
									[Specify Target version @<:@default=1.3.1@:>@]),
						with_version="$withval", with_version="1.3.2")
		AC_SUBST(TGT_VERSION, [$with_version])
		AC_MSG_RESULT(TGT_VERSION=$TGT_VERSION)
		#
		AC_MSG_CHECKING([whether OCT_PATH was specified])
		AC_ARG_WITH([oct_path],
			AS_HELP_STRING([--with-oct_path=@<:@*@:>@],
									[Specify Octasic path @<:@default=/mnt/goose@:>@]),
						with_oct_path="$withval", with_oct_path="/mnt/goose")
		AC_SUBST(OCT_PATH, [$with_oct_path])
		AC_MSG_RESULT(OCT_PATH=$OCT_PATH)
		#PPMODIF

		AFTER:# Check for doxygen

Modify file: configure.in

find	AC_OUTPUT=

ADD
  plugins/octasic/oct1010db_ws/Makefile
  plugins/octasic/octmfa_ws/Makefile
  plugins/octasic/octpkt_ws/Makefile
  plugins/octasic/rfc4175_yuv_ws/Makefile
  octvc1_place_holder

replace
    octvc1_place_holder
  by
    plugins/octasic/vocallo_mgw/octvocnet_ws/Makefile
    plugins/octasic/vocallo_mgw/octvc1/Makefile

with ant

****************************
cd /pub/dev.wireshark/1.2.6/wireshark-1.2.6/plugins

Modify file: Makefile.am
	// remove all plugin under
	SUBDIRS = $(_CUSTOM_SUBDIRS_) \

	// Added
  	octasic/oct1010db_ws
  	octasic/octmfa_ws
	octasic/octpkt_ws
  	octasic/rfc4175_yuv_ws
        octvc1_place_holder

	...




replace
    octvc1_place_holder
  by
    plugins/octasic/vocallo_mgw/octvocnet_ws/Makefile
    plugins/octasic/vocallo_mgw/octvc1/Makefile

with ant


****************************
# For new plugins
cp /pub/dev.wireshark/1.2.6/wireshark-1.2.6/plugins/octasic/octpkt_ws/Makefile.am /pub/dev.wireshark/1.2.6/wireshark-1.2.6/plugins/octasic/newplugi_dir/Makefile.am
cp /pub/dev.wireshark/1.2.6/wireshark-1.2.6/plugins/octasic/octpkt_ws/Makefile.common /pub/dev.wireshark/1.2.6/wireshark-1.2.6/plugins/octasic/newplugi_dir/Makefile.common
..
update file list and pkg name.

****************************
Making Wireshark plugins:

cd /pub/dev.wireshark/1.2.6/wireshark-1.2.6
./autogen.sh

===> OCTVC1 Pkg Version
===> OCTASIC PATH in /mnt/goose
./configure --with-version=1.4.0.1-PR --with-oct_path=/mnt/goose

make install	==> Result is /usr/local/lib/wireshark/plugins/1.2.6


