/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: OCTVC1_ETH.h

Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: 

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

#ifndef __OCTVC1_ETH_H__
#define __OCTVC1_ETH_H__


/*****************************  INCLUDE FILES  *******************************/
#include "../octdev_types.h"
#include "octvc1_base.h"


/************************  COMMON DEFINITIONS  *******************************/

/*-------------------------------------------------------------------------------------
 	Ethernet port identifiers
-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------
	tOCTVC1_ETH_PORT_ID_ENUM :
-------------------------------------------------------------------------------------*/
#define tOCTVC1_ETH_PORT_ID_ENUM							tOCT_UINT32

#define cOCTVC1_ETH_PORT_ID_ENUM_0							0		
#define cOCTVC1_ETH_PORT_ID_ENUM_1							1		
#define cOCTVC1_ETH_PORT_ID_ENUM_2							2		
#define cOCTVC1_ETH_PORT_ID_ENUM_3							3		
#define cOCTVC1_ETH_PORT_ID_ENUM_INVALID					0xFFFFFFFF	

#define cOCTVC1_ETH_802_1_PQ_TCI_INVALID					0xFFFFFFFF	 	/* Ethernet 802.1 pq definition */


#endif /* __OCTVC1_ETH_H__ */

