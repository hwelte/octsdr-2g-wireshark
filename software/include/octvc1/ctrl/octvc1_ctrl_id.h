/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: OCTVC1_CTRL_ID.h

Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contains the identifiers for the CTRL API.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

#ifndef __OCTVC1_CTRL_ID_H__
#define __OCTVC1_CTRL_ID_H__


/*****************************  INCLUDE FILES  *******************************/
#include "../octvc1_base.h"
#include "../octvc1_generic_rc.h"
#include "octvc1_ctrl_base.h"

/****************************************************************************
	Supervisory IDs
 ****************************************************************************/
#define cOCTVC1_CTRL_MSG_MODULE_REJECT_SID ( 0x0001 + cOCTVC1_CTRL_SID_BASE )

#define cOCTVC1_CTRL_SID_MAX               (( 0x0001 + 1 ) & 0xFFFF)


#endif /* __OCTVC1_CTRL_ID_H__ */

