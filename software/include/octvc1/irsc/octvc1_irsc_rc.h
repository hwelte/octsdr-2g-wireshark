/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\

File: OCTVC1_IRSC_RC.h

Copyright (c) 2016 Octasic Inc. All rights reserved.

Description: Contains the return codes for the IRSC API.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)

$Octasic_Revision: $

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

#ifndef OCTVC1_IRSC_RC_H__
#define OCTVC1_IRSC_RC_H__


/*****************************  INCLUDE FILES  *******************************/
#include "../octvc1_base.h"
#include "../octvc1_generic_rc.h"
#include "octvc1_irsc_base.h"

/****************************************************************************
	IRSC return codes
 ****************************************************************************/
#define cOCTVC1_IRSC_RC_INVALID_TASK						 ( 0x000A + cOCTVC1_IRSC_RC_BASE )

/****************************************************************************
	OBJMGR return codes
 ****************************************************************************/
#define cOCTVC1_IRSC_OBJMGR_RC_BASE							 ( 0x0450 + cOCTVC1_IRSC_RC_BASE )

#define cOCTVC1_IRSC_RC_OBJMGR_LIST_ERROR					 ( 0x0001 + cOCTVC1_IRSC_OBJMGR_RC_BASE )
#define cOCTVC1_IRSC_RC_OBJMGR_INVALID_OBJECT				 ( 0x0002 + cOCTVC1_IRSC_OBJMGR_RC_BASE )
#define cOCTVC1_IRSC_RC_OBJMGR_INTERNAL_ERROR				 ( 0x0003 + cOCTVC1_IRSC_OBJMGR_RC_BASE )
#define cOCTVC1_IRSC_RC_OBJMGR_NOT_INIT						 ( 0x0004 + cOCTVC1_IRSC_OBJMGR_RC_BASE )

/****************************************************************************
	API_COMMAND return codes
 ****************************************************************************/
#define cOCTVC1_IRSC_API_COMMAND_RC_BASE						 ( 0x0550 + cOCTVC1_IRSC_RC_BASE )

#define cOCTVC1_IRSC_RC_API_COMMAND_MONITORING_DISABLE			 ( 0x0002 + cOCTVC1_IRSC_API_COMMAND_RC_BASE )
#define cOCTVC1_IRSC_RC_API_COMMAND_MAX_REACHED					 ( 0x0003 + cOCTVC1_IRSC_API_COMMAND_RC_BASE )
#define cOCTVC1_IRSC_RC_API_COMMAND_MONITORING_ALREADY_START	 ( 0x0004 + cOCTVC1_IRSC_API_COMMAND_RC_BASE )
#define cOCTVC1_IRSC_RC_API_COMMAND_INTERNAL_ERROR				 ( 0x0005 + cOCTVC1_IRSC_API_COMMAND_RC_BASE )

#endif /* OCTVC1_IRSC_RC_H__ */

