/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

File: octvc1_priv_swap_hdrs.h	

Copyright (c) 2016 Octasic Inc. All rights reserved.	

Description:	

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program;if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
	

Release: OCTSDR Software Development Kit OCTSDR_GSM-02.05.00-B780 (2016/01/14)	

\*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	
#ifndef __OCTVC1_PRIV_SWAP_HDRS_H__
#define __OCTVC1_PRIV_SWAP_HDRS_H__

#ifdef __cplusplus
extern "C" {
#endif

/*****************************  INCLUDE FILES  *******************************/
#include "octvc1_swap_hdrs.h"

/* START OF SWAP FILE HDR */

#include "ctrl/octvc1_ctrl_api_priv_swap.h"
#include "main/octvc1_main_api_priv_swap.h"
#include "main/octvc1_main_evt_priv_swap.h"
#include "test/octvc1_test_api_priv_swap.h"
#include "test/octvc1_test_evt_priv_swap.h"
#include "hw/octvc1_hw_api_priv_swap.h"
#include "hw/octvc1_hw_evt_priv_swap.h"
#include "irsc/octvc1_irsc_api_priv_swap.h"
#include "irsc/octvc1_irsc_evt_priv_swap.h"
#include "gsm/octvc1_gsm_api_priv_swap.h"
#include "gsm/octvc1_gsm_evt_priv_swap.h"
/* END OF SWAP FILE HDR */

#ifdef __cplusplus
}
#endif
#endif /* __OCTVC1_PRIV_SWAP_HDRS_H__ */

